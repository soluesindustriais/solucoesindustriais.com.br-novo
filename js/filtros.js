document.addEventListener('DOMContentLoaded', function () {
  iniciarFiltros();
  iniciarFiltrosCheckbox();
  adicionarListenerLimparFiltros();
});

function adicionarFiltroSelecionado(idSelect) {
  var select = document.getElementById(idSelect);
  var valorSelecionado = select.options[select.selectedIndex].text;
  var divFiltros = document.getElementById('filtrosSelecionados');

  // Cria o elemento do filtro
  var spanFiltro = document.createElement('span');
  spanFiltro.className = 'filtro-selecionado';
  spanFiltro.textContent = valorSelecionado;

  // Cria o botão para remover o filtro
  var botaoRemover = document.createElement('button');
  botaoRemover.textContent = 'x';
  botaoRemover.className = 'botao-remover-filtro';
  botaoRemover.onclick = function () {

    spanFiltro.remove();
    select.selectedIndex = 0;
  };

  // Adiciona o botão de remover ao elemento do filtro
  spanFiltro.appendChild(botaoRemover);

  // Adiciona o filtro à lista de filtros selecionados
  divFiltros.appendChild(spanFiltro);
}

function iniciarFiltros() {
  var selects = document.querySelectorAll('.selecionar-propriedades select');
  selects.forEach(function (select) {
    select.addEventListener('change', function () {
      if (this.selectedIndex > 0) {
        adicionarFiltroSelecionado(this.id);
      }
    });
  });
}

function adicionarFiltroSelecionadoCheckbox(categoria, valor) {
  var divFiltros = document.getElementById('filtrosSelecionados');

  // Cria o elemento do filtro
  var spanFiltro = document.createElement('span');
  spanFiltro.className = 'filtro-selecionado';
  spanFiltro.textContent = categoria;

  // Cria o botão para remover o filtro
  var botaoRemover = document.createElement('button');
  botaoRemover.textContent = 'x';
  botaoRemover.className = 'botao-remover-filtro';
  botaoRemover.onclick = function () {
    var checkboxParaDesmarcar = document.querySelector(`.opcoes-menu-lateral input[type="checkbox"][value="${valor}"]`);
    if (checkboxParaDesmarcar) {
      checkboxParaDesmarcar.checked = false;
    }
    spanFiltro.remove();
  };

  spanFiltro.appendChild(botaoRemover);

  divFiltros.appendChild(spanFiltro);
}

function iniciarFiltrosCheckbox() {
  var checkboxes = document.querySelectorAll('.opcoes-menu-lateral input[type="checkbox"]');
  checkboxes.forEach(function (checkbox) {
    checkbox.addEventListener('change', function () {
      var categoria = this.closest('li').textContent.trim();


      if (this.checked) {
        adicionarFiltroSelecionadoCheckbox(categoria);
      } else {
        var filtroParaRemover = Array.from(document.querySelectorAll('.filtro-selecionado')).find(span => span.textContent.includes(categoria));
        if (filtroParaRemover) {
          filtroParaRemover.remove();
        }
      }
    });
  });
}


function adicionarListenerLimparFiltros() {
  var botaoLimpar = document.getElementById('limpar-filtros');
  botaoLimpar.addEventListener('click', function () {
    limparTudo();
  });
}

function limparTudo() {
  document.querySelectorAll('.filtro-selecionado').forEach(function (span) {
    span.remove();
  });

  // reseta selects
  document.querySelectorAll('.selecionar-propriedades select').forEach(function (select) {
    select.selectedIndex = 0;
  });

  // desmarca os checkboxes
  document.querySelectorAll('.opcoes-menu-lateral input[type="checkbox"]').forEach(function (checkbox) {
    checkbox.checked = false;
  });
}