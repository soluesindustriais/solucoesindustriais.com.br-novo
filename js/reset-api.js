const botaoCotar = document.querySelector('.botao-cotar');
botaoCotar.addEventListener('click', function () {
    const modalHeader = document.querySelector('.cotacao-modal-header');

    if (modalHeader) {
        // Verifica se a imagem já existe
        const existingImg = modalHeader.querySelector('img[alt="Logo Soluções Industriais"]');
        if (!existingImg) { // Se a imagem não existir, cria e adiciona
            const imgElement = document.createElement('img');
            imgElement.src = 'img/logo.png';
            imgElement.alt = 'Logo Soluções Industriais';
            modalHeader.prepend(imgElement);
        }

        const h3Element = modalHeader.querySelector('h3');
        if (h3Element) {
            const brElements = h3Element.querySelectorAll('br');
            brElements.forEach(function (br) {
                h3Element.removeChild(br);
            });
        }
    }
});
