/*
// - - - - - - - - - - - - - - - - - - -
// SOLUCOES INDUSTRIAIS - THEME v001
// - - - - - - - - - - - - - - - - - - -
// #wf-202312151332
// #wf-202312301217
// - - - - - - - - - - - - - - - - - - -
// https://wizrdfly.rf.gd
// - - - - - - - - - - - - - - - - - - -
*/
// A
// W i z r d F L Y
// A P P L I C A T I O N
// - - - - - - - - - - - - - - - - - - -
var Wapp = Wapp || {};
// - - - - - - - - - - - - - - - - - - -
// M O D U L E
// - - - - - - - - - - - - - - - - - - -
// FF (fully functions)
// - - - - - - - - - - - - - - - - - - -
Wapp.FF = Wapp.FF || {};

(function (win, doc, Vars) {
    'use strict';

    /* private Vars */
    Vars = {
        btnMenu: doc.querySelectorAll('.btnMenu')[0],
        boxMenu: doc.querySelectorAll('.section.header .menu')[0],
    };

    /* init all functions */
    Wapp.FF.Init = function () {
        console.log('FF - FULLYFUNCTIONS [init]');

        // all listens
        Wapp.FF.Listen();

        // all sliders
        Wapp.FF.Sliders();
    };

    /* for all events listeners */
    Wapp.FF.Listen = function () {
        // console.log(':: Listen [fnc]');

        // (Vars.btnSave) ? Vars.btnSave.addEventListener('click', Wapp.FF.Storage, true) : '';
        (Vars.btnMenu) ? Vars.btnMenu.addEventListener('click', Wapp.FF.MenuMob, true): '';
    };

    /* menu mobile */
    Wapp.FF.MenuMob = function (e) {
        // console.log(':: MenuMob [fnc]');

        // comprador page
        if (Vars.btnMenu.classList.contains('otherPages')) {
            var
                boxOtherSearch = doc.querySelectorAll('.section.header .row:first-of-type .col-6')[0],
                boxOtherMenu = doc.querySelectorAll('.section.header .row:last-of-type')[0];

            if (boxOtherSearch && boxOtherMenu) {
                if (boxOtherSearch.classList.contains('active')) {
                    boxOtherSearch.classList.remove('active');

                } else {
                    boxOtherSearch.classList.add('active');
                }

                if (boxOtherMenu.classList.contains('active')) {
                    boxOtherMenu.classList.remove('active');

                } else {
                    boxOtherMenu.classList.add('active');
                }
            }

            // home page
        } else {
            if (Vars.boxMenu) {
                if (Vars.boxMenu.classList.contains('active')) {
                    Vars.boxMenu.classList.remove('active');

                } else {
                    Vars.boxMenu.classList.add('active');
                }
            }
        }

        e.preventDefault();
        return false;
    };

    /* all sliders */
    Wapp.FF.Sliders = function (e) {
        // console.log(':: Sliders [fnc]');

        var
            swiperBlogGallery,
            swiperBrandsGallery,
            swiperAdvertsGallery,
            swiper,
            swiper2,
            swiperCommentsGallery,
            imageGallery,
            productListGallery,
            categoryGallery;

        if (doc.querySelectorAll('.blogGallery')[0]) {
            swiperBlogGallery = new Swiper(".blogGallery", {
                loop: true,
                spaceBetween: 48,
                slidesPerView: 3,
                freeMode: true,
                watchSlidesProgress: true,
                navigation: false,
                autoplay: {
                    delay: 1500,
                    pauseOnMouseEnter: true,
                    disableOnInteraction: false,
                },
                breakpoints: {
                    1024: {
                        slidesPerView: 3,
                        spaceBetween: 48,
                    },
                    640: {
                        slidesPerView: 2,
                        spaceBetween: 32,
                    },
                    460: {
                        slidesPerView: 1,
                        spaceBetween: 0,
                    },
                    0: {
                        slidesPerView: 1,
                        spaceBetween: 0,
                    }
                }
            });
        }

        if (doc.querySelectorAll('.brandsGallery')[0]) {
            swiperBrandsGallery = new Swiper(".brandsGallery", {
                loop: true,
                spaceBetween: 20,
                slidesPerView: 4,
                freeMode: true,
                watchSlidesProgress: true,
                navigation: false,
                autoplay: {
                    delay: 500,
                    pauseOnMouseEnter: true,
                    disableOnInteraction: false,
                },
                breakpoints: {
                    1024: {
                        slidesPerView: 6,
                        spaceBetween: 20,
                    },
                    640: {
                        slidesPerView: 4,
                        spaceBetween: 20,
                    },
                    460: {
                        slidesPerView: 2,
                        spaceBetween: 20,
                    },
                    0: {
                        slidesPerView: 2,
                        spaceBetween: 20,
                    }
                }
            });
        }
        if (doc.querySelectorAll('.brandsGallery2')[0]) {
            swiperBrandsGallery2 = new Swiper(".brandsGallery2", {
                loop: true,
                spaceBetween: 20,
                slidesPerView: 4,
                freeMode: true,
                watchSlidesProgress: true,
                navigation: false,
                autoplay: {
                    delay: 500,
                    pauseOnMouseEnter: true,
                    disableOnInteraction: false,
                },
                navigation: {
                    nextEl: '.swiper-button-next', // Seletor para o botão "Próximo"
                    prevEl: '.swiper-button-prev', // Seletor para o botão "Anterior"
                },
                breakpoints: {
                    // Remova ou ajuste os breakpoints para manter 4 slides em todas as resoluções
                    1024: {
                        slidesPerView: 4, // Mantém 4 slides por visualização também para telas maiores
                        spaceBetween: 20,
                    },
                    640: {
                        slidesPerView: 3, // Ajuste conforme necessário para telas menores, se desejar
                        spaceBetween: 20,
                    },
                    460: {
                        slidesPerView: 2, // Ajuste conforme necessário para telas ainda menores
                        spaceBetween: 20,
                    },
                    0: {
                        slidesPerView: 1, // Ajuste para garantir que o padrão seja 4 desde o menor breakpoint
                        spaceBetween: 20,
                    }
                }
            });
        }

        if (doc.querySelectorAll('.advertsGallery')[0]) {
            swiperAdvertsGallery = new Swiper(".advertsGallery", {
                loop: true,
                freeMode: false, 
                watchSlidesProgress: false,
                navigation: {
                    prevEl: '.swiper-button-next', // Seletor para o botão "Próximo"
                    nextEl: '.swiper-button-prev', // Seletor para o botão "Próximo"
                },
                breakpoints: {
                    
                    1440: {
                        slidesPerView: 5,
                        spaceBetween: 20,
                    },
                    1024: {
                        slidesPerView: 4,
                        spaceBetween: 10,
                    },
                    924:{
                        slidesPerView: 4,
                        spaceBetween: 10,
                    },
                    640: {
                        slidesPerView: 2,
                        spaceBetween: 0,
                    },
                    460: {
                        slidesPerView: 1,
                        spaceBetween: 0,
                    },
                    0: {
                        slidesPerView: 1,
                        spaceBetween: 0,
                    }
                }
            });

        }

        if (document.querySelectorAll('.advertsGallery2').length > 0) {
            var swiperAdvertsGallery = new Swiper(".advertsGallery2", {
                loop: true,
                spaceBetween: 20,
                slidesPerView: 6,
                freeMode: true,
                watchSlidesProgress: true,
                autoplay: {
                    delay: 3500,
                    pauseOnMouseEnter: true,
                    disableOnInteraction: false,
                },
                breakpoints: {
                    1024: {
                        slidesPerView: 6,
                        spaceBetween: 20,
                    },
                    640: {
                        slidesPerView: 4,
                        spaceBetween: 20,
                    },
                    460: {
                        slidesPerView: 2,
                        spaceBetween: 20,
                    },
                    0: {
                        slidesPerView: 2,
                        spaceBetween: 20,
                    }
                }
            });
        }


        if (doc.querySelectorAll('.commentsGallery')[0]) {
            swiperCommentsGallery = new Swiper(".commentsGallery", {
                loop: true,
                spaceBetween: 24,
                slidesPerView: 3,
                freeMode: true,
                watchSlidesProgress: true,
                navigation: false,
                autoplay: {
                    delay: 3500,
                    pauseOnMouseEnter: true,
                    disableOnInteraction: false,
                },
                breakpoints: {
                    1024: {
                        slidesPerView: 3,
                        spaceBetween: 24,
                    },
                    640: {
                        slidesPerView: 2,
                        spaceBetween: 20,
                    },
                    460: {
                        slidesPerView: 1,
                        spaceBetween: 0,
                    },
                    0: {
                        slidesPerView: 1,
                        spaceBetween: 0,
                    }
                }
            });
        }

        if (doc.querySelectorAll('.videoPlayerGallery')[0] && doc.querySelectorAll('.videoPlayerGalleryThumbs')[0]) {
            swiper = new Swiper(".videoPlayerGallery", {
                loop: true,
                spaceBetween: 20,
                slidesPerView: 6,
                freeMode: true,
                watchSlidesProgress: true,
                navigation: false,
                breakpoints: {
                    1024: {
                        slidesPerView: 6,
                        spaceBetween: 20,
                    },
                    640: {
                        slidesPerView: 4,
                        spaceBetween: 20,
                    },
                    0: {
                        slidesPerView: 4,
                        spaceBetween: 20,
                    }
                }
            });
            swiper2 = new Swiper(".videoPlayerGalleryThumbs", {
                loop: true,
                spaceBetween: 10,
                navigation: false,
                thumbs: {
                    swiper: swiper,
                }
            });
        }

        if (doc.querySelectorAll('.imageGallery')[0]) {
            imageGallery = new Swiper(".imageGallery", {
                loop: true,
                spaceBetween: 24,
                slidesPerView: 5,
                freeMode: true,
                watchSlidesProgress: true,
                navigation: false,
                autoplay: {
                    delay: 3500,
                    pauseOnMouseEnter: true,
                    disableOnInteraction: false,
                },
                breakpoints: {
                    1024: {
                        slidesPerView: 5,
                        spaceBetween: 24,
                    },
                    640: {
                        slidesPerView: 4,
                        spaceBetween: 18,
                    },
                    460: {
                        slidesPerView: 2,
                        spaceBetween: 8,
                    },
                    0: {
                        slidesPerView: 1,
                        spaceBetween: 0,
                    }
                }
            });
        }
        if (doc.querySelectorAll('.imageGallery2')[0]) {
            imageGallery2 = new Swiper(".imageGallery2", {
                loop: true,
                spaceBetween: 24,
                slidesPerView: 5,
                freeMode: true,
                watchSlidesProgress: true,
                navigation: false,
                autoplay: {
                    delay: 3500,
                    pauseOnMouseEnter: true,
                    disableOnInteraction: false,
                },
                breakpoints: {
                    1024: {
                        slidesPerView: 5,
                        spaceBetween: 24,
                    },
                    640: {
                        slidesPerView: 4,
                        spaceBetween: 18,
                    },
                    460: {
                        slidesPerView: 2,
                        spaceBetween: 8,
                    },
                    0: {
                        slidesPerView: 1,
                        spaceBetween: 0,
                    }
                }
            });
        }
        if (doc.querySelectorAll('.productListGallerySld')[0]) {
            productListGallery = new Swiper(".productListGallerySld", {
                loop: true,
                spaceBetween: 24,
                slidesPerView: 5,
                freeMode: true,
                watchSlidesProgress: true,
                navigation: false,
                autoplay: {
                    delay: 3500,
                    pauseOnMouseEnter: true,
                    disableOnInteraction: false,
                },
                breakpoints: {
                    1024: {
                        slidesPerView: 5,
                        spaceBetween: 24,
                    },
                    640: {
                        slidesPerView: 4,
                        spaceBetween: 18,
                    },
                    460: {
                        slidesPerView: 2,
                        spaceBetween: 8,
                    },
                    0: {
                        slidesPerView: 1,
                        spaceBetween: 0,
                    }
                }
            });
        }

        if (doc.querySelectorAll('.categoryGallery')[0]) {
            categoryGallery = new Swiper(".categoryGallery", {
                loop: true,
                spaceBetween: 18,
                slidesPerView: 5,
                freeMode: true,
                watchSlidesProgress: true,
                navigation: {
                    nextEl: ".swiper-button-next",
                    prevEl: ".swiper-button-prev",
                },
                autoplay: {
                    delay: 3500,
                    pauseOnMouseEnter: true,
                    disableOnInteraction: false,
                },
                breakpoints: {
                    1024: {
                        slidesPerView: 5,
                        spaceBetween: 18,
                    },
                    640: {
                        slidesPerView: 4,
                        spaceBetween: 14,
                    },
                    460: {
                        slidesPerView: 2,
                        spaceBetween: 8,
                    },
                    0: {
                        slidesPerView: 1,
                        spaceBetween: 0,
                    }
                }
            });
        }
    };

    /* instantiation this module in window load */
    win.addEventListener('load', Wapp.FF.Init, true);

    /* manual init */
    // Wapp.FF.Init();

})(window, document, 'Private');