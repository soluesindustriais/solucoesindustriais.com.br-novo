<?php
$h1 = 'Chat';
$desc = 'Falta desc';
include 'inc/head.php';
?>
<!-- styles -->
<link rel="stylesheet" href="css/cmp-styles.css" />

<!-- media -->
<link rel="stylesheet" href="css/cmp-media.css" />
</head>

<body>

    <!-- header -->
    <section class="section header">
        <!-- <div class="container"> -->
        <?php
        include 'inc/menu-interno.php';
        ?>
    </section>

    <!-- content message -->
    <section class="section contentMessage">
        <div class="container-xxl">
            <?= $caminho ?>
            <div class="row boxHead">
                <div class="col">
                    <div class="user">
                        <img src="img/user-msg.png" alt="user message" />
                        <p>
                            <!-- <strong class="statusOffline" title="Offline">
                                Michael Gonçalvez
                            </strong> -->
                            <strong class="statusOnline" title="Online">
                                Michael Gonçalvez
                            </strong>
                            <span>
                                Orçamento #343453 -  Nome do Produto
                            </span>
                        </p>
                    </div>
                    <div class="info">
                        <a href="#" title="Informação">
                            <img src="img/ico-infos.png" alt="informação" />
                        </a>
                    </div>
                </div>
            </div>
            <div class="row boxMsg">
                <div class="col">
                    <div class="linex">
                        <div class="details">
                            <img src="img/user-msg.png" alt="user message" />
                            <span>
                                08:02
                            </span>
                        </div>
                        <div class="message">
                            <p>
                                Olá!
                            </p>
                        </div>
                    </div>
                    <div class="linex right">
                        <div class="message">
                            <p>
                                Olá João Paulo! Entraremos em contato por telefone para melhor atendê-lo(a). Esteja atento às ligações nos próximos instantes.
                            </p>
                        </div>
                        <div class="details">
                            <img src="img/user-msg-2.png" alt="user message" />
                            <span>
                                09:10
                            </span>
                        </div>
                    </div>
                    <div class="linex">
                        <div class="details">
                            <img src="img/user-msg.png" alt="user message" />
                            <span>
                                09:12
                            </span>
                        </div>
                        <div class="message">
                            <p>
                                Lorem ipsum dolor sit amet consectetur. Lacinia proin orci nulla purus pellentesque. Risus sapien arcu felis sed. Lobortis vitae nulla arcu turpis pharetra sem elit. Tincidunt interdum ipsum commodo pulvinar vitae tellus tellus mollis at. Ligula velit pretium pulvinar tortor sed. Cum arcu cras risus convallis. Non adipiscing leo aliquet porttitor neque a adipiscing.
                            </p>
                        </div>
                    </div>
                    <div class="linex right">
                        <div class="message">
                            <p>
                                Lorem ipsum, dolor sit!
                            </p>
                        </div>
                        <div class="details">
                            <img src="img/user-msg-2.png" alt="user message" />
                            <span>
                                09:27
                            </span>
                        </div>
                    </div>
                    <div class="linex">
                        <div class="details">
                            <img src="img/user-msg.png" alt="user message" />
                            <span>
                                10:02
                            </span>
                        </div>
                        <div class="message">
                            <p>
                                Lorem ipsum dolor sit amet consectetur.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row boxSend">
                <div class="col">
                    <!-- form -->
                    <form action="#" class="msg" autocomplete="off">
                        <fieldset>
                            <input type="text" class="form-control" name="search" placeholder="Escreva alguma coisa..." autocomplete="off" />
                            <button type="submit" class="form-control" title="Enviar">
                                <img src="img/ico-btnsend.png" alt="send" />
                            </button>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <!-- footer -->
    <?php include 'inc/footer.php'; ?>

    <!-- BOOTSTRAP @5.3.2 -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>

    <!-- SWIPER JS @11.0.0 -->
    <!-- <script src="https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.js"></script> -->

    <!-- functions -->
    <script src="js/functions.js"></script>

</body>

</html>