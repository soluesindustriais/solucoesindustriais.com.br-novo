<?php
$h1 = 'Favoritos';
$desc = 'Falta desc';
include 'inc/head.php';
?>
<!-- styles -->
<link rel="stylesheet" href="css/cmp-styles.css" />

<!-- media -->
<link rel="stylesheet" href="css/cmp-media.css" />
</head>
<body>
    
<section class="section header">
    <? include 'inc/menu-interno.php'; ?>

</section>
<div class="orcamento-secao">
    <?php include 'inc/menu-orcamento.php' ?>
    <div class="container ">
        <?= $caminho ?>
        <h1 class="title-style p-3">Favoritos</h1>
        <div class="container" id="page-container">
            <div class="info-favoritos p-2 mt-3 mb-3 ml-0">
                <div class="row">
                    <div class="col-4 col-md-2">
                        <img src="https://via.placeholder.com/200x200" alt="escrever_aqui" title="escrever_aqui" class="img-fluid">
                    </div>
                    <div class="col-8">
                        <h2>Mangueira de incêndio vermelha 5 metros - 3M</h2>
                        <br>
                        <p>Empresa: XXXXXXXXXX</p>
                        <a href="<? $url ?>#" title="escrever_aqui" class="excluir">Excluir</a>
                    </div>
                </div>
            </div>
            <div class="info-favoritos p-2 mt-3 mb-3 ml-0">
                <div class="row">
                    <div class="col-4 col-md-2">
                        <img src="https://via.placeholder.com/200x200" alt="escrever_aqui" title="escrever_aqui" class="img-fluid">
                    </div>
                    <div class="col-8">
                        <h2>Mangueira de incêndio vermelha 5 metros - 3M</h2>
                        <br>
                        <p>Empresa: XXXXXXXXXX</p>
                        <a href="<? $url ?>#" title="escrever_aqui" class="excluir">Excluir</a>
                    </div>
                </div>
            </div>
            <div class="info-favoritos p-2 mt-3 mb-3 ml-0">
                <div class="row">
                    <div class="col-4 col-md-2">
                        <img src="https://via.placeholder.com/200x200" alt="escrever_aqui" title="escrever_aqui" class="img-fluid">
                    </div>
                    <div class="col-8">
                        <h2>Mangueira de incêndio vermelha 5 metros - 3M</h2>
                        <br>
                        <p>Empresa: XXXXXXXXXX</p>
                        <a href="<? $url ?>#" title="escrever_aqui" class="excluir">Excluir</a>
                    </div>
                </div>
            </div>
            <div class="info-favoritos p-2 mt-3 mb-3 ml-0">
                <div class="row">
                    <div class="col-4 col-md-2">
                        <img src="https://via.placeholder.com/200x200" alt="escrever_aqui" title="escrever_aqui" class="img-fluid">
                    </div>
                    <div class="col-8">
                        <h2>Mangueira de incêndio vermelha 5 metros - 3M</h2>
                        <br>
                        <p>Empresa: XXXXXXXXXX</p>
                        <a href="<? $url ?>#" title="escrever_aqui" class="excluir">Excluir</a>
                    </div>
                </div>
            </div>
            <div class="info-favoritos p-2 mt-3 mb-3 ml-0">
                <div class="row">
                    <div class="col-4 col-md-2">
                        <img src="https://via.placeholder.com/200x200" alt="escrever_aqui" title="escrever_aqui" class="img-fluid">
                    </div>
                    <div class="col-8">
                        <h2>Mangueira de incêndio vermelha 5 metros - 3M</h2>
                        <br>
                        <p>Empresa: XXXXXXXXXX</p>
                        <a href="<? $url ?>#" title="escrever_aqui" class="excluir">Excluir</a>
                    </div>
                </div>
            </div>
            <div class="info-favoritos p-2 mt-3 mb-3 ml-0">
                <div class="row">
                    <div class="col-4 col-md-2">
                        <img src="https://via.placeholder.com/200x200" alt="escrever_aqui" title="escrever_aqui" class="img-fluid">
                    </div>
                    <div class="col-8">
                        <h2>Mangueira de incêndio vermelha 5 metros - 3M</h2>
                        <br>
                        <p>Empresa: XXXXXXXXXX</p>
                        <a href="<? $url ?>#" title="escrever_aqui" class="excluir">Excluir</a>
                    </div>
                </div>
            </div>
            <div class="info-favoritos p-2 mt-3 mb-3 ml-0">
                <div class="row">
                    <div class="col-4 col-md-2">
                        <img src="https://via.placeholder.com/200x200" alt="escrever_aqui" title="escrever_aqui" class="img-fluid">
                    </div>
                    <div class="col-8">
                        <h2>Mangueira de incêndio vermelha 5 metros - 3M</h2>
                        <br>
                        <p>Empresa: XXXXXXXXXX</p>
                        <a href="<? $url ?>#" title="escrever_aqui" class="excluir">Excluir</a>
                    </div>
                </div>
            </div>
            <div class="info-favoritos p-2 mt-3 mb-3 ml-0">
                <div class="row">
                    <div class="col-4 col-md-2">
                        <img src="https://via.placeholder.com/200x200" alt="escrever_aqui" title="escrever_aqui" class="img-fluid">
                    </div>
                    <div class="col-8">
                        <h2>Mangueira de incêndio vermelha 5 metros - 3M</h2>
                        <br>
                        <p>Empresa: XXXXXXXXXX</p>
                        <a href="<? $url ?>#" title="escrever_aqui" class="excluir">Excluir</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

        <?php
        include 'inc/footer.php';
        ?>

        
</body>
</html>