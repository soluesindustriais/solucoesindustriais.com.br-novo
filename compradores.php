<?php
$h1 = 'Compradores';
$desc = 'Falta desc';
include 'inc/head.php';
?>
</head>

<body class="pageBuy">

    <!-- header -->
    <section class="section header">
        <!-- <div class="container"> -->
        <div class="container-xxl">

            <?php
            include 'inc/menu.php';
            ?>
            
            <div class="row">

                <div class="col-6">
                    <span>
                        Tudo que você busca no mercado industrial em um só lugar
                    </span>
                    <h1 class="title">
                        Soluções Industriais para Compradores
                    </h1>
                    <p>
                        Cote diversos produtos e serviços diretamente fornecedores. Compare diferentes opções e encontre as melhores propostas. Simplifique suas compras e economize tempo.
                    </p>
                    <a href="#" class="btn btn-success" title="Cadastre-se">
                        Cadastre-se
                    </a>
                </div>
                <div class="col-6">
                    <img src="img/bg-buy-header-001.jpg" alt="Aumente sua Visibilidade, Contatos e Resultados" />
                </div>
            </div>
        </div>
    </section>

    <!-- steps default -->
    <section class="section stepsDefault">
        <div class="container-xxl">
            <div class="row">
                <div class="col-md-4">
                    <span>
                        1
                    </span>
                    <p>
                        <strong>
                            Pesquise
                        </strong>
                        Mais de 1 milhão de produtos e serviços para sua escolha
                    </p>
                </div>
                <div class="col-md-4">
                    <span>
                        2
                    </span>
                    <p>
                        <strong>
                            Cote
                        </strong>
                        Negocie com os melhores fornecedores do mercado industrial
                    </p>
                </div>
                <div class="col-md-4">
                    <span>
                        3
                    </span>
                    <p>
                        <strong>
                            Compare
                        </strong>
                        Escolha a melhor cotação de acordo com a sua necessidade
                    </p>
                </div>
            </div>
        </div>
    </section>

    <!-- infosDefault -->
    <section class="section infosDefault">
        <div class="container-xxl">
            <div class="row">
                <div class="col">
                    <h3 class="title">
                        Busque por produtos e serviços
                    </h3>
                    <h6>
                        Pesquise por produtos e serviços de acordo com sua necessidade e encontre informações sobre eles, assim como de seus fornecedores, de forma simples e rápida. No Soluções Industriais você encontra tudo o que precisa em uma única página de navegação.
                    </h6>
                </div>
                <div class="col-6">
                    <img src="img/bg-buy-001.png" alt="Divulgue produtos e serviços" />
                </div>
            </div>
        </div>
    </section>

    <!-- infosDefault -->
    <section class="section infosDefault color">
        <div class="container-xxl">
            <div class="row">
                <div class="col-6">
                    <img src="img/bg-buy-002.png" alt="Negocie com compradores qualificados" />
                </div>
                <div class="col">
                    <h3 class="title">
                        Cote sem compromisso
                    </h3>
                    <h6>
                        Nossa plataforma oferece acesso a mais de 50 mil fornecedores com cotações gratuitas e sem limite mensal. Negocie com as empresas parceiras do Soluções Industriais para garantir qualidade e segurança em todo o processo.
                    </h6>
                </div>
            </div>
        </div>
    </section>

    <!-- infosDefault -->
    <section class="section infosDefault">
        <div class="container-xxl">
            <div class="row">
                <div class="col">
                    <h3 class="title">
                        Escolha a melhor opção
                    </h3>
                    <h6>
                        Só aqui você consegue escolher os melhores fornecedores da sua região em um só lugar e receber diversas cotações com um só clique. Interaja e negocie com os fornecedores de acordo com as suas necessidades.
                    </h6>
                </div>
                <div class="col-6">
                    <img src="img/bg-buy-003.png" alt="Aumente sua Visibilidade, Contatos e Resultados" />
                </div>
            </div>
        </div>
    </section>

    <!-- boxBrands -->
    <section class="section boxBrands">
        <div class="container-xxl">
            <div class="row text-center">
                <div class="col">
                    <h3 class="title">
                        Quem compra no Soluções Industriais
                    </h3>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <!-- brands gallery -->
                    <div class="swiper brandsGallery">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <a href="#" title="Gerdau">
                                    <img src="img/ico-gerdau.png" alt="Gerdau" />
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="#" title="Prefeitura São Paulo">
                                    <img src="img/ico-prefeitura-sp.png" alt="Prefeitura São Paulo" />
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="#" title="Petrobras">
                                    <img src="img/ico-petrobras.png" alt="Petrobras" />
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="#" title="Vale">
                                    <img src="img/ico-vale.png" alt="Vale" />
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="#" title="USP">
                                    <img src="img/ico-usp.png" alt="USP" />
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="#" title="Odebrecht">
                                    <img src="img/ico-odebrecht.png" alt="Odebrecht" />
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="#" title="Ambev">
                                    <img src="img/ico-ambev.png" alt="Ambev" />
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="#" title="Mahle">
                                    <img src="img/ico-mahle.png" alt="Mahle" />
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="#" title="Marinha do Brasil">
                                    <img src="img/ico-marinha-brasil.png" alt="Marinha do Brasil" />
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="#" title="General Motors">
                                    <img src="img/ico-gm.png" alt="General Motors" />
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="#" title="WEG">
                                    <img src="img/ico-weg.png" alt="WEG" />
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="#" title="Saint Gobain">
                                    <img src="img/ico-saint-gobain.png" alt="Saint Gobain" />
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- boxFind -->
    <section class="section boxFind">
        <div class="container-xxl">
            <div class="row text-center">
                <div class="col">
                    <h5>
                        O sucesso é feito de escolhas e o Soluções Industriais é a melhor delas para o futuro do seu negócio.
                    </h5>
                    <a href="#" class="btn btn-success blue" title="Cadastre-se">
                        Cadastre-se
                    </a>
                </div>
            </div>
        </div>
    </section>

    <!-- footer -->
    <?php
    include 'inc/footer.php';
    ?>

    <!-- BOOTSTRAP @5.3.2 -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>

    <!-- SWIPER JS @11.0.0 -->
    <script src="https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.js"></script>

    <!-- functions -->
    <script src="js/functions.js"></script>

</body>

</html>