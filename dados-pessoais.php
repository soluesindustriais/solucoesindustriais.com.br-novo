<?php
$h1 = 'Dados pessoais';
$desc = 'Falta desc';
include 'inc/head.php';
?>
<!-- styles -->
<link rel="stylesheet" href="css/cmp-styles.css" />

<!-- media -->
<link rel="stylesheet" href="css/cmp-media.css" />


</head>

<body>

    <!-- header -->
    <section class="section header">
        <!-- <div class="container"> -->
        <div class="container-xxl">
            <?php
            include 'inc/menu-interno.php';
            ?>

        </div>
    </section>

    <!-- user data -->
    <section class="section userData">
        <div class="container-xxl">
            <?= $caminho ?>
            <div class="row">
                <div class="col">
                    <h3>
                        Dados pessoais
                    </h3>
                    <div class="row">
                        <div class="col-4">
                            <label>
                                <em>
                                    Nome
                                </em>
                                <input type="text" class="form-control" name="name" placeholder="Insira seu nome" />
                            </label>
                        </div>
                        <div class="col-4">
                            <label>
                                <em>
                                    Gênero
                                </em>
                                <select class="form-control" name="gender">
                                    <option value="">
                                        Selecione uma opção
                                    </option>
                                    <option value="opt02">
                                        Option 02
                                    </option>
                                    <option value="opt03">
                                        Option 03
                                    </option>
                                </select>
                            </label>
                        </div>
                        <div class="col-4">
                            <label>
                                <em>
                                    CNPJ/CPF
                                </em>
                                <input type="text" class="form-control" name="cnpj" placeholder="Insira o número do CNPJ ou CPF" />
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <label>
                                <em>
                                    Data de nascimento
                                </em>
                                <input type="date" class="form-control" name="birthdate" placeholder="DD/MM/AAAA" />
                            </label>
                        </div>
                        <div class="col-6">
                            <label>
                                <em>
                                    Anexo
                                </em>
                                <input type="file" class="form-control" name="anexo" placeholder="Selecionar aquivo" />
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <h3>
                        Dados de acesso
                    </h3>
                    <div class="row">
                        <div class="col-6">
                            <label>
                                <em>
                                    E-mail
                                </em>
                                <input type="text" class="form-control" name="email" placeholder="Insira seu e-mail" />
                            </label>
                        </div>
                        <div class="col-6">
                            <label>
                                <em>
                                    Senha
                                </em>
                                <input type="password" class="form-control" name="pass" placeholder="Insira sua senha" />
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <h3>
                        Logradouro
                    </h3>
                    <div class="row">
                        <div class="col-4">
                            <label>
                                <em>
                                    CEP
                                </em>
                                <input type="text" class="form-control" name="cep" placeholder="00000-000" />
                            </label>
                        </div>
                        <div class="col-4">
                            <label>
                                <em>
                                    Endereço
                                </em>
                                <input type="text" class="form-control" name="address" placeholder="Insira sua rua ou avenida" />
                            </label>
                        </div>
                        <div class="col-4">
                            <label>
                                <em>
                                    Bairro
                                </em>
                                <input type="text" class="form-control" name="district" placeholder="Insira seu bairro" />
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-4">
                            <label>
                                <em>
                                    Número
                                </em>
                                <input type="text" class="form-control" name="number" placeholder="Digite o número" />
                            </label>
                        </div>
                        <div class="col-4">
                            <label>
                                <em>
                                    Cidade
                                </em>
                                <select class="form-control" name="city">
                                    <option value="">
                                        Selecione uma opção
                                    </option>
                                    <option value="opt02">
                                        Option 02
                                    </option>
                                    <option value="opt03">
                                        Option 03
                                    </option>
                                </select>
                            </label>
                        </div>
                        <div class="col-4">
                            <label>
                                <em>
                                    Estado
                                </em>
                                <select class="form-control" name="state">
                                    <option value="">
                                        Selecione uma opção
                                    </option>
                                    <option value="opt02">
                                        Option 02
                                    </option>
                                    <option value="opt03">
                                        Option 03
                                    </option>
                                </select>
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-4">
                            <label>
                                <em>
                                    Complemento
                                </em>
                                <input type="text" class="form-control" name="complement" placeholder="Opcional" />
                            </label>
                        </div>
                        <div class="col-4">
                            <label>
                                <em>
                                    Telefone
                                </em>
                                <input type="tel" class="form-control" name="phone" placeholder="Digite o número de telefone com DDD" />
                            </label>
                        </div>
                        <div class="col-4">
                            <label>
                                <em>
                                    Telefone alternativo
                                </em>
                                <input type="tel" class="form-control" name="phone2" placeholder="Digite o número de telefone com DDD" />
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row btnSubmit">
                <div class="col">
                    <a href="#" class="btn btn-success" title="Salvar">
                        Salvar
                    </a>
                </div>
            </div>
        </div>
    </section>

    <!-- footer -->
    <?php
    include 'inc/footer.php';
    ?>
    <!-- BOOTSTRAP @5.3.2 -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>

    <!-- SWIPER JS @11.0.0 -->
    <!-- <script src="https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.js"></script> -->

    <!-- functions -->
    <script src="js/functions.js"></script>

</body>

</html>