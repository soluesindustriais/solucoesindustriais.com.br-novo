<?php
$h1 = 'Orçamento';
$desc = 'Falta desc';
include 'inc/head.php';
?>
<!-- styles -->
<link rel="stylesheet" href="css/cmp-styles.css" />

<!-- media -->
<link rel="stylesheet" href="css/cmp-media.css" />
</head>

<body>

    <!-- header -->
    <section class="section header">
        <!-- <div class="container"> -->
        <?php
        include 'inc/menu-interno.php';
        ?>
    </section>

    <!-- content category -->
    <section class="section contentList">
        <div class="container-xxl">
            <?= $caminho ?>
            <div class="row">
                <div class="col">
                    <h3>
                        Meus orçamentos
                    </h3>
                    <div class="row linePending">
                        <div class="col">
                            <p>
                                <strong>
                                    01/01/2023
                                </strong>
                                Data do orçamento
                            </p>
                        </div>
                        <div class="col">
                            <img src="img/product-paper.png" alt="Caixa de papelão" />
                            <p>
                                <strong>
                                    Caixa de papelão
                                </strong>
                                <img src="img/ico-product-paper.png" alt="Caixa de papelão" />
                                Produto/Serviço
                            </p>
                        </div>
                        <div class="col">
                            <p>
                                <strong>
                                    Status
                                </strong>
                                <em class="statusPending">
                                    Pendente
                                </em>
                            </p>
                        </div>
                        <div class="col">
                            <a href="<?= $url ?>detalhes-orcamento" title="Caixa de papelão">
                                <img src="img/ico-arrow-right.png" alt="Caixa de papelão" />
                            </a>
                        </div>
                    </div>
                    <div class="row lineProgress">
                        <div class="col">
                            <p>
                                <strong>
                                    01/01/2023
                                </strong>
                                Data do orçamento
                            </p>
                        </div>
                        <div class="col">
                            <img src="img/product-paper.png" alt="Caixa de papelão" />
                            <p>
                                <strong>
                                    Caixa de papelão
                                </strong>
                                <img src="img/ico-product-paper.png" alt="Caixa de papelão" />
                                Produto/Serviço
                            </p>
                        </div>
                        <div class="col">
                            <p>
                                <strong>
                                    Status
                                </strong>
                                <em class="statusProgress">
                                    Em andamento
                                </em>
                            </p>
                        </div>
                        <div class="col">
                            <a href="#" title="Caixa de papelão">
                                <img src="img/ico-arrow-right.png" alt="Caixa de papelão" />
                            </a>
                        </div>
                    </div>
                    <div class="row linePending">
                        <div class="col">
                            <p>
                                <strong>
                                    01/01/2023
                                </strong>
                                Data do orçamento
                            </p>
                        </div>
                        <div class="col">
                            <img src="img/product-paper.png" alt="Caixa de papelão" />
                            <p>
                                <strong>
                                    Caixa de papelão
                                </strong>
                                <img src="img/ico-product-paper.png" alt="Caixa de papelão" />
                                Produto/Serviço
                            </p>
                        </div>
                        <div class="col">
                            <p>
                                <strong>
                                    Status
                                </strong>
                                <em class="statusPending">
                                    Pendente
                                </em>
                            </p>
                        </div>
                        <div class="col">
                            <a href="#" title="Caixa de papelão">
                                <img src="img/ico-arrow-right.png" alt="Caixa de papelão" />
                            </a>
                        </div>
                    </div>
                    <div class="row lineFinished">
                        <div class="col">
                            <p>
                                <strong>
                                    01/01/2023
                                </strong>
                                Data do orçamento
                            </p>
                        </div>
                        <div class="col">
                            <img src="img/product-paper.png" alt="Caixa de papelão" />
                            <p>
                                <strong>
                                    Caixa de papelão
                                </strong>
                                <img src="img/ico-product-paper.png" alt="Caixa de papelão" />
                                Produto/Serviço
                            </p>
                        </div>
                        <div class="col">
                            <p>
                                <strong>
                                    Status
                                </strong>
                                <em class="statusFinished">
                                    Finalizado
                                </em>
                            </p>
                        </div>
                        <div class="col">
                            <a href="#" title="Caixa de papelão">
                                <img src="img/ico-arrow-right.png" alt="Caixa de papelão" />
                            </a>
                        </div>
                    </div>
                    <div class="row lineProgress">
                        <div class="col">
                            <p>
                                <strong>
                                    01/01/2023
                                </strong>
                                Data do orçamento
                            </p>
                        </div>
                        <div class="col">
                            <img src="img/product-paper.png" alt="Caixa de papelão" />
                            <p>
                                <strong>
                                    Caixa de papelão
                                </strong>
                                <img src="img/ico-product-paper.png" alt="Caixa de papelão" />
                                Produto/Serviço
                            </p>
                        </div>
                        <div class="col">
                            <p class="statusProgress">
                                <strong>
                                    Status
                                </strong>
                                <em class="statusProgress">
                                    Em andamento
                                </em>
                            </p>
                        </div>
                        <div class="col">
                            <a href="#" title="Caixa de papelão">
                                <img src="img/ico-arrow-right.png" alt="Caixa de papelão" />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="amountPages">
                        Página 1 de 4
                    </div>
                    <ul class="pagination">
                        <li>
                            <a href="#" title="Anterior">
                                <img src="img/ico-pag-prev.png" alt="Anterior" />
                            </a>
                        </li>
                        <li>
                            <a href="#" title="Página 1" class="active">
                                1
                            </a>
                        </li>
                        <li>
                            <a href="#" title="Página 2">
                                2
                            </a>
                        </li>
                        <li>
                            <a href="#" title="Página 3">
                                3
                            </a>
                        </li>
                        <li>
                            <a href="#" title="Página 4">
                                4
                            </a>
                        </li>
                        <li>
                            <a href="#" title="Próximo">
                                <img src="img/ico-pag-next.png" alt="Próximo" />
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <!-- footer -->
    <?php include 'inc/footer.php'; ?>

    <!-- BOOTSTRAP @5.3.2 -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>

    <!-- SWIPER JS @11.0.0 -->
    <script src="https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.js"></script>

    <!-- functions -->
    <script src="js/functions.js"></script>

</body>

</html>