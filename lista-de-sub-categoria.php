<?php
// Informações da página
$h1 = 'Lista de sub-categoria de produtos';
$desc = 'Falta desc';
?>
<? include('inc/head.php') ?>
<!-- styles -->
<link rel="stylesheet" href="css/cmp-styles.css" />

<!-- media -->
<link rel="stylesheet" href="css/cmp-media.css" />
<style>
  .modal {
    display: none;
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-color: rgba(0, 0, 0, 0.7);
    z-index: 1;
    justify-content: center;
    align-items: center;
  }

  .modal-conteudo {
    background-color: #f4f4f4;
    padding: 20px;
    border-radius: 5px;
    box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.5);
    text-align: center;
    width: 50%;
    height: auto;
    position: absolute;
    top: 30%;
    left: 25%;
  }

  .fechar {
    position: absolute;
    top: 10px;
    right: 10px;
    font-size: 24px;
    cursor: pointer;
  }

  #abrirModal {
    background-color: #007BFF;
    color: #fff;
    border: none;
    padding: 10px 20px;
    border-radius: 5px;
    cursor: pointer;
  }

  #abrirModal:hover {
    background-color: #0056b3;
  }
</style>
</head>

<body class="lista-de-sub-categoria">
  <section class="section header">
    <!-- <div class="container"> -->
    <?php
    include 'inc/menu-interno.php';
    ?>
  </section>
  <div class="container">
    <?= $caminho3 ?>
    <h1 class="title-style p-3"><?=$h1?></h1>

  <script>
    function redirectToSelected() {
      var selectedLink = document.getElementById("linkSelect").value;
      if (selectedLink !== '') {
        window.location.href = selectedLink;
      }
    }
  </script>

<div class="content-sub-categoria">
  <div class="container container-anunciantes">
    <div class="row">

      <div class="col-sm-12">
        <div id="resultado"></div>
      </div>

      <div class="mais-categorias-field" style="width: 100%;">
        <button class="mais-categorias-btn" id="exibirMaisEmpresasBtn">Exibir mais empresas</button>
      </div>
      <div class="modal" id="meuModal">
        <div class="modal-conteudo">
          <span class="fechar" id="fecharModal">&times;</span>
          <div class="nomeprod">
            <div class="row">
              <div class="col-2"><img src="<?= $url ?>/img/logo.png" alt=""></div>
              <div class="col-10">
                <p>Ver contato de Nome da empresa</p>
              </div>
            </div>


            <div class="row">
              <div class="col-6">
                <div class="infos">
                  <p><span>Cidade: </span>São Paulo - SP</p>
                  <p><span>Responsável:</span> João</p>
                  <p><span>Email:</span> joaozinho@hotmail.com</p>
                  <p><span>Telefone:</span> (11)1234-1234</p>
                  <p><span>Outras opções de contato: botão de Whatsapp</span></p>
                </div>
              </div>
              <div class="col-6">
                <div class="form-modal">
                  <p>Sem tempo para entrar em contato? <br> Deixe seus dados abaixo:</p>
                  <form action="">
                    <input type="text" placeholder="Insira seu nome">
                    <input type="phone" placeholder="Insira seu telefone">
                    <input type="email" placeholder="Insira seu e-mail">
                    <input type="submit" value="SOLICITAR CONTATO">
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <script>
        const resultadoDiv = document.getElementById('resultado');
        let numeros = [1, 2, 3]; // Array inicial de números
        let contador = numeros[numeros.length - 1]; // Inicializa o contador com o último número do array

        function atualizarExibicao() {
          resultadoDiv.innerHTML = ''; // Limpa a exibição atual

          let nomeEmpresa = 1; // Variável para controlar o número do nome da empresa

          numeros.forEach(function(numero) {
            const html = `
      <div class="empresa">
        <div class="empresa-logo-field">
          <img src="<?= $url ?>img/logo.png" alt="">
        </div>
        <div class="empresa-info-field">
          <h6><a href="<?= $url ?>mini-site-home">Nome da Empresa ${nomeEmpresa}</a></h6>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores.adasdsad amet, consectetur adipisicing elit. Dolores.adasdsad texto da empresa
           Lorem, ipsum dolor sit, amet consectetur adipisicing elit. Cumque natus porro dignissimos reiciendis illo nobis praesentium! Soluta id nulla sed et sunt nam ullam suscipit? Dolores natus excepturi assumenda earum?  texte reticendia adipisicing elit. Dolores.adasdsad amet, Máximo 5 linhas pisicing elit. Dolores.adasdsad texto da empresa
           Lorem, <b>Máximo 5 linhas</b> , amet consectetur adipisicing elit. C</p>
          <div class="btn-empresas">
          <button class="rounded-no-border more-list-fornecedor botao-cotar" id="btn-cotar" title="betoneira">Solicitar Orçamento Grátis</button>
          </div>
        </div>
        <div class="empresa-logo-field-right">
          <img src="<?= $url ?>img/logo.png" alt="">
        </div>
      </div>
    `;

            resultadoDiv.innerHTML += html;

            nomeEmpresa++; // Incrementa o número do nome da empresa
          });
        }


        atualizarExibicao(); // Exibe os resultados iniciais

        const exibirMaisEmpresasBtn = document.getElementById('exibirMaisEmpresasBtn');
        exibirMaisEmpresasBtn.addEventListener('click', function() {
          const novoVetor = [6, 7, 8]; // Novo vetor a ser adicionado
          numeros = [...numeros, ...novoVetor]; // Adiciona o novo vetor ao array "numeros"

          atualizarExibicao(); // Atualiza a exibição dos resultados
        });
      </script>

      <script>
        $(document).ready(function() {
          const abrirModal = $('.abrirModaltel');
          const meuModal = $('#meuModal');
          const fecharModal = $('#fecharModal');

          abrirModal.click(function() {
            meuModal.css('display', 'block');
          });

          fecharModal.click(function() {
            meuModal.css('display', 'none');
          });

          $(window).click(function(event) {
            if (event.target === meuModal[0]) {
              meuModal.css('display', 'none');
            }
          });
        });
      </script>
    </div>
  </div>
  <? include('inc/coluna-lateral.php')?>
</div>
<div class="gallery">
  <? include('inc/gallery-categ.php')?>
</div>
</div>

  <? include('inc/footer.php') ?>
</body>

</html>