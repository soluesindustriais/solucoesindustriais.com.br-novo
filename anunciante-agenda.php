<?php
// Informações da página
$h1 = 'Anunciante - Agenda';
$desc = 'Falta desc';
?>
<?
include('inc/head.php');
?>
<!-- styles -->
<link rel="stylesheet" href="css/cmp-styles.css" />

<!-- media -->
<link rel="stylesheet" href="css/cmp-media.css" />
</head>

<body>
	
	<section class="section header">
        <?php
        include 'inc/menu-interno.php';
        ?>
    </section>
    <div class="container">
		<?=$caminho?>
	</div>

	<div class="container" id="page-container">
		<div class="container" id="page-container">
			<div class="title">
				<h1 class="mb-4"><?=$h1?></h1>
            <? include('inc/menu-fornecedor.php') ?>
<div class="container only-mobile">
						<div class="calendar-container" id="calendar"></div>
	
</div>


				<style>
            
    .calendar-container {
            display: flex;
            flex-wrap: wrap;
            /*justify-content: center;*/
            padding: 10px;
        }

     .day {
    flex-basis: calc(100% / 7);
    padding: 5px;
    border: 1px solid #ccc;
    box-sizing: border-box;
    font-size: 17px;
    text-align: center;
}

        .today {
            background-color: #f0f0f0;
            font-weight: bold;
        }

        .current-day {
            background-color: #00aaff;
            color: white;
            font-weight: bold;
        }

        @media (max-width: 768px) {
            .day {
                flex-basis: calc(100% / 7.5); /* Em telas menores, exibir em duas colunas */
            }
        }
    
    </style>


			</div>
			
			<div class="recieve-include only-desktop">
			
				<div class="row">
					<div class="col-12 col-md-6">
						<div class="calendar-container" id="calendario"></div>

     <script>
         function generateCalendar(year, month) {
            const calendarContainer = document.getElementById('calendario');
            const daysInMonth = new Date(year, month + 1, 0).getDate();
            const firstDayOfWeek = new Date(year, month, 1).getDay();
            const currentDate = new Date();
            const currentYear = currentDate.getFullYear();
            const currentMonth = currentDate.getMonth();
            const currentDay = currentDate.getDate();

            calendarContainer.innerHTML = '';

            // Adicionar os dias da semana no cabeçalho
            const weekDays = ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'];
            for (const dayOfWeek of weekDays) {
                const dayElement = document.createElement('div');
                dayElement.classList.add('day');
                dayElement.textContent = dayOfWeek;
                calendarContainer.appendChild(dayElement);
            }

            // Preencher os dias do mês
            for (let i = 0; i < firstDayOfWeek; i++) {
                const emptyDay = document.createElement('div');
                emptyDay.classList.add('day');
                calendarContainer.appendChild(emptyDay);
            }

            for (let i = 1; i <= daysInMonth; i++) {
                const dayElement = document.createElement('div');
                dayElement.classList.add('day');
                dayElement.textContent = i;

                if (year === currentYear && month === currentMonth && i === currentDay) {
                    dayElement.classList.add('current-day');
                }

                if (year === currentYear && month === currentMonth && i === currentDate.getDate()) {
                    dayElement.classList.add('today');
                }

                calendarContainer.appendChild(dayElement);
            }
        }

        // Obter a data atual
        const currentDate = new Date();
        const currentYear = currentDate.getFullYear();
        const currentMonth = currentDate.getMonth();

        // Gerar o calendário atual
        generateCalendar(currentYear, currentMonth);
    </script>
					</div>

					<div class="col-12 col-md-6">
						<div class="div_info_calendar desk-div-info">


							<h2>
								<span class="dia_numero_span"></span> <span class="mes_completo"></span>
							</h2>
							<br>
							<p class="bold">Nome do evento:</p>
							<p>Data:</p>
							<p>Horário:</p>
							<p>Descrição:</p>
							<br>
							<p>Link:</p>


						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
	
	<?php include 'inc/footer.php' ?>
</body>