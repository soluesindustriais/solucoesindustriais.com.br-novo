<?php
// Informações da página
$h1 = 'Nome do fornecedor';
$desc = 'Falta desc';
?>
<?
include('inc/head.php');
?>
<style>
    .modal {
        display: none;
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background-color: rgba(0, 0, 0, 0.7);
        z-index: 1;
        justify-content: center;
        align-items: center;
    }

    .modal-conteudo {
        background-color: #f4f4f4;
        padding: 20px;
        border-radius: 5px;
        box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.5);
        text-align: center;
        width: 50%;
        height: auto;
        position: absolute;
        top: 30%;
        left: 25%;
    }

    .fechar {
        position: absolute;
        top: 10px;
        right: 10px;
        font-size: 24px;
        cursor: pointer;
    }

    #abrirModal {
        background-color: #007BFF;
        color: #fff;
        border: none;
        padding: 10px 20px;
        border-radius: 5px;
        cursor: pointer;
    }

    #abrirModal:hover {
        background-color: #0056b3;
    }

    .popup-content button {
        display: block;
        margin-top: 10px;
        border: none;
        background: #fff;
        font-weight: 900;
    }


    .popup-overlay {
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background-color: rgba(0, 0, 0, 0.5);
        z-index: 9999;
    }

    .popup-content {
        position: fixed;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        width: 80%;
        max-width: 600px;
        background-color: #fff;
        padding: 20px;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        z-index: 10000;
    }

    .abrirModaltel {
        cursor: pointer;
    }
</style>
<!-- styles -->
<link rel="stylesheet" href="css/cmp-styles.css" />
<!-- media -->
<link rel="stylesheet" href="css/cmp-media.css" />
<link href="https://fonts.googleapis.com/css2?family=League+Spartan:wght@200;400;600;700&display=swap" rel="stylesheet">
</head>

<body>

    <section class="section header">
        <?php
        include 'inc/menu-interno.php';
        ?>
    </section>

    <div class="container">

        <!-- modal -->
        <div class="modal" id="meuModal">
            <div class="modal-conteudo">
                <span class="fechar" id="fecharModal">&times;</span>
                <div class="nomeprod">
                    <div class="row">
                        <div class="col-2"><img src="<?= $url ?>/img/logo.png" alt=""></div>
                        <div class="col-10">
                            <p>Ver contato de Nome da empresa</p>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-6">
                            <div class="infos">
                                <p><span>Cidade: </span>São Paulo - SP</p>
                                <p><span>Responsável:</span> João</p>
                                <p><span>Email:</span> joaozinho@hotmail.com</p>
                                <p><span>Telefone:</span> (11)1234-1234</p>
                                <p><span>Outras opções de contato: botão de Whatsapp</span></p>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-modal">
                                <p>Sem tempo para entrar em contato? <br> Deixe seus dados abaixo:</p>
                                <form action="">
                                    <input type="text" placeholder="Insira seu nome">
                                    <input type="phone" placeholder="Insira seu telefone">
                                    <input type="email" placeholder="Insira seu e-mail">
                                    <input type="submit" value="SOLICITAR CONTATO">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- fim modal -->
        <?= $caminho ?>
        <div class="container" id="page-container">
            <div class="title">
                <h1>Loja do Fornecedor</h1>
            </div>

            <? include('inc/menu-fornecedor.php') ?>
            <div class="mobile-area">
                <?php include 'inc/fornecedor-mobile.php'; ?>
            </div>
            <div class="recieve-include only-desktop">
                <div class="container" id="fornecedor-dados">
                    <div class="flex-header row">
                        <div class="center-img col-12 col-md-4">
                            <div class="">
                                <div class="logo-empresa">
                                    <img src="<?= $url ?>img/logo.png" alt="escrever_aqui" title="escrever_aqui">
                                </div>

                            </div>
                        </div>
                        <div class="flex-header-text col-12 col-md-4">
                            <div class="text-section">
                                <h2 class="ml-dados">Apresentação</h2>
                                <p class="paragrafo-fornecedores" id="paragrafo-apr">Lorem ipsum dolor sit amet sadadasd asd asd asd as daconsectetur adipisicing elit. Provident repudiandae vel perferendis quia eos unde quos id fuga maxime ipsam? Ad quaerat distinctio temporibus necessitatibus deserunt beatae enim voluptate aperiam?</p>
                            </div>
                            <br>
                            <div class="text-section">
                                <h2 class="ml-dados">História</h2>
                                <p class="paragrafo-fornecedores" id="paragrafo-hist">Lorem ipsum dolor sit amet consectetur asdasd elit. Provident repudiandae vel perferend perferendperferend perferend perferend is quia eos unde quos id fuga maxime ipsam? Ad quaerat distinctio temporiasdasdbus necessitatibus dasdasdeserunt basdeatae enim volasdasduptate apeasdasdriam?</p>




                            </div>
                        </div>
                        <div class="flex-header-buttons col-12 col-md-4 align-items-center d-flex flex-wrap">
                            <a class="abrirModaltel" title="escrever_aqui">
                                <div class="">Ver o telefone</div>
                            </a>
                            <a class="abrirModaltel" href="<?= $url ?>mini-site-home" target="_blank" title="escrever_aqui">
                                <div>Visite o site</div>
                            </a>

                        </div>
                    </div>
                </div>

                <div class="status-icons-container row">

                    <div class="col-md-4 col-12">
                        <div class="status-icons">
                            <div class="status-icons-img">
                                <img class="quadrado-azul" src="<?= $url ?>img/pie-chart.png" alt="escrever aqui">
                            </div>
                            <div class="status-icons-text">
                                <h2 class="ml-dados ">Segmento</h2>
                                <p class="paragrafo-fornecedores-sm ">Maquinas e equipamentos</p>
                            </div>
                        </div>
                        <div class="status-icons">
                            <div class="status-icons-img">
                                <img class="quadrado-azul" src="<?= $url ?>img/city.png" alt="escrever aqui">
                            </div>
                            <div class="status-icons-text">
                                <h2 class="ml-dados ">Cidade/Estado</h2>
                                <p class="paragrafo-fornecedores-sm ">São Paulo/SP</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-12 flex-icons">
                        <div class="status-icons">
                            <div class="status-icons-img">
                                <img class="quadrado-azul" src="<?= $url ?>img/filiais.png" alt="escrever aqui">
                            </div>
                            <div class="status-icons-text">
                                <h2 class="ml-dados ">Filiais</h2>
                                <p class="paragrafo-fornecedores-sm ">Não Possui</p>
                            </div>
                        </div>
                        <div class="status-icons">
                            <div class="status-icons-img">
                                <img class="quadrado-azul" src="<?= $url ?>img/selos.png" alt="escrever aqui">
                            </div>
                            <div class="status-icons-text">
                                <h2 class="ml-dados ">Selos</h2>
                                <p class="paragrafo-fornecedores-sm ">Maquinas e equipamentos</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-12 flex-icons">

                        <div class="status-icons">
                            <div class="status-icons-img">
                                <img class="quadrado-azul" src="<?= $url ?>img/certificate.png" alt="escrever aqui">
                            </div>
                            <div class="status-icons-text">
                                <h2 class="ml-dados ">Certificações</h2>
                                <p class="paragrafo-fornecedores-sm ">Não Possui</p>
                            </div>
                        </div>
                        <div class="status-icons">
                            <div class="status-icons-img">
                                <img class="quadrado-azul" src="<?= $url ?>img/star.png" alt="escrever aqui">
                            </div>
                            <div class="status-icons-text">
                                <h2 class="ml-dados ">Avaliações</h2>
                                <div class="rate rate-info stars-collection" id="review">
                                    <span onclick="paintStars0()" class="fa fa-star review-stars checked"></span>
                                    <span onclick="paintStars1()" class="fa fa-star review-stars checked"></span>
                                    <span onclick="paintStars2()" class="fa fa-star review-stars checked"></span>
                                    <span onclick="paintStars3()" class="fa fa-star review-stars"></span>
                                    <span onclick="paintStars4()" class="fa fa-star review-stars"></span>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <h2 class="title-style fs-title text-center mt-5 mb-3">Conquistas</h2>
                <div class="container d-flex justify-content-center mb-4">
                    <img alt="Imagem do Produto" title="Imagem do Produto" src="https://climba.com.br/blog/wp-content/uploads/2019/12/selo-ebit-classificacoes-climba.png">
                </div>
                <hr>
                <div class="container-fluid form-container">
                    <div class="row">
                        <section class="title-brand">

                            <h2 class="text-center title-style fs-title ">Entre em contato</h2>
                        </section>


                        <form class=" col-sm-12 col-lg-8 form-inline form-field" style="    margin: 0 auto;
">

                            <div class=" text-align-center container-fluid w-70 contato-mini-site" style="padding-right: 10px;width: 100%;margin: 0 auto;">
                                <input class="colorize-blue" type="text" name="nome" placeholder="Nome completo*">
                                <input class="colorize-blue" type="text" name="email" placeholder="Seu e-mail*">
                                <input class="colorize-blue" type="text" name="telefone" placeholder="DDD + Telefone*">
                                <textarea class="colorize-blue text-area-block" name="observacoes" cols="30" rows="6" placeholder="Observações"></textarea>
                                <div class="btn-submit-field">
                                    <input class="see-more-btn-anunciantes submit-form-btn-home btn btn-success p-3 " type="submit" value="Enviar mensagem">
                                </div>
                            </div>


                            <p class="camposObrigatorios">* campos obrigatorios</p>



                        </form>

                    </div>
                </div>

            </div>
        </div>
    </div>


    <script>
        function criarPopup(paragrafoId) {
            const paragrafo = document.querySelector(`p#${paragrafoId}`);

            if (paragrafo.textContent.length > 225) {
                const textoCompleto = paragrafo.textContent;
                const textoResumido = textoCompleto.slice(0, 225) + '...';

                paragrafo.innerHTML = textoResumido;

                const lerMaisLink = document.createElement('span');
                lerMaisLink.textContent = 'Ver mais';
                lerMaisLink.className = 'ler-mais-link';

                lerMaisLink.addEventListener('click', () => {
                    const popupContent = document.createElement('div');
                    popupContent.className = 'popup-content';

                    const textoCompletoElemento = document.createElement('p');
                    textoCompletoElemento.textContent = textoCompleto;

                    const fecharBotao = document.createElement('button');
                    fecharBotao.textContent = 'Fechar';

                    fecharBotao.addEventListener('click', () => {
                        document.body.removeChild(overlay);
                        document.body.removeChild(popupContent);
                    });

                    popupContent.appendChild(textoCompletoElemento);
                    popupContent.appendChild(fecharBotao);

                    const overlay = document.createElement('div');
                    overlay.className = 'popup-overlay';

                    document.body.appendChild(overlay);
                    document.body.appendChild(popupContent);

                    overlay.addEventListener('click', () => {
                        document.body.removeChild(overlay);
                        document.body.removeChild(popupContent);
                    });
                });

                paragrafo.appendChild(lerMaisLink);

                // Cria o CSS dinamicamente para estilizar o popup
                const css = `
      .ler-mais-link {
        color: blue;
        cursor: pointer;
      }

      .popup-overlay {
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background-color: rgba(0, 0, 0, 0.5);
        z-index: 9999;
      }

      .popup-content {
        position: fixed;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        background-color: white;
        padding: 20px;
        max-width: 80%;
        max-height: 80%;
        overflow: auto;
        box-shadow: 0 2px 10px rgba(0, 0, 0, 0.2);
        z-index: 10000;
      }
    `;

                const style = document.createElement('style');
                style.textContent = css;

                document.head.appendChild(style);
            }
        }

        criarPopup('paragrafo-hist');
        criarPopup('paragrafo-apr');
        criarPopup('paragrafo-hist-mob');
        criarPopup('paragrafo-apr-mob');
    </script>
    <script>
        $(document).ready(function() {
            const abrirModal = $('.abrirModaltel');
            const meuModal = $('#meuModal');
            const fecharModal = $('#fecharModal');

            abrirModal.click(function() {
                meuModal.css('display', 'block');
            });

            fecharModal.click(function() {
                meuModal.css('display', 'none');
            });

            $(window).click(function(event) {
                if (event.target === meuModal[0]) {
                    meuModal.css('display', 'none');
                }
            });
        });
    </script>
    <?php include 'inc/footer.php' ?>

</body>

</html>