<?php
$h1 = 'Nome Anuncio';
$desc = 'Falta desc';
include 'inc/head.php';

?>
<!-- styles -->
<link rel="stylesheet" href="css/cmp-styles.css" />
<!-- media -->
<link rel="stylesheet" href="css/cmp-media.css" />
<style>
   @font-face {
      font-family: OpenSansBold;
      font-display: fallback;
      src: url('<?= $url ?>fonts/OpenSans/OpenSans-bold.ttf') format("truetype"),
         url('<?= $url ?>fonts/OpenSans/OpenSans-Bold.woff') format("woff");
   }
</style>
</head>

<body>
   <!-- header -->
   <section class="section header">
      <!-- <div class="container"> -->
      <?php
      include 'inc/menu-interno.php';
      ?>
   </section>
   <!-- content category -->

   <div class="container" onload="startTimer()">
      <?= $caminho ?>
      <div class="balao-orcamento">
         <div class="content-balao">
            <h2><?= $h1 ?></h2>
            <img src="<?= $url ?>img/beto-trocar-1.webp" alt="">
         </div>
         <button class="rounded-no-border more-list-fornecedor botao-cotar" id="btn-cotar" title="betoneira">Solicitar Orçamento Grátis</button>
      </div>
      <div class="row prod-topo-row">
         <div class="produto-branding align-items-start">
            <div class="row">
               <h1 class="mt-2 d-block d-sm-none title-style"><?= $h1 ?></h1>
               <div class="info-anuncio d-none-desk"><span onclick="curtirProduto(this)" class="fa fa-heart " style="
                        width: 15%;
                        "></span>
                  <span>Novo</span>
                  <span class="produto-codigo-desk">COD 00112233</span>
               </div>
               <div class="produto-img-field divider-branding col-12 col-md-8">
                  <div class="rate rate-info stars-collection">
                     <span onclick="paintStars0()" class="fa fa-star review-stars"></span>
                     <span onclick="paintStars1()" class="fa fa-star review-stars"></span>
                     <span onclick="paintStars2()" class="fa fa-star review-stars"></span>
                     <span onclick="paintStars3()" class="fa fa-star review-stars"></span>
                     <span onclick="paintStars4()" class="fa fa-star review-stars"></span>
                  </div>
                  <br>
                  <div id="carouselExampleInterval" class="carousel slide" data-ride="carousel">
                     <div class="slider-count">
                        <p id="slider-count">1/3</p>
                     </div>
                     <div class="row">
                        <div class="mini-thumbs col-2">
                           <div class="col-3">
                              <img id="image-box-1" onclick="changeImageBox(this)" src="<?= $url ?>img/beto-trocar-1.webp" alt="<?= $h1 ?>" title="<?= $h1 ?>">
                           </div>
                           <div class="col-3">
                              <img id="image-box-2" onclick="changeImageBox(this)" src="<?= $url ?>img/beto-trocar-2.webp" alt="<?= $h1 ?>" title="<?= $h1 ?>">
                           </div>
                           <div class="col-3">
                              <img id="image-box-3" onclick="changeImageBox(this)" src="<?= $url ?>img/beto-trocar-3.webp" alt="<?= $h1 ?>" title="<?= $h1 ?>">
                           </div>
                           <div class="col-3">
                              <img id="image-box-4" onclick="changeImageBox(this)" src="<?= $url ?>img/beto-trocar-4.webp" alt="<?= $h1 ?>" title="<?= $h1 ?>">
                           </div>
                        </div>
                        <div class="col-10">
                           <div class="slider-container">
                              <ul class="logo-carousel-topo slider p-0">
                                 <li class="slide">
                                    <img id="slider-95" class="img-principal" alt="<?= $h1 ?>" title="<?= $h1 ?>" onclick="openLightBox1(this)" src="<?= $url ?>/img/beto-trocar-1.webp">
                                 </li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
                  <script>
                     function changeImageBox(element) {
                        const src = element.src;
                        const sliderImage = document.getElementById('slider-95');
                        sliderImage.src = src;
                     }
                  </script>
                  <script>
                     function curtirProduto(x) {
                        x.classList.toggle("coracao-curtido");
                     }
                  </script>
                  <div>
                     <div class="container p-0 ">
                        <div class="LeiaMais">
                           <h3>Descrição</h3>
                           <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quod, eaque fugit aperiam non iste officia veniam, a, quis hic perferendis aut doloribus repudiandae? Praesentium nulla necessitatibus rem doloribus, sit, ea. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Repellendus nostrum nam doloribus dicta nulla voluptates sed eaque deserunt aliquid rem ipsum sunt, dignissimos tempore. Maiores, esse fugiat ducimus recusandae similique. </p>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="produto-orcamento-desk col-md-4 col-12 divider-branding">
                  <div class="container">
                     <div class="row cotacao-row">
                        <div class="info-anuncio d-none-mobile"><span onclick="curtirProduto(this)" class="fa fa-heart " style="
                        width: 15%;
                        "></span>
                           <span>Novo</span>
                           <span class="produto-codigo-desk">COD 00112233</span>
                        </div>
                        <h1 class="d-none-mobile"><?= $h1 ?></h1>
                        <div class="informacoes">
                           <div class="orcamento-info">
                              <p>Litros</p>
                              <span>100L</span>
                              <span>100L</span>
                              <span>100L</span>
                           </div>
                           <div class="orcamento-info">
                              <p>Comprimento</p>
                              <span>10M</span>
                              <span>100M</span>
                              <span>1000M</span>
                           </div>
                           <div class="orcamento-info">
                              <p>Cor</p>
                              <span>Azul</span>
                              <span>Vermelho</span>
                              <span>Amarelo</span>
                           </div>
                           <a href="<?= $url ?>mini-site-home">
                              <div class="orcamento-info">
                                 <div class="orcamento-info-img">
                                    <i class="fa fa-bullhorn" aria-hidden="true"></i>

                                 </div>
                                 <div class="orcameto-info-txt">
                                    <h2 class="orcamento-info-desc">Anunciante</h2>

                                    <p class="p-0">Mini site do Anunciante</p>

                                 </div>
                              </div>
                           </a>
                           <div class="orcamento-info">
                              <div class="orcamento-info-img">
                                 <img src="img/colombia.png" alt="Palavra-chave" title="Palavra-chave Soluções Industriais">
                              </div>
                              
                              <div class="orcameto-info-txt">
                                 <h2 class="orcamento-info-desc">Região de atendimento</h2>
                                 <p class="p-0">Maquinas e equipamentos</p>
                              </div>
                           </div>
                           <div class="orcamento-info">
                              <div class="orcamento-info-img">
                                 <img src="img/category.png" alt="Palavra-chave" title="Palavra-chave Soluções Industriais">
                              </div>
                              <div class="orcameto-info-txt">
                                 <h2 class="orcamento-info-desc">Categoria</h2>
                                 <p class="paragrafo-fornecedores-sm  p-0">Maquinas e equipamentos</p>
                              </div>
                           </div>
                           <div class="orcamento-info">
                              <div class="orcamento-info-img">
                                 <img src="img/subfolder.png" alt="Palavra-chave" title="Palavra-chave Soluções Industriais">
                              </div>
                              <div class="orcameto-info-txt">
                                 <h2 class="orcamento-info-desc">Sub Categoria</h2>
                                 <p class="paragrafo-fornecedores-sm  p-0">Maquinas e equipamentos</p>
                              </div>
                           </div>
                        </div>
                        <div class="estoque-info">
                           <p>Quantidade: <b id="quantidade">1</b> (3000 disponíveis) </p>
                           <img id="select-more" onclick="selectLess()" src="img/quantidade-menos.png" alt="Ver mais" title="Ver mais">
                           <img id="select-less" onclick="selectMore()" src="img/quantidade-mais.png" alt="Ver mais" title="Ver mais">
                        </div>
                        <div class="btn-orcamento-field">
                           <button class="rounded-no-border more-list-fornecedor botao-cotar" id="btn-cotar" title="betoneira">Solicitar Orçamento</button>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="container mt-3" style="background: #fff;
      padding: 25px;border-radius: 6px;  background-color: #fff;   box-shadow: rgba(60, 55, 56, 0.07) 0px 0px 0px 1px, rgba(60, 55, 56, 0.07) 0px 0px 1px 0px, rgba(60, 55, 56, 0.07) 0px 2px 2px 0px;border: 1px solid #eee;
      ">
         <div class="row prod-topo-row">
            <div class="opnioes-field">
               <!--             <div class="tags tags-desk">
               <button class="main-focus" onclick="switchTagProdutos()">Produtos Vistos</button>
               <button onclick="switchTagGaleria()">Galeria</button>
               <button onclick="switchTagAvaliacoes()">Avaliacoes</button>
               </div> -->
               <div class="tags tags-desk">
                  <button id="btn-produtos" class="main-focus" onclick="switchTag('produtos')">Produtos Vistos</button>
                  <button id="btn-galeria" onclick="switchTag('galeria')">Galeria</button>
                  <button id="btn-avaliacao" onclick="switchTag('avaliacao')">Avaliações</button>
               </div>
               <div class="opinion-list">
                  <div class="tag-avaliacao-desk col-12" id="tag-avaliacao">
                     <div class="row">
                        <div class="tag-avaliacao-nota col-md-5 col-12">

                           <div class="avaliacoes-bar">
                              <div class="opinioes descricao-produto">
                                 <h2 class="prod-descript">Opiniões sobre o produto</h2>
                                 <div class="nota-field">
                
                                    <div>
                                       <div class="rate rate-info stars-collection">
                                          <span class="fa fa-star star-checked"></span>
                                          <span class="fa fa-star star-checked"></span>
                                          <span class="fa fa-star star-checked"></span>
                                          <span class="fa fa-star star-checked"></span>
                                          <span class="fa fa-star star-checked"></span>
                                       </div>
                                       <br>
                                       <p><span id="nota">4.8</span> Média entre 320 opiniões</p>
                                    </div>
                                 </div>
                              </div>
                              <div class="avaliacao-item">
                                 <p>5</p>
                                 <span class="fa fa-star star dark-star"></span>
                                 <span class="gray-bar"><span class="golden-bar"></span></span>
                                 <p>4</p>
                              </div>
                              <div class="avaliacao-item">
                                 <p>4</p>
                                 <span class="fa fa-star star dark-star"></span>
                                 <span class="gray-bar"><span class="golden-bar high-width"></span></span>
                                 <p>5</p>
                              </div>
                              <div class="avaliacao-item">
                                 <p>3</p>
                                 <span class="fa fa-star star dark-star"></span>
                                 <span class="gray-bar"><span class="golden-bar mid-width"></span></span>
                                 <p>2</p>
                              </div>
                              <div class="avaliacao-item">
                                 <p>2</p>
                                 <span class="fa fa-star star dark-star"></span>
                                 <span class="gray-bar"><span class="golden-bar no-width"></span></span>
                                 <p>0</p>
                              </div>
                              <div class="avaliacao-item">
                                 <p>1</p>
                                 <span class="fa fa-star star dark-star"></span>
                                 <span class="gray-bar"><span class="golden-bar no-width"></span></span>
                                 <p>0</p>
                              </div>
                           </div>
                           <br>

                        </div>
                        <div class="tag-avaliacao-opinioes col-12 col-md-7">
                           <div class="opinion-filter">
                              <p>1-3 de 12 Avalicacoes</p>
                           </div>
                           <div class="opinion">
                              <div class="rate rate-info stars-collection">
                                 <span class="fa fa-star star-checked"></span>
                                 <span class="fa fa-star star-checked"></span>
                                 <span class="fa fa-star star-checked"></span>
                                 <span class="fa fa-star star-checked"></span>
                                 <span class="fa fa-star star-checked"></span>
                                 <b class="opinion-username">Usuario - SP</b>
                              </div>
                              <h2 class="opinion-title">Qualidade e resistência </h2>
                              <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nemo aliquid, nobis fugit dolorem architecto voluptatibus commodi! Aspernatur natus unde sunt provident placeat consequuntur deleniti eveniet, praesentium voluptatem, doloribus, quos nostrum. Lorem ipsum dolor sit amet consectetur adipisicing elit. Nemo aliquid, nobis fugit dolorem architecto voluptatibus commodi! Aspernatur natus unde sunt provident placeat consequuntur deleniti eveniet, praesentium voluptatem, doloribus, quos nostrum.</p>
                           </div>
                           <div class="opinion">
                              <div class="rate rate-info stars-collection">
                                 <span class="fa fa-star star-checked"></span>
                                 <span class="fa fa-star star-checked"></span>
                                 <span class="fa fa-star star-checked"></span>
                                 <span class="fa fa-star star-checked"></span>
                                 <span class="fa fa-star star-checked"></span>
                                 <b class="opinion-username">Usuario - SP</b>
                              </div>
                              <h2 class="opinion-title">Qualidade e resistência </h2>
                              <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nemo aliquid, nobis fugit dolorem architecto voluptatibus commodi! Aspernatur natus unde sunt provident placeat consequuntur deleniti eveniet, praesentium voluptatem, doloribus, quos nostrum. Lorem ipsum dolor sit amet consectetur adipisicing elit. Nemo aliquid, nobis fugit dolorem architecto voluptatibus commodi! Aspernatur natus unde sunt provident placeat consequuntur deleniti eveniet, praesentium voluptatem, doloribus, quos nostrum.</p>
                           </div>
                           <div class="opinion">
                              <div class="rate rate-info stars-collection">
                                 <span class="fa fa-star star-checked"></span>
                                 <span class="fa fa-star star-checked"></span>
                                 <span class="fa fa-star star-checked"></span>
                                 <span class="fa fa-star star-checked"></span>
                                 <span class="fa fa-star star-checked"></span>
                                 <b class="opinion-username">Usuario - SP</b>
                              </div>
                              <h2 class="opinion-title">Qualidade e resistência </h2>
                              <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nemo aliquid, nobis fugit dolorem architecto voluptatibus commodi! Aspernatur natus unde sunt provident placeat consequuntur deleniti eveniet, praesentium voluptatem, doloribus, quos nostrum. Lorem ipsum dolor sit amet consectetur adipisicing elit. Nemo aliquid, nobis fugit dolorem architecto voluptatibus commodi! Aspernatur natus unde sunt provident placeat consequuntur deleniti eveniet, praesentium voluptatem, doloribus, quos nostrum.</p>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="tag-galeria-desk col-12" id="tag-galeria">
                     <div class="container">
                        <div class="row">
                           <div class="">
                              <div class="">
                                 <div class="logo-carousel-produtos">
                                    <a href="<?= $url ?>img/produtos/prod-1.jpg" class="lightbox" title="<?= $h1 ?>"><img src="<?= $url ?>/img/produtos/prod-1.jpg" alt="<?= $h1 ?>" title="<?= $h1 ?>"></a>
                                    <!-- <p class="alt-produto">texto</p> -->
                                    <a href="<?= $url ?>img/produtos/prod-2.jpg" class="lightbox" title="<?= $h1 ?>"><img src="<?= $url ?>/img/produtos/prod-2.jpg" alt="<?= $h1 ?>" title="<?= $h1 ?>"></a>
                                    <!-- <p class="alt-produto">texto</p> -->
                                    <a href="<?= $url ?>img/produtos/prod-3.jpg" class="lightbox" title="<?= $h1 ?>"><img src="<?= $url ?>/img/produtos/prod-3.jpg" alt="<?= $h1 ?>" title="<?= $h1 ?>"></a>
                                    <!-- <p class="alt-produto">texto</p> -->
                                    <a href="<?= $url ?>img/produtos/prod-4.jpg" class="lightbox" title="<?= $h1 ?>"><img src="<?= $url ?>/img/produtos/prod-4.jpg" alt="<?= $h1 ?>" title="<?= $h1 ?>"></a>
                                    <!-- <p class="alt-produto">texto</p> -->
                                    <a href="<?= $url ?>img/produtos/prod-5.jpg" class="lightbox" title="<?= $h1 ?>"><img src="<?= $url ?>/img/produtos/prod-5.jpg" alt="<?= $h1 ?>" title="<?= $h1 ?>"></a>
                                    <!-- <p class="alt-produto">texto</p> -->
                                    <a href="<?= $url ?>img/produtos/prod-6.jpg" class="lightbox" title="<?= $h1 ?>"><img src="<?= $url ?>/img/produtos/prod-6.jpg" alt="<?= $h1 ?>" title="<?= $h1 ?>"></a>
                                    <!-- <p class="alt-produto">texto</p> -->
                                    <a href="<?= $url ?>img/produtos/prod-7.jpg" class="lightbox" title="<?= $h1 ?>"><img src="<?= $url ?>/img/produtos/prod-7.jpg" alt="<?= $h1 ?>" title="<?= $h1 ?>"></a>
                                    <!-- <p class="alt-produto">texto</p> -->
                                    <a href="<?= $url ?>img/produtos/prod-1.jpg" class="lightbox" title="<?= $h1 ?>"><img src="<?= $url ?>/img/produtos/prod-1.jpg" alt="<?= $h1 ?>" title="<?= $h1 ?>"></a>
                                    <!-- <p class="alt-produto">texto</p> -->
                                    <a href="<?= $url ?>img/produtos/prod-2.jpg" class="lightbox" title="<?= $h1 ?>"><img src="<?= $url ?>/img/produtos/prod-2.jpg" alt="<?= $h1 ?>" title="<?= $h1 ?>"></a>
                                    <!-- <p class="alt-produto">texto</p> -->
                                    <a href="<?= $url ?>img/produtos/prod-3.jpg" class="lightbox" title="<?= $h1 ?>"><img src="<?= $url ?>/img/produtos/prod-3.jpg" alt="<?= $h1 ?>" title="<?= $h1 ?>"></a>
                                    <!-- <p class="alt-produto">texto</p> -->
                                    <a href="<?= $url ?>img/produtos/prod-4.jpg" class="lightbox" title="<?= $h1 ?>"><img src="<?= $url ?>/img/produtos/prod-4.jpg" alt="<?= $h1 ?>" title="<?= $h1 ?>"></a>
                                    <!-- <p class="alt-produto">texto</p> -->
                                    <a href="<?= $url ?>img/produtos/prod-5.jpg" class="lightbox" title="<?= $h1 ?>"><img src="<?= $url ?>/img/produtos/prod-5.jpg" alt="<?= $h1 ?>" title="<?= $h1 ?>"></a>
                                    <!-- <p class="alt-produto">texto</p> -->
                                    <a href="<?= $url ?>img/produtos/prod-6.jpg" class="lightbox" title="<?= $h1 ?>"><img src="<?= $url ?>/img/produtos/prod-6.jpg" alt="<?= $h1 ?>" title="<?= $h1 ?>"></a>
                                    <!-- <p class="alt-produto">texto</p> -->
                                    <a href="<?= $url ?>img/produtos/prod-7.jpg" class="lightbox" title="<?= $h1 ?>"><img src="<?= $url ?>/img/produtos/prod-7.jpg" alt="<?= $h1 ?>" title="<?= $h1 ?>"></a>
                                    <!-- <p class="alt-produto">texto</p> -->
                                 </div>
                              </div>
                              <p class="gallery-disclaimer">
                                 Estas imagens foram obtidas em bancos de imagens públicas e disponíveis livremente na internet
                              </p>
                           </div>
                        </div>
                     </div>
                  </div>
                  <ul class="resultado-produtos miniSite" id="tag-produtos">
                     <? if ($isMobile == false) {
                     ?>
                        <li class="produto-lista ">
                           <i onclick="curtirProduto(this)" class="fa fa-heart "></i>
                           <img alt="Imagem do Produto" title="Imagem do Produto" src="img/pallet.png">
                           <a href="<?= $url ?>anuncio" class="nome-do-produto" title="Pallet de madeira">Pallet Liso Madeira Pinus Aparelhado 1200x200cm Settis</a>
                           <!-- <span class="codigo-do-produto">Cód. 123456789</span> -->
                           <div class="avaliacao-produto pb-3">
                              <span class="fa fa-star checked"></span>
                              <span class="fa fa-star checked"></span>
                              <span class="fa fa-star checked"></span>
                              <span class="fa fa-star checked"></span>
                              <span class="fa fa-star"></span>
                              <span>(23)</span>
                           </div>
                           <button class="btn-orcamento text-center btn-cotar botao-cotar">SOLICITE UM ORÇAMENTO GRÁTIS</button>
                        </li>
                        <li class="produto-lista ">
                           <i onclick="curtirProduto(this)" class="fa fa-heart "></i>
                           <!-- <span class="produto-em-destaque d-none d-sm-block">Destaque</span> -->
                           <img alt="Imagem do Produto" title="Imagem do Produto" src="img/pallet.png">
                           <a href="<?= $url ?>anuncio" class="nome-do-produto" title="Pallet de madeira">Pallet Liso Madeira Pinus Aparelhado 1200x200cm Settis</a>
                           <!-- <span class="codigo-do-produto">Cód. 123456789</span> -->
                           <div class="avaliacao-produto pb-3">
                              <span class="fa fa-star checked"></span>
                              <span class="fa fa-star checked"></span>
                              <span class="fa fa-star checked"></span>
                              <span class="fa fa-star checked"></span>
                              <span class="fa fa-star"></span>
                              <span>(23)</span>
                           </div>
                           <button class="btn-orcamento text-center btn-cotar botao-cotar">SOLICITE UM ORÇAMENTO GRÁTIS</button>
                        </li>
                        <li class="produto-lista ">
                           <i onclick="curtirProduto(this)" class="fa fa-heart "></i>
                           <img alt="Imagem do Produto" title="Imagem do Produto" src="img/pallet.png">
                           <a href="<?= $url ?>anuncio" class="nome-do-produto" title="Pallet de madeira">Pallet Liso Madeira Pinus Aparelhado 1200x200cm Settis</a>
                           <!-- <span class="codigo-do-produto">Cód. 123456789</span> -->
                           <div class="avaliacao-produto pb-3">
                              <span class="fa fa-star checked"></span>
                              <span class="fa fa-star checked"></span>
                              <span class="fa fa-star checked"></span>
                              <span class="fa fa-star checked"></span>
                              <span class="fa fa-star"></span>
                              <span>(23)</span>
                           </div>
                           <button class="btn-orcamento text-center btn-cotar botao-cotar">SOLICITE UM ORÇAMENTO GRÁTIS</button>
                        </li>
                        <li class="produto-lista ">
                           <i onclick="curtirProduto(this)" class="fa fa-heart "></i>
                           <img alt="Imagem do Produto" title="Imagem do Produto" src="img/pallet.png">
                           <a href="<?= $url ?>anuncio" class="nome-do-produto" title="Pallet de madeira">Pallet Liso Madeira Pinus Aparelhado 1200x200cm Settis</a>
                           <!-- <span class="codigo-do-produto">Cód. 123456789</span> -->
                           <div class="avaliacao-produto pb-3">
                              <span class="fa fa-star checked"></span>
                              <span class="fa fa-star checked"></span>
                              <span class="fa fa-star checked"></span>
                              <span class="fa fa-star checked"></span>
                              <span class="fa fa-star"></span>
                              <span>(23)</span>
                           </div>
                           <button class="btn-orcamento text-center btn-cotar botao-cotar">SOLICITE UM ORÇAMENTO GRÁTIS</button>
                        </li> <?
                           } else {
                              ?>
                        <li class="produto-lista ">
                           <i onclick="curtirProduto(this)" class="fa fa-heart "></i>
                           <img alt="Imagem do Produto" title="Imagem do Produto" src="img/pallet.png">
                           <a href="<?= $url ?>anuncio" class="nome-do-produto" title="Pallet de madeira">Pallet Liso Madeira Pinus Aparelhado 1200x200cm Settis</a>
                           <!-- <span class="codigo-do-produto">Cód. 123456789</span> -->
                           <div class="avaliacao-produto pb-3">
                              <span class="fa fa-star checked"></span>
                              <span class="fa fa-star checked"></span>
                              <span class="fa fa-star checked"></span>
                              <span class="fa fa-star checked"></span>
                              <span class="fa fa-star"></span>
                              <span>(23)</span>
                           </div>
                           <button class="btn-orcamento text-center btn-cotar botao-cotar">SOLICITE UM ORÇAMENTO GRÁTIS</button>
                        </li>
                        <li class="produto-lista ">
                           <i onclick="curtirProduto(this)" class="fa fa-heart "></i>
                           <span class="produto-em-destaque d-none d-sm-block">Destaque</span>
                           <img alt="Imagem do Produto" title="Imagem do Produto" src="img/pallet.png">
                           <a href="<?= $url ?>anuncio" class="nome-do-produto" title="Pallet de madeira">Pallet Liso Madeira Pinus Aparelhado 1200x200cm Settis</a>
                           <!-- <span class="codigo-do-produto">Cód. 123456789</span> -->
                           <div class="avaliacao-produto pb-3">
                              <span class="fa fa-star checked"></span>
                              <span class="fa fa-star checked"></span>
                              <span class="fa fa-star checked"></span>
                              <span class="fa fa-star checked"></span>
                              <span class="fa fa-star"></span>
                              <span>(23)</span>
                           </div>
                           <button class="btn-orcamento text-center btn-cotar botao-cotar">SOLICITE UM ORÇAMENTO GRÁTIS</button>
                        </li>
                     <?
                           } ?>


                  </ul>
               </div>
            </div>
         </div>
      </div>
      <script>
         function switchTag(tag) {
            var tags = ['produtos', 'galeria', 'avaliacao'];

            tags.forEach(function(t) {
               var btn = document.getElementById('btn-' + t);
               var tagContent = document.getElementById('tag-' + t);

               if (t === tag) {
                  btn.classList.add('main-focus');
                  tagContent.style.display = 'flex';
               } else {
                  btn.classList.remove('main-focus');
                  tagContent.style.display = 'none';
               }
            });
         }
      </script>
      <div class="container produto-specs-desk col-12" style="background: #fff;
      padding: 25px; border-radius: 6px;  background-color: #fff;   box-shadow: rgba(60, 55, 56, 0.07) 0px 0px 0px 1px, rgba(60, 55, 56, 0.07) 0px 0px 1px 0px, rgba(60, 55, 56, 0.07) 0px 2px 2px 0px;border: 1px solid #eee;
      ">
         <div class="row">
            <div class="descricao-produto col-12 col-md-7">
               <h2 class="prod-descript produto-specs-subtitle">Especificações técnicas</h2>
               <br>
               <table>
                  <tr class="bg-gray-blue">
                     <td class="left-width">Produto</td>
                     <td>Pallet</td>
                  </tr>
                  <tr class="no-border-b">
                     <td class="left-width">Tipo</td>
                     <td>Liso</td>
                  </tr>
                  <tr class="no-border-b bg-gray-blue">
                     <td class="left-width">Largura</td>
                     <td>100cm</td>
                  </tr>
                  <tr class="no-border-b">
                     <td class="left-width">Comprimento</td>
                     <td>100cm</td>
                  </tr>
                  <tr class="no-border-b bg-gray-blue">
                     <td class="left-width">Espessura</td>
                     <td>14 cm</td>
                  </tr>
                  <tr class="no-border-b">
                     <td class="left-width">Dimensao</td>
                     <td>100x100</td>
                  </tr>
                  <tr class="no-border-b bg-gray-blue">
                     <td class="left-width">Material</td>
                     <td>Madeira</td>
                  </tr>
                  <tr class="no-border-b">
                     <td class="left-width">Tipo de Material</td>
                     <td>Pinus</td>
                  </tr>
                  <tr class="no-border-b bg-gray-blue">
                     <td class="left-width">Madeira do Produto</td>
                     <td>Pinus</td>
                  </tr>
                  <tr class="no-border-b">
                     <td class="left-width">Acabamento</td>
                     <td>Cru</td>
                  </tr>
                  <tr class="no-border-b bg-gray-blue">
                     <td class="left-width">Cor</td>
                     <td>Bege</td>
                  </tr>
                  <tr class="no-border-b">
                     <td class="left-width">Tonalidade</td>
                     <td>Bege</td>
                  </tr>
                  <tr class="no-border-b bg-gray-blue">
                     <td class="left-width">Marca</td>
                     <td>xxxxx</td>
                  </tr>
               </table>
               <br>
               <div class="container p-0 ">
                  <div class="LeiaMais">
                     <h3>Descrição</h3>
                     <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quod, eaque fugit aperiam non iste officia veniam, a, quis hic perferendis aut doloribus repudiandae? Praesentium nulla necessitatibus rem doloribus, sit, ea. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Repellendus nostrum nam doloribus dicta nulla voluptates sed eaque deserunt aliquid rem ipsum sunt, dignissimos tempore. Maiores, esse fugiat ducimus recusandae similique. </p>
                  </div>
               </div>
            </div>
            <div class="info-anunciante col-12 col-md-5">
               <div class="informacoes">
                  <h2 class="prod-descript produto-specs-subtitle">Informacoes do anunciante</h2>
                  <br>
                  <div class="orcamento-info">
                     <div class="orcamento-info-img">
                     <i class="fa fa-bullhorn" aria-hidden="true"></i>
                     </div>
                     <div class="orcameto-info-txt">
                        <h2 class="orcamento-info-desc">Anunciante</h2>

                                    <p class="p-0">Mini site do Anunciante</p>

                                 </div>
                  </div>
                  <div class="orcamento-info">
                     <div class="orcamento-info-img">
                        <img src="img/colombia.png" alt="">
                     </div>
                     
                     <div class="orcameto-info-txt">
                        <h2 class="orcamento-info-desc">Região de atendimento</h2>
                        <p class="p-0">Maquinas e equipamentos</p>
                     </div>
                  </div>
                  <div class="orcamento-info">
                     <div class="orcamento-info-img">
                        <img src="img/category.png" alt="">
                     </div>
                     <div class="orcameto-info-txt">
                        <h2 class="orcamento-info-desc">Categoria</h2>
                        <p class="paragrafo-fornecedores-sm  p-0">Maquinas e equipamentos</p>
                     </div>
                  </div>
                  <div class="orcamento-info">
                     <div class="orcamento-info-img">
                        <img src="img/subfolder.png" alt="">
                     </div>
                     <div class="orcameto-info-txt">
                        <h2 class="orcamento-info-desc">Sub Categoria</h2>
                        <p class="paragrafo-fornecedores-sm  p-0">Maquinas e equipamentos</p>
                     </div>
                  </div>
                  <div class="orcamento-info">
                     <div class="orcamento-info-img">
                        <img src="img/telefone-fixo.png" alt="Palavra-chave" title="Palavra-chave Soluções Industriais">
                     </div>
                     <div class="orcameto-info-txt">
                        <h2 class="orcamento-info-desc">Contato Comercial</h2>
                        <!-- <p class="paragrafo-fornecedores-sm  p-0">(11)912... <span id="telefone">(11)1234-1234</span> <button id="verMais">Ver Mais</button></p> -->
                        <p class="paragrafo-fornecedores-sm  p-0"><span id="trechoTelefone">(11)123...</span><span class="telefone-completo">(11)1234-1234</span> <button id="verMaisTelProd">Ver Mais</button></p>
                        <script>
                           const verMaisButton = document.getElementById('verMaisTelProd');
                           const trechoTelefoneSpan = document.getElementById('trechoTelefone');
                           const telefoneCompletoSpan = document.querySelector('.telefone-completo');

                           verMaisButton.addEventListener('click', () => {
                              trechoTelefoneSpan.style.display = 'none';
                              telefoneCompletoSpan.style.display = 'inline';
                              verMaisButton.style.display = 'none';
                           });
                        </script>
                     </div>
                  </div>
                  <img alt="Imagem do Produto" title="Imagem do Produto" src="https://climba.com.br/blog/wp-content/uploads/2019/12/selo-ebit-classificacoes-climba.png">
                  <div class="btn-orcamento-field">
                     <a href="<?= $url ?>mini-site-home" title="Mais informações do vendedor" class="rounded-no-border more-list-fornecedor-desk botao-cotar more-info-btn">
                        Mais informações do vendedor
                        <!-- <button class="" >Mais informacoes do vendedor</button> -->
                     </a>
                  </div>
               </div>
            </div>
         </div>

      </div>






   </div>
   <!-- footer -->
   <?php include 'inc/footer.php'; ?>
   <!-- BOOTSTRAP @5.3.2 -->
   <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
   <!-- SWIPER JS @11.0.0 -->
   <script src="https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.js"></script>
   <!-- functions -->
   <script src="js/functions.js"></script>

   <script>
      $(document).ready(function() {
         $(window).on('scroll', function() {
            $('.balao-orcamento').stop();
            if ($(this).scrollTop() >= 1010) {
               $('.balao-orcamento').animate({
                  bottom: "0"
               }, 1000);
            } else {
               $('.balao-orcamento').animate({
                  bottom: "-100%"
               }, 1000);
            }
         });
      });
   </script>
   <?php include 'inc/fancy.php' ?>
</body>

</html>