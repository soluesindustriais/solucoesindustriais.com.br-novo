<?php
$h1 = 'Pesquisa de satisfação';
$desc = 'Falta desc';
include 'inc/head.php';
?>
<!-- styles -->
<link rel="stylesheet" href="css/cmp-styles.css" />

<!-- media -->
<link rel="stylesheet" href="css/cmp-media.css" />
</head>

<body>

    <!-- header -->
    <section class="section header">
        <!-- <div class="container"> -->
        <?php
        include 'inc/menu-interno.php';
        ?>
    </section>

    <!-- content category -->
    <section class="section content">
        <div class="container-xxl">
            <?= $caminho ?>
            <div class="row text-center">
                <div class="col">
                    <br /><br /><br /><br /><br /><br /><br /><br />
                    :: PESQUISA SATISFAÇÃO ::
                    <br /><br />

                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#surveyModal">
                        abrir popup
                    </button>

                    <br /><br /><br /><br /><br /><br /><br /><br />

                    <!-- MODAL -->
                    <div class="modal defaultModal" id="surveyModal" tabindex="-1">
                        <div class="modal-dialog modal-dialog-centered modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">
                                        Pesquisa de satisfação
                                    </h5>
                                    <!-- <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button> -->
                                </div>
                                <div class="modal-body">
                                    <p>
                                        Avalie o contato feito em Muito Satisfeito, Satisfeito, Neutro, Insatisfeito e Muito Insatisfeito.
                                    </p>
                                    <ul class="surveyOptions">
                                        <li>
                                            <a href="#" title="Muito Insatisfeito">
                                                <img src="img/survey-01.png" alt="Muito Insatisfeito" />
                                                <strong>
                                                    Muito Insatisfeito
                                                </strong>
                                                <em>
                                                    1
                                                </em>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" title="Insatisfeito">
                                                <img src="img/survey-02.png" alt="Insatisfeito" />
                                                <strong>
                                                    Insatisfeito
                                                </strong>
                                                <em>
                                                    2
                                                </em>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" title="Neutro">
                                                <img src="img/survey-03.png" alt="Neutro" />
                                                <strong>
                                                    Neutro
                                                </strong>
                                                <em>
                                                    3
                                                </em>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" title="Satisfeito">
                                                <img src="img/survey-04.png" alt="Satisfeito" />
                                                <strong>
                                                    Satisfeito
                                                </strong>
                                                <em>
                                                    4
                                                </em>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" title="Muito Satisfeito">
                                                <img src="img/survey-05.png" alt="Muito Satisfeito" />
                                                <strong>
                                                    Muito Satisfeito
                                                </strong>
                                                <em>
                                                    5
                                                </em>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-success btn-fill btn-blue" data-bs-dismiss="modal" title="Cancelar">
                                        Cancelar
                                    </button>
                                    <button type="button" class="btn btn-success" title="Enviar avaliação">
                                        Enviar avaliação
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /MODAL -->
                </div>
            </div>
        </div>
    </section>

    <!-- footer -->
    <?php include 'inc/footer.php'; ?>

    <!-- BOOTSTRAP @5.3.2 -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>

    <!-- SWIPER JS @11.0.0 -->
    <script src="https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.js"></script>

    <!-- functions -->
    <script src="js/functions.js"></script>

</body>

</html>