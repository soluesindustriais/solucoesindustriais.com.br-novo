<?php
// Informações da página
$h1 = 'Anunciante - Produtos';
$desc = 'Falta desc';
include 'inc/head.php';
?>
<!-- styles -->
<link rel="stylesheet" href="css/cmp-styles.css" />

<!-- media -->
<link rel="stylesheet" href="css/cmp-media.css" />
</head>
<body>
    <section class="section header">
    <? include 'inc/menu-interno.php'; ?>

</section>
    <div class="container">
    <?=$caminho?>
        
    </div>
    <div class="container" id="page-container">
        <div class="container">
            <div class="title">
                <h1>Produtos do Fornecedor</h1>
                
            </div>
            
            <? include ('inc/menu-fornecedor.php')?>
          
            <div class="recieve-include only-desktop">
            <div class="container" id="fornecedor-produtos">

    <div class="produtos-fornecedor ">
        <div class="produtos-fornecedor-content row">
            <div class="produtos-fornecedor-img col-12 col-md-2 d-flex align-items-center justify-content-center">
                <img src="https://via.placeholder.com/350x350" alt="escrever_aqui" title="escrever_aqui" class="img-fluid">
            </div>
            <div class="produtos-fornecedor-text col-12 col-md-8">
                <h2>Nome do produto</h2>
                <p class="m-0 cinza-fonte bold ">Categoria/Subcategoria:</p>
                <p class="m-0 cinza-fonte bold">Descrição: </p>
                 <div class="rate rate-info stars-collection">
                    <span  class="fa fa-star star-checked"></span>
                    <span  class="fa fa-star star-checked"></span>
                    <span  class="fa fa-star star-checked"></span>
                    <span  class="fa fa-star star-checked"></span>
                    <span  class="fa fa-star star-checked"></span>
                </div>
                <div class="produtos-fornecedor-orcamento">

                    <p class="m-0 cinza-forte ">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ex, et. Quidem facilis voluptatum repellendus dignissimos quo repellat voluptas quae atque a, quasi tenetur facere blanditiis itaque vitae tempore mollitia corrupti.</p>

                </div>
            </div>
            <div class="col-12 col-md-2 d-flex align-items-center justify-content-center">
                <button class="btn btn-success">
                        Solicitar orçamento
                    </button>
            </div>

        </div>
    </div>
    <hr class="line-prod">
     <div class="produtos-fornecedor ">
        <div class="produtos-fornecedor-content row">
            <div class="produtos-fornecedor-img col-12 col-md-2 d-flex align-items-center justify-content-center">
                <img src="https://via.placeholder.com/350x350" alt="escrever_aqui" title="escrever_aqui" class="img-fluid">
            </div>
            <div class="produtos-fornecedor-text col-12 col-md-8">
                <h2>Nome do produto</h2>
                <p class="m-0 cinza-fonte bold ">Categoria/Subcategoria:</p>
                <p class="m-0 cinza-fonte bold">Descrição: </p>
                 <div class="rate rate-info stars-collection">
                    <span  class="fa fa-star star-checked"></span>
                    <span  class="fa fa-star star-checked"></span>
                    <span  class="fa fa-star star-checked"></span>
                    <span  class="fa fa-star star-checked"></span>
                    <span  class="fa fa-star star-checked"></span>
                </div>
                <div class="produtos-fornecedor-orcamento">

                    <p class="m-0 cinza-forte ">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ex, et. Quidem facilis voluptatum repellendus dignissimos quo repellat voluptas quae atque a, quasi tenetur facere blanditiis itaque vitae tempore mollitia corrupti.</p>

                </div>
            </div>
            <div class="col-12 col-md-2 d-flex align-items-center justify-content-center">
                <button class="btn btn-success">
                        Solicitar orçamento
                    </button>
            </div>

        </div>
    </div>
    
    <hr class="line-prod">
     <div class="produtos-fornecedor ">
        <div class="produtos-fornecedor-content row">
            <div class="produtos-fornecedor-img col-12 col-md-2 d-flex align-items-center justify-content-center">
                <img src="https://via.placeholder.com/350x350" alt="escrever_aqui" title="escrever_aqui" class="img-fluid">
            </div>
            <div class="produtos-fornecedor-text col-12 col-md-8">
                <h2>Nome do produto</h2>
                <p class="m-0 cinza-fonte bold ">Categoria/Subcategoria:</p>
                <p class="m-0 cinza-fonte bold">Descrição: </p>
                 <div class="rate rate-info stars-collection">
                    <span  class="fa fa-star star-checked"></span>
                    <span  class="fa fa-star star-checked"></span>
                    <span  class="fa fa-star star-checked"></span>
                    <span  class="fa fa-star star-checked"></span>
                    <span  class="fa fa-star star-checked"></span>
                </div>
                <div class="produtos-fornecedor-orcamento">

                    <p class="m-0 cinza-forte ">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ex, et. Quidem facilis voluptatum repellendus dignissimos quo repellat voluptas quae atque a, quasi tenetur facere blanditiis itaque vitae tempore mollitia corrupti.</p>

                </div>
            </div>
            <div class="col-12 col-md-2 d-flex align-items-center justify-content-center">
                <button class="btn btn-success">
                        Solicitar orçamento
                    </button>
            </div>

        </div>
    </div>
    
    <hr class="line-prod">
     <div class="produtos-fornecedor ">
        <div class="produtos-fornecedor-content row">
            <div class="produtos-fornecedor-img col-12 col-md-2 d-flex align-items-center justify-content-center">
                <img src="https://via.placeholder.com/350x350" alt="escrever_aqui" title="escrever_aqui" class="img-fluid">
            </div>
            <div class="produtos-fornecedor-text col-12 col-md-8">
                <h2>Nome do produto</h2>
                <p class="m-0 cinza-fonte bold ">Categoria/Subcategoria:</p>
                <p class="m-0 cinza-fonte bold">Descrição: </p>
                 <div class="rate rate-info stars-collection">
                    <span  class="fa fa-star star-checked"></span>
                    <span  class="fa fa-star star-checked"></span>
                    <span  class="fa fa-star star-checked"></span>
                    <span  class="fa fa-star star-checked"></span>
                    <span  class="fa fa-star star-checked"></span>
                </div>
                <div class="produtos-fornecedor-orcamento">

                    <p class="m-0 cinza-forte ">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ex, et. Quidem facilis voluptatum repellendus dignissimos quo repellat voluptas quae atque a, quasi tenetur facere blanditiis itaque vitae tempore mollitia corrupti.</p>

                </div>
            </div>
            <div class="col-12 col-md-2 d-flex align-items-center justify-content-center">
                <button class="btn btn-success">
                        Solicitar orçamento
                    </button>
            </div>

        </div>
    </div>
    <hr class="line-prod">
        <div class="mais-categorias-field">
    <p class="text-center exibir-mais">
    <button class="mais-categorias-btn">Exibir mais produtos de <?=$fornecedor?></button>
    </p>
</div>
    <div class="clear"></div>
</div></div>
</div>
</div>
    </div>
    
    
    <?php include 'inc/footer.php' ?>
</body>