<?php
// Informações da página
$h1 = 'Anunciante - Notícias';
$desc = 'Falta desc';
?>
<?
include('inc/head.php');
?>
<!-- styles -->
<link rel="stylesheet" href="css/cmp-styles.css" />

<!-- media -->
<link rel="stylesheet" href="css/cmp-media.css" />
<style>
	.noticia {
    border: 1px solid rgb(229, 229, 229);
    border-radius: 8px;
    padding: 15px !important;
    margin: 20px 0px;
}

.noticia h4 {
    font-size: 19px;
    padding-left: 12px;
    color: #012f73;
    font-weight: bold;
}

.noticia p {
    font-size: 14px;
    color: gray;
}

.notice-btn {
    background-color: transparent;
    color: #012f73;
    font-size: 14px;
    font-weight: bold;
}

.noticia-main {
    padding: 10px;
    padding-left: 10px !important;
    padding-right: 10px !important;
    margin-bottom: 30px;
}

.noticia-main p {
    text-align: justify;
}

.noticia-main h1 {
    margin-bottom: 20px;
    color: #012f73;
    font-weight: 700;
}

.noticia-main img {
    width: 50%;
    margin-bottom: 20px;
    box-shadow: 1px 7px 11px #3333338f;
}

#noticia-link {
    text-decoration: none;
    color: #000;
}
</style>
</head>
<body>
			<section class="section header">
        <?php
        include 'inc/menu-interno.php';
        ?>
    </section>
	<div class="container">
		<?=$caminho?>
	</div>
	<div class="container" id="page-container">
			<div class="title">
				<h1><?=$h1?></h1>
 
			</div>
			
			<? include ('inc/menu-fornecedor.php')?>

			<div class="recieve-include ">
			<div class="container" id="fornecedor-noticias">


	<div class="row noticias-list">
		<div class="noticia desk-news">
			<h4>Prêmio Destaque Nacional 2021</h4>
			<p>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odio, totam! Saepe harum explicabo ab pariatur velit fugiat eius id! Eius laborum unde vel omnis aperiam. Unde consequuntur voluptates accusamus enimonsectetur adipisicing elit. Odio, totam! Saepe harum explicabo ab pariatur velit fugiat eius id! Eius laborum unde vel omnis aperiam. Unde consequuntur voluptates accusamus enim ur adipisicing elit. Odio, totam! Saepe harum explicabo ab pariatur velit fugiat eius id! Eius laborum ur adipisicing elit. Odio, totam! Saepe harum explicabo ab pariatur velit fugiat eius id! Eius laborum
			</p>
			<a href="<?$url?><?= $url ?>noticia" title="escrever_aqui" class="btn-noticias">Ver mais...</a>
		</div>
		<div class="noticia desk-news">
			<h4>Expô 2022</h4>
			<p>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odio, totam! Saepe harum explicabo ab pariatur velit fugiat eius id! Eius laborum unde vel omnis aperiam. Unde consequuntur voluptates accusamus enimnsectetur adipisicing elit. Odio, totam! Saepe harum explicabo ab pariatur velit fugiat eius id! Eius laborum unde vel omnis aperiam. Unde consequuntur voluptates accusamus enim ur adipisicing elit. Odio, totam! Saepe harum explicabo ab pariatur velit fugiat eius id! Eius laborum ur adipisicing elit. Odio, totam! Saepe harum explicabo ab pariatur velit fugiat eius id! Eius laborum
			</p>
			<a href="<?$url?><?= $url ?>noticia" title="escrever_aqui" class="btn-noticias">Ver mais...</a>
		</div>
		<div class="noticia desk-news">
			<h4>Expô 2022</h4>
			<p>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odio, totam! Saepe harum explicabo ab pariatur velit fugiat eius id! Eius laborum unde vel omnis aperiam. Unde consequuntur voluptates accusamus enimnsectetur adipisicing elit. Odio, totam! Saepe harum explicabo ab pariatur velit fugiat eius id! Eius laborum unde vel omnis aperiam. Unde consequuntur voluptates accusamus enim ur adipisicing elit. Odio, totam! Saepe harum explicabo ab pariatur velit fugiat eius id! Eius laborum ur adipisicing elit. Odio, totam! Saepe harum explicabo ab pariatur velit fugiat eius id! Eius laborum
			</p>
			<a href="<?$url?><?= $url ?>noticia" title="escrever_aqui" class="btn-noticias">Ver mais...</a>
		</div>
		<div class="noticia desk-news">
			<h4>Expô 2022</h4>
			<p>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odio, totam! Saepe harum explicabo ab pariatur velit fugiat eius id! Eius laborum unde vel omnis aperiam. Unde consequuntur voluptates accusamus enimnsectetur adipisicing elit. Odio, totam! Saepe harum explicabo ab pariatur velit fugiat eius id! Eius laborum unde vel omnis aperiam. Unde consequuntur voluptates accusamus enim ur adipisicing elit. Odio, totam! Saepe harum explicabo ab pariatur velit fugiat eius id! Eius laborum ur adipisicing elit. Odio, totam! Saepe harum explicabo ab pariatur velit fugiat eius id! Eius laborum
			</p>
			<a href="<?$url?><?= $url ?>noticia" title="escrever_aqui" class="btn-noticias">Ver mais...</a>
		</div>

		<div class="mais-categorias-field">
			<button class="mais-categorias-btn more-list-fornecedor">Exibir mais notícias</button>
		</div>

	</div>

</div></div>
</div>
</div>
	</div>
	
	
	<?php include 'inc/footer.php' ?>
</body>