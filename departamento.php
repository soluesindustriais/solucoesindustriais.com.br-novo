<?php
// Informações da página
$h1 = 'Lista de departamentos';
$desc = 'Falta desc';
include('inc/head.php');
?>
<!-- styles -->
<link rel="stylesheet" href="css/cmp-styles.css" />

<!-- media -->
<link rel="stylesheet" href="css/cmp-media.css" />
</head>

<body>
    <section class="section header">
        <!-- <div class="container"> -->
        <?php
        include 'inc/menu-interno.php';
        ?>
    </section>
    <div class="container">
        <?= $caminho ?>
        <h1 class="list-h1 title-style p-3"><?=$h1?></h1>
        <div class="container align-items-center " id="categ-grade-1">
            <ul class="row CatFix mx-auto">
                <li class="categoria-item col-lg-3 col-sm-12">
                    <a href="<?=$url?>departamento-madeira">
                        <div class="categoria">
                            <img src="<?= $url ?>img/pallet.png" alt="Nome produto" title="Nome Produto - Nome projeto">
                            <div>
                                <b>Departamentos</b>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="categoria-item col-lg-3 col-sm-12">
                    <a href="produto">
                        <div class="categoria">
                            <img src="<?= $url ?>img/pallet.png" alt="Nome produto" title="Nome Produto - Nome projeto">
                            <div>
                                <b>Abrasivo</b>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="categoria-item col-lg-3 col-sm-12">
                    <a href="produto">
                        <div class="categoria">
                            <img src="<?= $url ?>img/pallet.png" alt="Nome produto" title="Nome Produto - Nome projeto">
                            <div>
                                <b>Abrasivo</b>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="categoria-item col-lg-3 col-sm-12">
                    <a href="produto">
                        <div class="categoria">
                            <img src="<?= $url ?>img/pallet.png" alt="Nome produto" title="Nome Produto - Nome projeto">
                            <div>
                                <b>Abrasivo</b>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="categoria-item col-lg-3 col-sm-12">
                    <a href="produto">
                        <div class="categoria">
                            <img src="<?= $url ?>img/pallet.png" alt="Nome produto" title="Nome Produto - Nome projeto">
                            <div>
                                <b>Abrasivo</b>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="categoria-item col-lg-3 col-sm-12">
                    <a href="produto">
                        <div class="categoria">
                            <img src="<?= $url ?>img/pallet.png" alt="Nome produto" title="Nome Produto - Nome projeto">
                            <div>
                                <b>Abrasivo</b>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="categoria-item col-lg-3 col-sm-12">
                    <a href="produto">
                        <div class="categoria">
                            <img src="<?= $url ?>img/pallet.png" alt="Nome produto" title="Nome Produto - Nome projeto">
                            <div>
                                <b>Abrasivo</b>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="categoria-item col-lg-3 col-sm-12">
                    <a href="produto">
                        <div class="categoria">
                            <img src="<?= $url ?>img/pallet.png" alt="Nome produto" title="Nome Produto - Nome projeto">
                            <div>
                                <b>Abrasivo</b>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="categoria-item col-lg-3 col-sm-12">
                    <a href="produto">
                        <div class="categoria">
                            <img src="<?= $url ?>img/pallet.png" alt="Nome produto" title="Nome Produto - Nome projeto">
                            <div>
                                <b>Abrasivo</b>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="categoria-item col-lg-3 col-sm-12">
                    <a href="produto">
                        <div class="categoria">
                            <img src="<?= $url ?>img/pallet.png" alt="Nome produto" title="Nome Produto - Nome projeto">
                            <div>
                                <b>Abrasivo</b>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="categoria-item col-lg-3 col-sm-12">
                    <a href="produto">
                        <div class="categoria">
                            <img src="<?= $url ?>img/pallet.png" alt="Nome produto" title="Nome Produto - Nome projeto">
                            <div>
                                <b>Abrasivo</b>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="categoria-item col-lg-3 col-sm-12">
                    <a href="produto">
                        <div class="categoria">
                            <img src="<?= $url ?>img/pallet.png" alt="Nome produto" title="Nome Produto - Nome projeto">
                            <div>
                                <b>Abrasivo</b>
                            </div>
                        </div>
                    </a>
                </li>
            </ul>
        </div>

    </div>
    <div class="container">

        <div class="container resultados">
            <b id="results">233 </b>
            <p>resultados</p>
        </div>


        <div class="mais-categorias-field">
            <button id="mais-categorias" class="mais-categorias-btn" onclick="moreCategDesk()">Exibir mais categorias</button>
        </div>
    </div>


    <script>
        function adicionarElementosCategorias() {
            var containerCategorias = document.querySelector('#categ-grade-1 .CatFix');

            if (containerCategorias) {
                for (var i = 0; i < 4; i++) {
                    var liCategoria = document.createElement('li');
                    liCategoria.className = 'categoria-item col-lg-3 col-sm-12';

                    var aElement = document.createElement('a');
                    aElement.href = 'produto';

                    var divCategoria = document.createElement('div');
                    divCategoria.className = 'categoria';

                    var imgElement = document.createElement('img');
                    imgElement.src = '<?= $url ?>img/pallet.png';
                    imgElement.alt = 'Nome produto';
                    imgElement.title = 'Nome Produto - Nome projeto';

                    var divNomeProduto = document.createElement('div');
                    var bNomeProduto = document.createElement('b');
                    bNomeProduto.textContent = 'Abrasivo';

                    divNomeProduto.appendChild(bNomeProduto);
                    divCategoria.appendChild(imgElement);
                    divCategoria.appendChild(divNomeProduto);
                    aElement.appendChild(divCategoria);
                    liCategoria.appendChild(aElement);
                    containerCategorias.appendChild(liCategoria);
                }
            } else {
                console.error("Elemento 'categ-grade-1 .CatFix' não encontrado.");
            }
        }

        function moreCategDesk() {
            adicionarElementosCategorias();
        }

        document.addEventListener('DOMContentLoaded', function() {
            var botaoMaisCategorias = document.querySelector('#mais-categorias');
            if (botaoMaisCategorias) {
                botaoMaisCategorias.addEventListener('click', moreCategDesk);
            }
        });
    </script>


    <? include('inc/footer.php') ?>

</body>

</html>