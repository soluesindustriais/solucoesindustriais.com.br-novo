<div class="container" id="fornecedor-dados">
    <div class="flex-header col-12">
        <div class="center-img col-4">
            <div class="">
                <div class="logo-empresa">
                    <img src="assets/imagens/solucs/S1.png" alt="escrever_aqui" title="escrever_aqui">
                </div>
            </div>
        </div>
        <div class="flex-header-text col-4">
            <section class="text-section">
                <h2 class="ml-dados">Apresentação</h2>
                <p class="paragrafo-fornecedores">Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident repudiandae vel perferendis quia eos unde quos id fuga maxime ipsam? Ad quaerat distinctio temporibus necessitatibus deserunt beatae enim voluptate aperiam?</p>
            </section>
            <br>
            <section class="text-section">
                <h2 class="ml-dados">História</h2>
                <p class="paragrafo-fornecedores">Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident repudiandae vel perferendis quia eos unde quos id fuga maxime ipsam? Ad quaerat distinctio temporibus necessitatibus deserunt beatae enim voluptate aperiam?</p>
            </section>
        </div>
        <div class="flex-header-buttons col-4">
            <a href="<?$url?><?= $url ?>" title="escrever_aqui">
                <div>Ver o telefone</div>
            </a>
            <a href="<?$url?><?= $url ?>" title="escrever_aqui">
                <div>Visite o site</div>
            </a>

        </div>
    </div>

    <div class="status-icons-container">

        <div class="col-4">
            <div class="status-icons">
                <div class="status-icons-img">
                    <img class="quadrado-azul" src="assets/imagens/icons/pie-chart.png">
                </div>
                <div class="status-icons-text">
                    <h2 class="ml-dados ">Segmento</h2>
                    <p class="paragrafo-fornecedores-sm ">Maquinas e equipamentos</p>
                </div>
            </div>
            <div class="status-icons">
                <div class="status-icons-img">
                    <img class="quadrado-azul" src="assets/imagens/icons/city.png">
                </div>
                <div class="status-icons-text">
                    <h2 class="ml-dados ">Cidade/Estado</h2>
                    <p class="paragrafo-fornecedores-sm ">São Paulo/SP</p>
                </div>
            </div>
        </div>

        <div class="col-4 flex-icons">
            <div class="status-icons">
                <div class="status-icons-img">
                    <img class="quadrado-azul" src="assets/imagens/icons/filiais.png">
                </div>
                <div class="status-icons-text">
                    <h2 class="ml-dados ">Filiais</h2>
                    <p class="paragrafo-fornecedores-sm ">Não Possui</p>
                </div>
            </div>
            <div class="status-icons">
                <div class="status-icons-img">
                    <img class="quadrado-azul" src="assets/imagens/icons/selos.png">
                </div>
                <div class="status-icons-text">
                    <h2 class="ml-dados ">Selos</h2>
                    <p class="paragrafo-fornecedores-sm ">Maquinas e equipamentos</p>
                </div>
            </div>
        </div>

        <div class="col-4 flex-icons">

            <div class="status-icons">
                <div class="status-icons-img">
                    <img class="quadrado-azul" src="assets/imagens/icons/certificate.png">
                </div>
                <div class="status-icons-text">
                    <h2 class="ml-dados ">Certificações</h2>
                    <p class="paragrafo-fornecedores-sm ">Não Possui</p>
                </div>
            </div>
            <div class="status-icons">
                <div class="status-icons-img">
                    <img class="quadrado-azul" src="assets/imagens/icons/star.png">
                </div>
                <div class="status-icons-text">
                    <h2 class="ml-dados ">Avaliações</h2>
                    <p class="paragrafo-fornecedores-sm ">
                    <div class="rate rate-info stars-collection" id="review">
                        <span onclick="paintStars0()" class="fa fa-star review-stars checked"></span>
                        <span onclick="paintStars1()" class="fa fa-star review-stars checked"></span>
                        <span onclick="paintStars2()" class="fa fa-star review-stars checked"></span>
                        <span onclick="paintStars3()" class="fa fa-star review-stars"></span>
                        <span onclick="paintStars4()" class="fa fa-star review-stars"></span>
                    </div>
                    </p>
                </div>
            </div>

        </div>
    </div>

    <div class="container-fluid form-container">
        <div class="row">
            <section class="title-brand">

                <h1 class="title-brand">Entre em contato</h1>
            </section>


            <form class=" col-sm-12 col-lg-8 form-inline form-field" style="    margin: 0 auto;
">

                <div class=" text-align-center container-fluid w-70 contato-mini-site" style="padding-right: 10px;width: 100%;margin: 0 auto;">
                    <input class="colorize-blue" type="text" name="nome" placeholder="Nome completo*">
                    <input class="colorize-blue" type="text" name="email" placeholder="Seu e-mail*">
                    <input class="colorize-blue" type="text" name="telefone" placeholder="DDD + Telefone*">
                    <textarea class="colorize-blue text-area-block" name="" id="" cols="30" rows="6" placeholder="Observações"></textarea>
                </div>


                <p class="camposObrigatorios">* campos obrigatorios</p>



            </form>
            <section class="btn-submit-field">
                <input class="see-more-btn-anunciantes submit-form-btn-home" type="submit" value="Enviar mensagem">
            </section>
        </div>
    </div>

</div>