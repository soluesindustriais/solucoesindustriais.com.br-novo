<?php 
$h1 = 'Fornecedor';
$desc = 'Falta desc';
    include'inc/head.php';
 ?>
</head>
<body class="pageSeller">

    <!-- header -->
    <section class="section header">
        <!-- <div class="container"> -->
            <div class="container-xxl">
        <?php 
            include'inc/menu.php';
         ?>
         <div class="row">
                <div class="col-6">
                    <span>
                        Aumente sua Visibilidade, Contatos e Resultados
                    </span>
                    <h1 class="title">
                        Soluções Industriais para Fornecedores Aumente sua Visibilidade, Contatos e Resultados
                    </h1>
                    <p>
                        Anuncie na maior plataforma B2B da América Latina, com mais de um milhão de usuários mensais. Relacione-se com compradores, aumente a visibilidade online da sua marca e receba oportunidades de negócios para o seu segmento.
                    </p>
                    <a href="#" class="btn btn-success" title="Quero anunciar">
                        Quero anunciar
                    </a>
                </div>
                <div class="col-6">
                    <img src="img/img-results-01.jpg" alt="Aumente sua Visibilidade, Contatos e Resultados"/>
                </div>
            </div>
     </div>
    </section>

    <!-- steps default -->
    <section class="section stepsDefault">
        <div class="container-xxl">
            <div class="row">
                <div class="col-md-4">
                    <span>
                        1
                    </span>
                    <p>
                        <strong>
                            Anuncie
                        </strong>
                        Divulgue seus produtos e serviços na plataforma que tem mais de 1 milhão de acessos
                    </p>
                </div>
                <div class="col-md-4">
                    <span>
                        2
                    </span>
                    <p>
                        <strong>
                            Aumente a visibilidade
                        </strong>
                        Tenha sua marca divulgada em mais de 100 canais online
                    </p>
                </div>
                <div class="col-md-4">
                    <span>
                        3
                    </span>
                    <p>
                        <strong>
                            Negocie
                        </strong>
                        Receba contatos comerciais segmentados para a sua área de atuação
                    </p>
                </div>
            </div>
        </div>
    </section>

    <!-- infosDefault -->
    <section class="section infosDefault">
        <div class="container-xxl">
            <div class="row">
                <div class="col">
                    <h3 class="title">
                        Divulgue produtos e serviços
                    </h3>
                    <h6>
                        O Soluções Industriais tem uma carteira de mais de 30 mil negociações mensais. São mais de 3 milhões de negócios fechados de acordo com a segmentação de compradores por área de atuação.
                    </h6>
                </div>
                <div class="col-6">
                    <img src="img/img-share.png" alt="Divulgue produtos e serviços"/>
                </div>
            </div>
        </div>
    </section>

    <!-- infosDefault -->
    <section class="section infosDefault color">
        <div class="container-xxl">
            <div class="row">
                <div class="col-6">
                    <img src="img/img-view.png" alt="Negocie com compradores qualificados"/>
                </div>
                <div class="col">
                    <h3 class="title">
                        Negocie com compradores qualificados
                    </h3>
                    <h6>
                        Os compradores industriais buscam pelas mais diversas necessidades. Para facilitar o gerenciamento dos contatos comerciais, o Soluções Industriais oferece CRM integrado. E você ainda pode interagir com seus clientes diretamente pelo chat da plataforma.
                    </h6>
                </div>
            </div>
        </div>
    </section>

    <!-- infosDefault -->
    <section class="section infosDefault">
        <div class="container-xxl">
            <div class="row">
                <div class="col">
                    <h3 class="title">
                        Aumente sua visibilidade online
                    </h3>
                    <h6>
                        Com conteúdos relevantes e informativos sobre os produtos e serviços, conquistamos mais de 500 mil usuários mensais por meio de estratégias digitais personalizadas para a sua empresa.
                    </h6>
                </div>
                <div class="col-6">
                    <img src="img/img-business.png" alt="Aumente sua Visibilidade, Contatos e Resultados"/>
                </div>
            </div>
        </div>
    </section>

    <!-- boxBrands -->
    <section class="section boxBrands">
        <div class="container-xxl">
            <div class="row text-center">
                <div class="col">
                    <h3 class="title">
                        Quem compra no Soluções Industriais
                    </h3>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <!-- brands gallery -->
                    <div class="swiper brandsGallery">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <a href="#" title="Gerdau">
                                    <img src="img/ico-gerdau.png" alt="Gerdau"/>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="#" title="Prefeitura São Paulo">
                                    <img src="img/ico-prefeitura-sp.png" alt="Prefeitura São Paulo"/>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="#" title="Petrobras">
                                    <img src="img/ico-petrobras.png" alt="Petrobras"/>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="#" title="Vale">
                                    <img src="img/ico-vale.png" alt="Vale"/>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="#" title="USP">
                                    <img src="img/ico-usp.png" alt="USP"/>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="#" title="Odebrecht">
                                    <img src="img/ico-odebrecht.png" alt="Odebrecht"/>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="#" title="Ambev">
                                    <img src="img/ico-ambev.png" alt="Ambev"/>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="#" title="Mahle">
                                    <img src="img/ico-mahle.png" alt="Mahle"/>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="#" title="Marinha do Brasil">
                                    <img src="img/ico-marinha-brasil.png" alt="Marinha do Brasil"/>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="#" title="General Motors">
                                    <img src="img/ico-gm.png" alt="General Motors"/>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="#" title="WEG">
                                    <img src="img/ico-weg.png" alt="WEG"/>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="#" title="Saint Gobain">
                                    <img src="img/ico-saint-gobain.png" alt="Saint Gobain"/>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- boxComments -->
    <section class="section boxComments">
        <div class="container-xxl">
            <div class="row text-center">
                <div class="col">
                    <h6 class="title">
                        Depoimentos
                    </h6>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <!-- comments gallery -->
                    <div class="swiper commentsGallery">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <p>
                                    Com a aquisição da plataforma aumentamos o volume de orçamentos e como consequência aumentamos as vendas. A plataforma deu visibilidade nacional para a Áttollo. O objetivo na aquisição da plataforma era aumentar vendas e faturamento, o que aconteceu com o amadurecimento do site e o modo como operamos. Nossa percepção é que a plataforma ajuda na visibilidade da empresa em sites de buscas e nos coloca na primeira página do google.
                                </p>
                                <span>
                                    <b>
                                        Alexandre
                                    </b>
                                    Attollo Elevadores
                                </span>
                            </div>
                            <div class="swiper-slide">
                                <p>
                                    Ajudou com a ampliação de nossa visibilidade junto ao mundo digital e consecutivamente ajudando com a evolução de nossa marca. Nossas expectativas foram alcançadas com a chegada das cotações nos possibilitando nosso aumento da carteira e  nos mantende satisfeito com o projeto até a presente data.
                                </p>
                                <span>
                                    <b>
                                        Sandro
                                    </b>
                                    Petroval
                                </span>
                            </div>
                            <div class="swiper-slide">
                                <p>
                                    O projeto nos ajudou a alcançar um público maior, atendendo nossas expectativas iniciais. O cliente tem a facilidade de nos encontrar e conhecer nossos produtos. O portal de controle de orçamentos é bem dinâmico e de fácil acesso.
                                </p>
                                <span>
                                    <b>
                                        Edson
                                    </b>
                                    Eginox
                                </span>
                            </div>
                            <div class="swiper-slide">
                                <p>
                                    Com a aquisição da plataforma aumentamos o volume de orçamentos e como consequência aumentamos as vendas. A plataforma deu visibilidade nacional para a Áttollo. O objetivo na aquisição da plataforma era aumentar vendas e faturamento, o que aconteceu com o amadurecimento do site e o modo como operamos. Nossa percepção é que a plataforma ajuda na visibilidade da empresa em sites de buscas e nos coloca na primeira página do google.
                                </p>
                                <span>
                                    <b>
                                        Alexandre
                                    </b>
                                    Attollo Elevadores
                                </span>
                            </div>
                            <div class="swiper-slide">
                                <p>
                                    Ajudou com a ampliação de nossa visibilidade junto ao mundo digital e consecutivamente ajudando com a evolução de nossa marca. Nossas expectativas foram alcançadas com a chegada das cotações nos possibilitando nosso aumento da carteira e  nos mantende satisfeito com o projeto até a presente data.
                                </p>
                                <span>
                                    <b>
                                        Sandro
                                    </b>
                                    Petroval
                                </span>
                            </div>
                            <div class="swiper-slide">
                                <p>
                                    O projeto nos ajudou a alcançar um público maior, atendendo nossas expectativas iniciais. O cliente tem a facilidade de nos encontrar e conhecer nossos produtos. O portal de controle de orçamentos é bem dinâmico e de fácil acesso.
                                </p>
                                <span>
                                    <b>
                                        Edson
                                    </b>
                                    Eginox
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- boxFind -->
    <section class="section boxFind">
        <div class="container-xxl">
            <div class="row text-center">
                <div class="col">
                    <h5>
                        O sucesso é feito de escolhas e o Soluções Industriais é a melhor delas para o futuro do seu negócio.
                    </h5>
                    <a href="#" class="btn btn-success blue" title="Cadastre-se">
                        Cadastre-se
                    </a>
                </div>
            </div>
        </div>
    </section>

    <!-- footer -->
    <?php 
        include'inc/footer.php';
     ?>

    <!-- BOOTSTRAP @5.3.2 -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>

    <!-- SWIPER JS @11.0.0 -->
    <script src="https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.js"></script>

    <!-- functions -->
    <script src="js/functions.js"></script>

</body>
</html>
