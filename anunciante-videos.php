<?php
// Informações da página
$h1 = 'Anunciante - Vídeos';
$desc = 'Falta desc';
?>
<?
include('inc/head.php');
include('inc/fancy.php');
?>
<!-- styles -->
<link rel="stylesheet" href="css/cmp-styles.css" />

<!-- media -->
<link rel="stylesheet" href="css/cmp-media.css" />
<style>
    /* LAZY LOAD VIDEO */
[data-video] { cursor: pointer; position: relative; background-color: #000;}
[data-video] img {opacity: 0.6;}
/*.ytvideo[data-video]::before { content: ''; position: absolute; top: 0; left: 0; height: 100%; width: 100%; background: rgba(0, 0, 0, 0.4); cursor: pointer; }*/
.ytvideo[data-video] a::after {
 content: '\f04b';
    position: absolute;
    z-index: 1;
    top: 50%;
    left: 50%;
    transform: translateX(-50%) translateY(-50%);
    font: 27px/normal FontAwesome;
    color: #fff;
    /* text-shadow: 0px 0px 50px white; */
    -webkit-transition: .3s;
    transition: .3s;
    width: 79px;
    border-radius: 17px;
    display: flex;
    align-items: center;
    justify-content: center;
    padding: 10px;}
[data-video]:hover a::after { color: red; }

</style>
</head>
<body>
      <section class="section header">
        <!-- <div class="container"> -->
        <?php
        include 'inc/menu-interno.php';
        ?>
    </section>
    <div class="container">
        <?=$caminho?>
    </div>
    <div class="container" id="page-container">
        <div class="container" id="page-container">
            <div class="title">
                <h1><?=$h1?></h1>
                
            </div>
            
            <? include ('inc/menu-fornecedor.php')?>
            <div class="recieve-include ">
                <div class="container" id="fornecedor-videos">
    <div class="row videos-list">
        <div class="col-md-4 col-12">
            <div class="video-host border-video-block border-video">
                <div data-video="https://www.youtube.com/watch?v=xxbK6yYpaz4"></div>
                
                <h5>Titulo</h5>
                <p>Descrição</p>
            </div>

            <div class="video-host border-video-block border-video" >
                <div data-video="https://www.youtube.com/watch?v=ndQcnOhkLXw"></div>
                
                <h5>Titulo</h5>
                <p>Descrição</p>
            </div>
            <div class="video-host border-video-block border-video" >
                <div data-video="https://www.youtube.com/watch?v=ndQcnOhkLXw"></div>
                <h5>Titulo</h5>
                <p>Descrição</p>
            </div>
        </div>
        <div class="col-md-4 col-12">
            <div class="video-host border-video-block border-video" >
                <div data-video="https://www.youtube.com/watch?v=ndQcnOhkLXw"></div>
                <h5>Titulo</h5>
                <p>Descrição</p>
            </div>
            <div class="video-host border-video-block border-video" >
                <div data-video="https://www.youtube.com/watch?v=ndQcnOhkLXw"></div>
                <h5>Titulo</h5>
                <p>Descrição</p>
            </div>
            <div class="video-host border-video-block border-video" >
                <div data-video="https://www.youtube.com/watch?v=ndQcnOhkLXw"></div>
                <h5>Titulo</h5>
                <p>Descrição</p>
            </div>
        </div>
        <div class="col-md-4 col-12">
            <div class="video-host border-video-block border-video" >
                <div data-video="https://www.youtube.com/watch?v=ndQcnOhkLXw"></div>
                <h5>Titulo</h5>
                <p>Descrição</p>
            </div>
            <div class="video-host border-video-block border-video" >
                <div data-video="https://www.youtube.com/watch?v=ndQcnOhkLXw"></div>
                <h5>Titulo</h5>
                <p>Descrição</p>
            </div>
            <div class="video-host border-video-block border-video" >
                <div data-video="https://www.youtube.com/watch?v=ndQcnOhkLXw"></div>
                <h5>Titulo</h5>
                <p>Descrição</p>
            </div>
        </div>
    </div>

    <div class="mais-categorias-field">
        <button class="mais-categorias-btn more-list-fornecedor">Exibir mais vídeos</button>
    </div>





</div>
</div>
</div>
</div>
    </div>

   <script>
    // INSTRUÇÕES
    // Faça a chama deste arquivo via include no footer da página em questão
    // Ex.: include('inc/ytvideo.php')
    // Insira o atributo 'data-video' e atribua a ele a URL do vídeo
    // Ex.: <div data-video="https://www.youtube.com/watch?v=iONPHwtYoTU"></div>
    // FIM INSTRUÇÕES
    
    // Armazena todos os placeholders para os iframes numa variável.
    const videos = new Array();
    const videoUrl = new Array();
    $('[data-video]').each(function(){
        videos.push($(this));
        videoUrl.push($(this).attr('data-video'));
    });
    // Defina aqui asclasses a serem adicionadas aos iframes.
    const videoClasses = [
      'fwidth',
    ]
    // Cria a thumbnail dos vídeos nas divs criadas.
    window.addEventListener("load", () => {
      for(let i = 0; i < videos.length; i++) {
        // Cria a imagem de thumbnail
        // Adiciona a classe ytvideo para que seja exibido o botão do YouTube
        $(videos[i]).addClass('ytvideo');
        let image = document.createElement('img');
        let link = document.createElement('a');
        // Define o src da imagem.
        image.src = `https://img.youtube.com/vi/${videoUrl[i].split('=')[1]}/0.jpg`
        // Gera o tamanho da width considerando o tamanho da div pai.
        let width = $(videos[i]).width();
        // Define a height da imagem com matemática
        image.height = (width / 16) * 9;
        // Implicita o valor da width pois caso não expecificado o video redimensiona
        image.width = width;
        // Define o alt e title da imagem
        image.alt = "Confira mais um vídeo da <?=$nomeSite?>";
        image.title = "Confira mais um vídeo da <?=$nomeSite?>";
        // Adiciona a classe necessária para remover as bordas pretas
        image.classList.add('object-fit-cover');
        // Insere a imagem no DOM
        link.href = videoUrl[i];
        link.setAttribute('data-fancybox', 'group-1');
        link.setAttribute('class', 'lightbox');
        link.setAttribute('data-caption', image.title);
        link.title = image.title;
        $(videos[i]).append(link);
        $(link).append(image);
        $(videos[i]).on('click', function(){$(this).find("a").fancybox({})});
      }
      // // Aqui é onde a magia acontece.
      //   for(let i = 0; i < videos.length; i++){
      //     // Loopa por todos os videos e adiciona um "escutador" de evento para o click em cada um deles
      //     $(videos[i]).click(function(){
      //       //Cria o iframe a partir do indice do element que foi clicado
      //       let videoToInsert = document.createElement('iframe');
      //       videoToInsert.src = `${videoUrl[i].replace('watch?v=', 'embed/')}?&autoplay=1`;
      //       videoToInsert.classList.add(...videoClasses);
      //       let width = $(videos[i]).width();
      //       videoToInsert.height = (width / 16) * 9;
      //       // Remove a classe ytvideo para o botão do YouTube seja removido
      //       $(videos[i]).removeClass('ytvideo');
      //       // Remove o placeholder
      //       $(videos[i]).empty();
      //       // Adiciona o Iframe
      //       $(videos[i]).append(videoToInsert);
      //     })
      //   }
    })
</script>
    
    <?php include 'inc/footer.php' ?>
</body>