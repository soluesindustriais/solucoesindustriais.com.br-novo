<?php 

include 'inc/head.php';
?>
<style>
    @media only screen and (max-width: 765px) {

    .login-screen input:not([type="submit"]) {
    width:240px !important;
    height: 40px;
}

.login-screen select#regiao {
    width:240px !important;
}

}
.login-screen {
    /* display: flex; */
    /* align-items: center; */
    /* justify-content: center; */
    width: 240px;
    margin: 0 auto;
}    
.login-screen .logo-form img {
    margin: 0 auto !important;
    display: block !important;
    margin-top: 38px !important;
}

.login-screen form input[type="checkbox"] {
    width: 17px !important;
    display: inline-flex !important;
    margin: 0 0 !important;
    opacity: 0.7 !important;
}
.manterlogado {
    display: flex !important;
    align-items: center !important;
}
.login-screen a.login-external {
    width: 400px !important;
    display: flex !important;
    align-items: center !important;
    justify-content: center !important;
    height: 50px !important;
    border: 1px solid #2f5283 !important;
    margin: 7px !important;
    border-radius: 6px !important;
    background: #2f5283 !important;
    color: #fff !important;
}
.login-screen p {
    margin: 18px 0px !important;
    font-size: 16px !important;
}
.login-screen form {display: block;margin: 0 auto;}

.login-screen form label {
    position: relative !important;
    top: 13px !important;
    letter-spacing: 0.10em !important;
    text-transform: uppercase !important;
    font-size: 14px !important;
    font-weight: bold !important;
    left: 5px;
}
.login-screen form span {margin-left: 0px !important;font-size: 13px !important;font-weight: bold !important;display: block;ign-items: center;justify-content: center;flex-wrap: wrap;}

.login-screen form a {font-size: 12px !important;font-weight: bold !important;}
input[type="submit"] {
    background: #3EBD5D !important;
    border: 1px solid #3EBD5D !important;
}
.login-screen input:not([type="submit"]) {
    background: #e5e5e5 !important;
    border: none !important;
    color: #000 !important;
    padding: 0px 16px !important;
    font-weight: 100 !important;
    width: 100%;
    height: 40px;
    border-radius: 9px;
}
.login-screen a i {
    font-size: 23px !important;
    margin-right: 18px !important;
}
.login-screen hr {
    color: #939393 !important;
    height: 2px !important;
    opacity: 1 !important;
    width: 350px !important;
    margin-top: 20px !important;
}
.login-screen form a:hover{
    text-decoration: underline !important;
}
.login-screen select#regiao {
    width: 100%;
    display: block !important;
    margin: 20px 0px !important;
    height: 42px !important;
    background: #e5e5e5 !important;
    color: #000 !important;
    font-weight: bold !important;
    border: 1px solid #d0d7e1 !important;
    border-radius: 12px;
}
.bg-white{
    background: #fff !important;
    padding: 10px !important;
    height: 100vh;
}
body {
    background-color: white;
}
input[type="submit"] {width: 100%;color: #fff;border-radius: 7px;height: 39px;}
</style>
</head>


<div class="bg-white">
    
<body>
    <div class="container">
        <div class="login-screen">
            <div class="logo-form">
                <a href="<?=$url?>" title="Soluções Industriais"><img src="<?=$url?>img/logo.png" alt="Logo Soluções Industriais" title="Logo Soluções Industriais"></a>
            </div>
        <br>
        <form action="#">
            <label for="name">Nome</label>
            <input type="text" id="name">
            <label for="email">E-mail</label>
            <input type="email" id="email">
            <label for="senha">Senha</label>
            <input type="password" id="senha">
            <label for="senha">Confirme sua senha</label>
            <input type="password" id="confirmpass">
            <label for="phone">Telefone</label>
            <input type="text" id="phone">
            <label for="regiao">Informe a região em que você está!</label>
            <select name="" id="regiao">
                <option value="">Selecione</option>
                <option value="AC">Acre</option>
                <option value="AL">Alagoas</option>
                <option value="AP">Amapá</option>
                <option value="AM">Amazonas</option>
                <option value="BA">Bahia</option>
                <option value="CE">Ceará</option>
                <option value="ES">Espírito Santo</option>
                <option value="GO">Goiás</option>
                <option value="MA">Maranhão</option>
                <option value="MT">Mato Grosso</option>
                <option value="MS">Mato Grosso do Sul</option>
                <option value="MG">Minas Gerais</option>
                <option value="PA">Pará</option>
                <option value="PB">Paraíba</option>
                <option value="PR">Paraná</option>
                <option value="PE">Pernambuco</option>
                <option value="PI">Piauí</option>
                <option value="RJ">Rio de Janeiro</option>
                <option value="RN">Rio Grande do Norte</option>
                <option value="RS">Rio Grande do Sul</option>
                <option value="RO">Rondônia</option>
                <option value="RR">Roraima</option>
                <option value="SC">Santa Catarina</option>
                <option value="SP">São Paulo</option>
                <option value="SE">Sergipe</option>
                <option value="TO">Tocantins</option>
            </select>
            <input type="submit" name="Entrar"  value="Finalizar cadastro">
            <span>Ao criar a conta, você aceitará os <a class="termos-pre-cadastro" href="<?=$url?>pdf/termos-de-uso.pdf" target="_blank" title="termos e condições">termos e condições</a> do site.</span>
        </form>
    </div>
</div>


</body>
</html>