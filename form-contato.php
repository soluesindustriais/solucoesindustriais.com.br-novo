<?php
// Informações da página
$h1 = 'Formulário para contato';
$desc = 'Falta desc';
include('inc/head.php');
?>
<!-- styles -->
<link rel="stylesheet" href="css/cmp-styles.css" />
<link rel="stylesheet" href="css/form.css" />

<!-- media -->
<link rel="stylesheet" href="css/cmp-media.css" />
</head>

<body>
    <section class="section header">
        <!-- <div class="container"> -->
        <?php
        include 'inc/menu-interno.php';
        ?>
    </section>
    <div class="container">
        <?= $caminho ?>
        <!-- <h1 class="list-h1 title-style p-3"></h1> -->
        <div class="form">
            <form method="post">
                <div class="form-header">
                    <div class="title">
                        <h1><?=$h1?></h1>
                    </div>
                    <div class="login-button">
                        <button><a href="<?=$url?>">voltar</a></button>
                    </div>
                </div>

                <div class="input-group">
                    <div class="input-box">
                        <label for="firstname">Primeiro Nome</label>
                        <input id="firstname" type="text" name="firstname" placeholder="Digite seu primeiro nome" >
                    </div>

                    <div class="input-box">
                        <label for="lastname">Sobrenome</label>
                        <input id="lastname" type="text" name="lastname" placeholder="Digite seu sobrenome" >
                    </div>
                    <div class="input-box">
                        <label for="email">E-mail</label>
                        <input id="email" type="email" name="email" placeholder="Digite seu e-mail" >
                    </div>

                    <div class="input-box">
                        <label for="number">Telefone</label>
                        <input id="number" type="tel" name="number" placeholder="(xx) xxxx-xxxx" >
                    </div>

                    <div class="input-box">
                    <label for="message">Mensagem</label>
                    <textarea id="message" name="message" placeholder="Digite sua mensagem" ></textarea>
                    </div>


                </div>

                <div class="continue-button">
                    <button><a href="<?=$url?>">Continuar</a> </button>
                </div>
            </form>

    </div>
        </div>
       
        

    <? include('inc/footer.php') ?>

</body>

</html>