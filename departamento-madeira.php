<?php
$h1 = 'Lista de Departamentos';
$desc = 'Falta desc';
include 'inc/head.php';
?>
<!-- styles -->
<link rel="stylesheet" href="css/cmp-styles.css" />

<!-- media -->
<link rel="stylesheet" href="css/cmp-media.css" />
</head>

<body>

    <!-- header -->
    <section class="section header">
        <!-- <div class="container"> -->
        <?php
        include 'inc/menu-interno.php';
        ?>
    </section>

    <!-- content category -->
    <section class="section contentCat">

        <div class="container-xxl">
            <?= $caminho2 ?>
           
            <div class="row">
                <div class="col">
                    <h2>
                        Madeiras
                    </h2>
                    <p>
                        Dentre os usos mais comuns, estão as confecções de móveis, tábuas para construções civis, produção de artigos de luxo, instrumentos musicais, palanques, pisos laminados, fabricação de barcos, além de muitos outros produtos. Dentre os usos mais comuns, estão as confecções de móveis, tábuas para construções civis, produção de artigos de luxo, instrumentos musicais, palanques, pisos laminados, fabricação de barcos, além de muitos outros produtos.
                    </p>
                    <a href="#" class="btn btn-success btn-fill btn-blue" title="Ler mais">
                        Ler mais
                    </a>
                </div>
            </div>
        </div>
    </section>

    <!-- content gallery -->
    <section class="section contentGallery">
        <div class="container-xxl">
            <div class="row">
                <div class="col">
                    <h3>
                        Qual produto de madeira você mais deseja?
                    </h3>
                    <!-- category gallery -->
                    <div class="swiper categoryGallery">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <a href="#" title="Escadas Fixas">
                                    <img src="img/product-escadas-fixas.png" alt="Escadas Fixas" />
                                    <strong>
                                        Escadas Fixas
                                    </strong>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="#" title="Cavaletes">
                                    <img src="img/product-cavaletes.png" alt="Cavaletes" />
                                    <strong>
                                        Cavaletes
                                    </strong>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="#" title="Chapas e painéis">
                                    <img src="img/product-chapas-paineis.png" alt="Chapas e painéis" />
                                    <strong>
                                        Chapas e painéis
                                    </strong>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="#" title="Escadas Fixas">
                                    <img src="img/product-escadas-fixas.png" alt="Escadas Fixas" />
                                    <strong>
                                        Escadas Fixas
                                    </strong>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="#" title="Cavaletes">
                                    <img src="img/product-cavaletes.png" alt="Cavaletes" />
                                    <strong>
                                        Cavaletes
                                    </strong>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="#" title="Chapas e painéis">
                                    <img src="img/product-chapas-paineis.png" alt="Chapas e painéis" />
                                    <strong>
                                        Chapas e painéis
                                    </strong>
                                </a>
                            </div>
                        </div>
                        <div class="swiper-button-next">
                            <img src="img/ico-sld-next.png" alt="próximo" />
                        </div>
                        <div class="swiper-button-prev">
                            <img src="img/ico-sld-prev.png" alt="anterior" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- content banner -->
    <section class="section contentBanner">
        <div class="container-xxl">
            <div class="row">
                <div class="col">
                    <h4>
                        Pallets
                    </h4>
                    <p>
                        Compre sempre a melhor qualidade de produtos
                    </p>
                    <a href="#" class="btn btn-success btn-fill btn-blue" title="Comprar agora">
                        Comprar agora
                    </a>
                </div>
                <div class="col">
                    <img src="img/banner-pallets.png" alt="pallets" />
                </div>
            </div>
        </div>
    </section>

    <!-- product list -->
    <section class="section productList">
        <div class="container-xxl">
            <div class="row">
                <div class="col">
                    <!-- product list gallery -->
                    <div class="swiper productListGallerySld">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <a href="#" title="pallet">
                                    <img src="img/product-pallet-big.png" alt="pallet" />
                                    <p>
                                        <em>
                                            Cód. 123456788
                                        </em>
                                        <strong>
                                            Palhete liso madeira pinus aparelhado 1200x200 CM Settis
                                        </strong>
                                    </p>
                                    <div class="rate">
                                        <span class="star">
                                            <img src="img/ico-star.png" alt="rate" />
                                            <img src="img/ico-star.png" alt="rate" />
                                            <img src="img/ico-star.png" alt="rate" />
                                            <img src="img/ico-star.png" alt="rate" />
                                            <img src="img/ico-star-middle.png" alt="rate" />
                                        </span>
                                        <em>
                                            23 avaliações
                                        </em>
                                    </div>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="#" title="pallet">
                                    <img src="img/product-pallet-big.png" alt="pallet" />
                                    <p>
                                        <em>
                                            Cód. 123456788
                                        </em>
                                        <strong>
                                            Palhete liso madeira pinus aparelhado 1200x200 CM Settis
                                        </strong>
                                    </p>
                                    <div class="rate">
                                        <span class="star">
                                            <img src="img/ico-star.png" alt="rate" />
                                            <img src="img/ico-star.png" alt="rate" />
                                            <img src="img/ico-star.png" alt="rate" />
                                            <img src="img/ico-star.png" alt="rate" />
                                            <img src="img/ico-star-middle.png" alt="rate" />
                                        </span>
                                        <em>
                                            23 avaliações
                                        </em>
                                    </div>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="#" title="pallet">
                                    <img src="img/product-pallet-big.png" alt="pallet" />
                                    <p>
                                        <em>
                                            Cód. 123456788
                                        </em>
                                        <strong>
                                            Palhete liso madeira pinus aparelhado 1200x200 CM Settis
                                        </strong>
                                    </p>
                                    <div class="rate">
                                        <span class="star">
                                            <img src="img/ico-star.png" alt="rate" />
                                            <img src="img/ico-star.png" alt="rate" />
                                            <img src="img/ico-star.png" alt="rate" />
                                            <img src="img/ico-star.png" alt="rate" />
                                            <img src="img/ico-star-middle.png" alt="rate" />
                                        </span>
                                        <em>
                                            23 avaliações
                                        </em>
                                    </div>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="#" title="pallet">
                                    <img src="img/product-pallet-big.png" alt="pallet" />
                                    <p>
                                        <em>
                                            Cód. 123456788
                                        </em>
                                        <strong>
                                            Palhete liso madeira pinus aparelhado 1200x200 CM Settis
                                        </strong>
                                    </p>
                                    <div class="rate">
                                        <span class="star">
                                            <img src="img/ico-star.png" alt="rate" />
                                            <img src="img/ico-star.png" alt="rate" />
                                            <img src="img/ico-star.png" alt="rate" />
                                            <img src="img/ico-star.png" alt="rate" />
                                            <img src="img/ico-star-middle.png" alt="rate" />
                                        </span>
                                        <em>
                                            23 avaliações
                                        </em>
                                    </div>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="#" title="pallet">
                                    <img src="img/product-pallet-big.png" alt="pallet" />
                                    <p>
                                        <em>
                                            Cód. 123456788
                                        </em>
                                        <strong>
                                            Palhete liso madeira pinus aparelhado 1200x200 CM Settis
                                        </strong>
                                    </p>
                                    <div class="rate">
                                        <span class="star">
                                            <img src="img/ico-star.png" alt="rate" />
                                            <img src="img/ico-star.png" alt="rate" />
                                            <img src="img/ico-star.png" alt="rate" />
                                            <img src="img/ico-star.png" alt="rate" />
                                            <img src="img/ico-star-middle.png" alt="rate" />
                                        </span>
                                        <em>
                                            23 avaliações
                                        </em>
                                    </div>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="#" title="pallet">
                                    <img src="img/product-pallet-big.png" alt="pallet" />
                                    <p>
                                        <em>
                                            Cód. 123456788
                                        </em>
                                        <strong>
                                            Palhete liso madeira pinus aparelhado 1200x200 CM Settis
                                        </strong>
                                    </p>
                                    <div class="rate">
                                        <span class="star">
                                            <img src="img/ico-star.png" alt="rate" />
                                            <img src="img/ico-star.png" alt="rate" />
                                            <img src="img/ico-star.png" alt="rate" />
                                            <img src="img/ico-star.png" alt="rate" />
                                            <img src="img/ico-star-middle.png" alt="rate" />
                                        </span>
                                        <em>
                                            23 avaliações
                                        </em>
                                    </div>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="#" title="pallet">
                                    <img src="img/product-pallet-big.png" alt="pallet" />
                                    <p>
                                        <em>
                                            Cód. 123456788
                                        </em>
                                        <strong>
                                            Palhete liso madeira pinus aparelhado 1200x200 CM Settis
                                        </strong>
                                    </p>
                                    <div class="rate">
                                        <span class="star">
                                            <img src="img/ico-star.png" alt="rate" />
                                            <img src="img/ico-star.png" alt="rate" />
                                            <img src="img/ico-star.png" alt="rate" />
                                            <img src="img/ico-star.png" alt="rate" />
                                            <img src="img/ico-star-middle.png" alt="rate" />
                                        </span>
                                        <em>
                                            23 avaliações
                                        </em>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- products others -->
    <section class="section productsOthers">
        <div class="container-xxl">
            <div class="row">
                <div class="col">
                    <h3>
                        Confira também
                    </h3>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <p>
                        <strong>
                            PALLETS
                        </strong>
                        Compre sempre a melhor qualidade de produtos
                        <a href="#" class="btn btn-link" title="Ver produtos">
                            Ver produtos
                            <svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M6.44933 10.8288C6.38222 10.7619 6.32897 10.6824 6.29264 10.5949C6.2563 10.5074 6.2376 10.4136 6.2376 10.3189C6.2376 10.2241 6.2563 10.1303 6.29264 10.0428C6.32897 9.95532 6.38222 9.87585 6.44933 9.80897L9.53812 6.72018L0.719858 6.72018C0.52894 6.72018 0.345841 6.64434 0.210842 6.50934C0.0758426 6.37434 -2.28688e-07 6.19124 -2.20343e-07 6.00033C-2.11997e-07 5.80941 0.0758427 5.62631 0.210842 5.49131C0.345841 5.35631 0.52894 5.28047 0.719858 5.28047L9.53812 5.28047L6.44933 2.19048C6.31409 2.05524 6.23812 1.87183 6.23812 1.68058C6.23812 1.48933 6.31409 1.30591 6.44933 1.17068C6.58456 1.03545 6.76798 0.959472 6.95923 0.959472C7.15048 0.959472 7.33389 1.03545 7.46913 1.17068L11.7883 5.48983C11.8554 5.5567 11.9086 5.63617 11.945 5.72367C11.9813 5.81117 12 5.90498 12 5.99973C12 6.09447 11.9813 6.18828 11.945 6.27578C11.9086 6.36328 11.8554 6.44275 11.7883 6.50963L7.46913 10.8288C7.40225 10.8959 7.32278 10.9491 7.23528 10.9855C7.14778 11.0218 7.05397 11.0405 6.95923 11.0405C6.86448 11.0405 6.77067 11.0218 6.68317 10.9855C6.59567 10.9491 6.5162 10.8959 6.44933 10.8288Z" fill="#3EBD5D"></path>
                            </svg>
                        </a>
                    </p>
                    <img src="img/product-hood.png" alt="pallet" />
                </div>
                <div class="col">
                    <p>
                        <strong>
                            PALLETS
                        </strong>
                        Compre sempre a melhor qualidade de produtos
                        <a href="#" class="btn btn-link" title="Ver produtos">
                            Ver produtos
                            <svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M6.44933 10.8288C6.38222 10.7619 6.32897 10.6824 6.29264 10.5949C6.2563 10.5074 6.2376 10.4136 6.2376 10.3189C6.2376 10.2241 6.2563 10.1303 6.29264 10.0428C6.32897 9.95532 6.38222 9.87585 6.44933 9.80897L9.53812 6.72018L0.719858 6.72018C0.52894 6.72018 0.345841 6.64434 0.210842 6.50934C0.0758426 6.37434 -2.28688e-07 6.19124 -2.20343e-07 6.00033C-2.11997e-07 5.80941 0.0758427 5.62631 0.210842 5.49131C0.345841 5.35631 0.52894 5.28047 0.719858 5.28047L9.53812 5.28047L6.44933 2.19048C6.31409 2.05524 6.23812 1.87183 6.23812 1.68058C6.23812 1.48933 6.31409 1.30591 6.44933 1.17068C6.58456 1.03545 6.76798 0.959472 6.95923 0.959472C7.15048 0.959472 7.33389 1.03545 7.46913 1.17068L11.7883 5.48983C11.8554 5.5567 11.9086 5.63617 11.945 5.72367C11.9813 5.81117 12 5.90498 12 5.99973C12 6.09447 11.9813 6.18828 11.945 6.27578C11.9086 6.36328 11.8554 6.44275 11.7883 6.50963L7.46913 10.8288C7.40225 10.8959 7.32278 10.9491 7.23528 10.9855C7.14778 11.0218 7.05397 11.0405 6.95923 11.0405C6.86448 11.0405 6.77067 11.0218 6.68317 10.9855C6.59567 10.9491 6.5162 10.8959 6.44933 10.8288Z" fill="#3EBD5D"></path>
                            </svg>
                        </a>
                    </p>
                    <img src="img/product-hood-dark.png" alt="pallet" />
                </div>
            </div>
        </div>
    </section>

    <!-- content banner -->
    <section class="section contentBanner">
        <div class="container-xxl">
            <div class="row">
                <div class="col">
                    <h4>
                        Pallets
                    </h4>
                    <p>
                        Compre sempre a melhor qualidade de produtos
                    </p>
                    <a href="#" class="btn btn-success btn-fill btn-blue" title="Comprar agora">
                        Comprar agora
                    </a>
                </div>
                <div class="col">
                    <img src="img/banner-pallets.png" alt="pallets" />
                </div>
            </div>
        </div>
    </section>

    <!-- product list -->
    <section class="section productListGallery">
        <div class="container-xxl">
            <div class="row">
                <div class="col">
                    <h3>
                        Galeria de imagens
                    </h3>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <!-- images gallery -->
                    <div class="swiper imageGallery">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <a href="#" title="Product Gallery 01">
                                    <img src="img/product-gal-hood-01.png" alt="pallet" />
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="#" title="Product Gallery 02">
                                    <img src="img/product-gal-hood-02.png" alt="pallet" />
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="#" title="Product Gallery 03">
                                    <img src="img/product-gal-hood-03.png" alt="pallet" />
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="#" title="Product Gallery 04">
                                    <img src="img/product-gal-hood-04.png" alt="pallet" />
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="#" title="Product Gallery 05">
                                    <img src="img/product-gal-hood-05.png" alt="pallet" />
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="#" title="Product Gallery 03">
                                    <img src="img/product-gal-hood-03.png" alt="pallet" />
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- footer -->
    <?php include 'inc/footer.php'; ?>

    <!-- BOOTSTRAP @5.3.2 -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>

    <!-- SWIPER JS @11.0.0 -->
    <script src="https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.js"></script>

    <!-- functions -->
    <script src="js/functions.js"></script>

</body>

</html>