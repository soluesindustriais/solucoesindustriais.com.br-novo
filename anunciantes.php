<?php
// Informações da página
$h1 = 'Anunciantes';
$desc = 'Falta desc';
?>
<? include('inc/head.php') ?>
<!-- styles -->
<link rel="stylesheet" href="css/cmp-styles.css" />

<!-- media -->
<link rel="stylesheet" href="css/cmp-media.css" />
<style>
  .modal {
    display: none;
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-color: rgba(0, 0, 0, 0.7);
    z-index: 1;
    justify-content: center;
    align-items: center;
  }

  .modal-conteudo {
    background-color: #f4f4f4;
    padding: 20px;
    border-radius: 5px;
    box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.5);
    text-align: center;
    width: 50%;
    height: auto;
    position: absolute;
    top: 30%;
    left: 25%;
  }

  .fechar {
    position: absolute;
    top: 10px;
    right: 10px;
    font-size: 24px;
    cursor: pointer;
  }

  #abrirModal {
    background-color: #007BFF;
    color: #fff;
    border: none;
    padding: 10px 20px;
    border-radius: 5px;
    cursor: pointer;
  }

  #abrirModal:hover {
    background-color: #0056b3;
  }
</style>
</head>

<body>
  <section class="section header">
    <!-- <div class="container"> -->
    <?php
    include 'inc/menu-interno.php';
    ?>
  </section>
  <div class="container">
    <?= $caminho ?>



    <div class="navigations container">





      <h1 class="title-style p-3"><?= $h1 ?></h1>
    </div>

    <nav class="nav_lista_empresas hide-mobile">
      <ul style="display: flex;justify-content: center;margin: 0; padding: 0;">
        <li class=""> <a rel="noopener" class="notranslate" href="https://www.solucoesindustriais.com.br/empresas/A">A</a> </li>
        <li class=""> <a rel="noopener" class="notranslate" href="https://www.solucoesindustriais.com.br/empresas/B">B</a> </li>
        <li class=""> <a rel="noopener" class="notranslate" href="https://www.solucoesindustriais.com.br/empresas/C">C</a> </li>
        <li class=""> <a rel="noopener" class="notranslate" href="https://www.solucoesindustriais.com.br/empresas/D">D</a> </li>
        <li class=""> <a rel="noopener" class="notranslate" href="https://www.solucoesindustriais.com.br/empresas/E">E</a> </li>
        <li class=""> <a rel="noopener" class="notranslate" href="https://www.solucoesindustriais.com.br/empresas/F">F</a> </li>
        <li class=""> <a rel="noopener" class="notranslate" href="https://www.solucoesindustriais.com.br/empresas/G">G</a> </li>
        <li class=""> <a rel="noopener" class="notranslate" href="https://www.solucoesindustriais.com.br/empresas/H">H</a> </li>
        <li class=""> <a rel="noopener" class="notranslate" href="https://www.solucoesindustriais.com.br/empresas/I">I</a> </li>
        <li class=""> <a rel="noopener" class="notranslate" href="https://www.solucoesindustriais.com.br/empresas/J">J</a> </li>
        <li class=""> <a rel="noopener" class="notranslate" href="https://www.solucoesindustriais.com.br/empresas/K">K</a> </li>
        <li class=""> <a rel="noopener" class="notranslate" href="https://www.solucoesindustriais.com.br/empresas/L">L</a> </li>
        <li class=""> <a rel="noopener" class="notranslate" href="https://www.solucoesindustriais.com.br/empresas/M">M</a> </li>
        <li class=""> <a rel="noopener" class="notranslate" href="https://www.solucoesindustriais.com.br/empresas/N">N</a> </li>
        <li class=""> <a rel="noopener" class="notranslate" href="https://www.solucoesindustriais.com.br/empresas/O">O</a> </li>
        <li class=""> <a rel="noopener" class="notranslate" href="https://www.solucoesindustriais.com.br/empresas/P">P</a> </li>
        <li class=""> <a rel="noopener" class="notranslate" href="https://www.solucoesindustriais.com.br/empresas/Q">Q</a> </li>
        <li class=""> <a rel="noopener" class="notranslate" href="https://www.solucoesindustriais.com.br/empresas/R">R</a> </li>
        <li class=""> <a rel="noopener" class="notranslate" href="https://www.solucoesindustriais.com.br/empresas/S">S</a> </li>
        <li class=""> <a rel="noopener" class="notranslate" href="https://www.solucoesindustriais.com.br/empresas/T">T</a> </li>
        <li class=""> <a rel="noopener" class="notranslate" href="https://www.solucoesindustriais.com.br/empresas/U">U</a> </li>
        <li class=""> <a rel="noopener" class="notranslate" href="https://www.solucoesindustriais.com.br/empresas/V">V</a> </li>
        <li class=""> <a rel="noopener" class="notranslate" href="https://www.solucoesindustriais.com.br/empresas/W">W</a> </li>
        <li class=""> <a rel="noopener" class="notranslate" href="https://www.solucoesindustriais.com.br/empresas/X">X</a> </li>
        <li class=""> <a rel="noopener" class="notranslate" href="https://www.solucoesindustriais.com.br/empresas/Y">Y</a> </li>
        <li class=""> <a rel="noopener" class="notranslate" href="https://www.solucoesindustriais.com.br/empresas/Z">Z</a> </li>
      </ul>
    </nav>
  </div>
  <select id="linkSelect" class="only-mobile" onchange="redirectToSelected()">

    <option value="">Selecione</option>
    <!-- Variavel de "fornecedor" no arquivo 'inc/menu-fornecedor.php' -->
    <option value="https://www.solucoesindustriais.com.br/empresas/A">A</a> </option>
    <option value="https://www.solucoesindustriais.com.br/empresas/B">B</a> </option>
    <option value="https://www.solucoesindustriais.com.br/empresas/C">C</a> </option>
    <option value="https://www.solucoesindustriais.com.br/empresas/D">D</a> </option>
    <option value="https://www.solucoesindustriais.com.br/empresas/E">E</a> </option>
    <option value="https://www.solucoesindustriais.com.br/empresas/F">F</a> </option>
    <option value="https://www.solucoesindustriais.com.br/empresas/G">G</a> </option>
    <option value="https://www.solucoesindustriais.com.br/empresas/H">H</a> </option>
    <option value="https://www.solucoesindustriais.com.br/empresas/I">I</a> </option>
    <option value="https://www.solucoesindustriais.com.br/empresas/J">J</a> </option>
    <option value="https://www.solucoesindustriais.com.br/empresas/K">K</a> </option>
    <option value="https://www.solucoesindustriais.com.br/empresas/L">L</a> </option>
    <option value="https://www.solucoesindustriais.com.br/empresas/M">M</a> </option>
    <option value="https://www.solucoesindustriais.com.br/empresas/N">N</a> </option>
    <option value="https://www.solucoesindustriais.com.br/empresas/O">O</a> </option>
    <option value="https://www.solucoesindustriais.com.br/empresas/P">P</a> </option>
    <option value="https://www.solucoesindustriais.com.br/empresas/Q">Q</a> </option>
    <option value="https://www.solucoesindustriais.com.br/empresas/R">R</a> </option>
    <option value="https://www.solucoesindustriais.com.br/empresas/S">S</a> </option>
    <option value="https://www.solucoesindustriais.com.br/empresas/T">T</a> </option>
    <option value="https://www.solucoesindustriais.com.br/empresas/U">U</a> </option>
    <option value="https://www.solucoesindustriais.com.br/empresas/V">V</a> </option>
    <option value="https://www.solucoesindustriais.com.br/empresas/W">W</a> </option>
    <option value="https://www.solucoesindustriais.com.br/empresas/X">X</a> </option>
    <option value="https://www.solucoesindustriais.com.br/empresas/Y">Y</a> </option>
    <option value="https://www.solucoesindustriais.com.br/empresas/Z">Z</a> </option>

  </select>


  <script>
    function redirectToSelected() {
      var selectedLink = document.getElementById("linkSelect").value;
      if (selectedLink !== '') {
        window.location.href = selectedLink;
      }
    }
  </script>


  <div class="resultados">
    <b id="results">233 </b>
    <p>resultados</p>
  </div>
  <div class="container container-anunciantes">
    <div class="row">

      <div class="col-sm-12">
        <div id="resultado"></div>
      </div>

      <div class="mais-categorias-field" style="width: 100%;">
        <button class="mais-categorias-btn" id="exibirMaisEmpresasBtn">Exibir mais empresas</button>
      </div>
      <div class="modal" id="meuModal">
        <div class="modal-conteudo">
          <span class="fechar" id="fecharModal">&times;</span>
          <div class="nomeprod">
            <div class="row">
              <div class="col-2"><img src="<?= $url ?>/img/logo.png" alt=""></div>
              <div class="col-10">
                <p>Ver contato de Nome da empresa</p>
              </div>
            </div>


            <div class="row">
              <div class="col-6">
                <div class="infos">
                  <p><span>Cidade: </span>São Paulo - SP</p>
                  <p><span>Responsável:</span> João</p>
                  <p><span>Email:</span> joaozinho@hotmail.com</p>
                  <p><span>Telefone:</span> (11)1234-1234</p>
                  <p><span>Outras opções de contato: botão de Whatsapp</span></p>
                </div>
              </div>
              <div class="col-6">
                <div class="form-modal">
                  <p>Sem tempo para entrar em contato? <br> Deixe seus dados abaixo:</p>
                  <form action="">
                    <input type="text" placeholder="Insira seu nome">
                    <input type="phone" placeholder="Insira seu telefone">
                    <input type="email" placeholder="Insira seu e-mail">
                    <input type="submit" value="SOLICITAR CONTATO">
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <script>
        const resultadoDiv = document.getElementById('resultado');
        let numeros = [1, 2, 3]; // Array inicial de números
        let contador = numeros[numeros.length - 1]; // Inicializa o contador com o último número do array

        function atualizarExibicao() {
          resultadoDiv.innerHTML = ''; // Limpa a exibição atual

          let nomeEmpresa = 1; // Variável para controlar o número do nome da empresa

          numeros.forEach(function(numero) {
            const html = `
      <div class="empresa">
        <div class="empresa-logo-field">
          <img src="<?= $url ?>img/logo.png" alt="">
        </div>
        <div class="empresa-info-field">
          <h6><a href="<?= $url ?>mini-site-home">Nome da Empresa ${nomeEmpresa}</a></h6>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores.adasdsad amet, consectetur adipisicing elit. Dolores.adasdsad texto da empresa
           Lorem, ipsum dolor sit, amet consectetur adipisicing elit. Cumque natus porro dignissimos reiciendis illo nobis praesentium! Soluta id nulla sed et sunt nam ullam suscipit? Dolores natus excepturi assumenda earum?  texte reticendia adipisicing elit. Dolores.adasdsad amet, Máximo 5 linhas pisicing elit. Dolores.adasdsad texto da empresa
           Lorem, <b>Máximo 5 linhas</b> , amet consectetur adipisicing elit. C</p>
          <div class="btn-empresas">
            <a href="<?= $url ?>mini-site-home" class="see-more-btn-anunciantes-desk">Ver site</a>
            <a class="see-more-btn-anunciantes-desk abrirModaltel">Ver telefone</a>
          </div>
        </div>
      </div>
    `;

            resultadoDiv.innerHTML += html;

            nomeEmpresa++; // Incrementa o número do nome da empresa
          });
        }


        atualizarExibicao(); // Exibe os resultados iniciais

        const exibirMaisEmpresasBtn = document.getElementById('exibirMaisEmpresasBtn');
        exibirMaisEmpresasBtn.addEventListener('click', function() {
          const novoVetor = [6, 7, 8]; // Novo vetor a ser adicionado
          numeros = [...numeros, ...novoVetor]; // Adiciona o novo vetor ao array "numeros"

          atualizarExibicao(); // Atualiza a exibição dos resultados
        });
      </script>

      <script>
        $(document).ready(function() {
          const abrirModal = $('.abrirModaltel');
          const meuModal = $('#meuModal');
          const fecharModal = $('#fecharModal');

          abrirModal.click(function() {
            meuModal.css('display', 'block');
          });

          fecharModal.click(function() {
            meuModal.css('display', 'none');
          });

          $(window).click(function(event) {
            if (event.target === meuModal[0]) {
              meuModal.css('display', 'none');
            }
          });
        });
      </script>
    </div>
  </div>
  </div>


  <? include('inc/footer.php') ?>
</body>

</html>