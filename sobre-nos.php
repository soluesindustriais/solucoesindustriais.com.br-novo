<?
$h1 = 'Sobre nós';
$desc = 'Falta desc';
include('inc/head.php');
?>
<!-- styles -->
<link rel="stylesheet" href="css/cmp-styles.css" />
<!-- media -->
<link rel="stylesheet" href="css/cmp-media.css" />
<link href="https://fonts.googleapis.com/css2?family=League+Spartan:wght@200;400;600;700&display=swap" rel="stylesheet">
</head>

<body>
    <!-- header -->
    <section class="section header">
        <!-- <div class="container"> -->
        <?php
        include 'inc/menu-interno.php';
        ?>
    </section>

    <div class="container">
        <?= $caminho ?>
        <div class="row d-flex align-items-center">
            <div class="col-12 col-lg-6 sobre-nos-config">

                <h1>Sobre <br> Nós</h1>
                <p>O Soluções Industriais é uma empresa do Grupo Ideal Trends (GIT), criada em 2010 com o objetivo de abraçar as indústrias e suas necessidades.</p>

                <p>O GIT é composto por mais de 30 empresas e marcas que atuam em diversos setores como: Marketing Digital, Saúde, Educação, Cosméticos, Construção Civil, Gestão de Negócios, entre outros.</p>

                <p>A plataforma do Soluções Industriais conecta compradores e os melhores fornecedores por meio de cotações realizadas de forma prática e eficiente em um só lugar.</p>

                <p>Nossas estratégias de Marketing geram visibilidade online, a moeda da Era Digital, com o objetivo de evidenciar a marca dos clientes na internet e consequentemente possibilitar novas oportunidades de negócios.</p>

                <p>Fazemos isso captando tráfego qualificado em mais de 100 canais a partir da divulgação dos produtos e serviços de anunciantes que trabalham conosco.</p>

                <p>De maneira inovadora no mercado nacional, o Soluções Industriais é uma resposta confiável para empresários que buscam crescimento com o Marketing Digital.</p>

            </div>
            <div class="col-12 col-lg-6">
                <img src="<?= $url ?>img/sobre-nos-lateral-2.png" alt="" title="" style="mix-blend-mode: multiply; width: 100%;">
            </div>
        </div>
    </div>
    <div class="container">


        <h2 class="mt-4 feedbacks_sobre mt-4 text-center">Nossos feedbacks</h2>

        <h3 class="text-center mt-4 mb-4">
            Em mais de 13 anos de empresa, o Soluções Industriais conquistou a confiança de milhares de clientes.
            Confira o que alguns dos nossos anunciantes estão falando:
        </h3>
    </div>


    <div class="feedbacks-sobre">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-12">
                    <img src="./img/logo-novo-maze.png" alt=" Imagem Feedback 1" class="img-feedbacks">
                    <h3 class="feedbacks-h3">Sérgio Thomazelli</h3>
                    <p class="feedbacks-p">"Nós estamos sentido uma diferença no número de vendas."</p>
                </div>
                <div class="col-lg-4 col-12">
                    <img src="./img/logo-novo-junseal.png" alt=" Imagem Feedback 2" class="img-feedbacks">
                    <h3 class="feedbacks-h3">Mauro Sanchez</h3>
                    <p class="feedbacks-p">"Temos retorno com o Soluções Industriais, novos contatos comerciais e novas prospecções"</p>
                </div>
                <div class="col-lg-4 col-12">
                    <img src="./img/logo-novo-itaqua.png" alt=" Imagem Feedback 3" class="img-feedbacks">
                    <h3 class="feedbacks-h3">Clécio Araújo</h3>
                    <p class="feedbacks-p">"O Soluções Industriais vêm somando para o crescimento da nossa empresa"</p>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-sobre-nos-time">
        <div class="container">



            <div class="row">
                <div class="col-lg-6 col-12 p-0 sobre-nos-config">
                    <h2>Conheça <br> nosso time</h2>

                    <p>O Soluções Industriais conta com uma equipe de excelência, desde as áreas de Vendas, Novos Negócios e Customer Success, atreladas ao atendimento dos clientes, até os setores Operacionais em que os especialistas de Marketing Digital cuidam dos projetos da casa.</p>

                    <p>Todos do nosso time realizam rigorosos controles de qualidade com o objetivo de aprimorar cada vez mais os processos internos e entregar as conquistas desejadas por empresas de diversos segmentos.</p>

                    <p>O sucesso é uma escolha, e o Soluções Industriais é a melhor delas para o futuro do seu negócio.</p>
                </div>
                <div class="col-lg-6 col-12 p-0">
                    <img src="<?= $url ?>img/sobre-nos-lateral.png" alt="" title="" style="mix-blend-mode: multiply;">
                </div>
            </div>
        </div>
    </div>
    <div class="form-faca-parte wrapper">
        <h2>PREENCHA O FORMULÁRIO ABAIXO E FAÇA PARTE DA MAIOR PLATAFORMA DE CONTATOS COMERCIAIS DO BRASIL</h2>
        <p>NÃO FIQUE DE FORA! APROVEITE A OPORTUNIDADE DE ATRAIR CENTENAS DE POTENCIAIS CONTATOS COMERCIAIS PARA A SUA EMPRESA COM A MAIOR PLATAFORMA B2B DO BRASIL E ALAVANQUE O SEU NEGÓCIO</p>
    <script type="text/javascript" src="//automacao.marketingparaindustria.com.br/form/generate.js?id=1"></script>
    </div>
    <?
    include('inc/footer.php')
    ?>
</body>
</html>