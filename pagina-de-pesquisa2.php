<?php
// Informações da página
$h1 = 'Pesquisa';
$desc = 'Falta desc';
?>

<head>
    <?
    include('inc/head.php');
    ?>
    <link rel="stylesheet" href="<?= $url ?>/assets/css/pagina-de-pesquisa2.css">
    <link rel="shortcut icon" href="<?= $url ?>assets/imagens/solucs/solucoes-industriais.png">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?= $url ?>assets/css/owl.carousel.css">
    <link rel="stylesheet" href="<?= $url ?>assets/css/owl.theme.css">
    <link rel="stylesheet" href="assets/css/jquery.fancybox.min.css">
</head>

<style>

</style>

<body>

    <?
    include('inc/image-lightbox.php');
    ?>


    <main class="container">
        <div class="row filter-row">
            <div class="col-sm-12">

                <div class="container py-2 ">
                    <article>
                        <div class="card2 LeiaMais info-content-sub"> <!-- LeiaMais info-content-sub -->
                            <h1>Página de pesquisa</h1>
                            <p>Dentre os usos mais comuns, estão as confecções de móveis, tábuas para construções civis, produção de artigos de luxo, instrumentos musicais, palanques, pisos laminados, fabricação de barcos, além de muitos outros produtos. Dentre os usos mais comuns, estão as confecções de móveis, tábuas para construções civis, produção de artigos de luxo, instrumentos musicais, palanques, pisos laminados, fabricação de barcos, além de muitos outros produtos. Dentre os usos mais comuns, estão as confecções de móveis, tábuas para construções civis, produção de artigos de luxo, instrumentos musicais, palanques, pisos laminados, fabricação de barcos, além de muitos outros produtos. Dentre os usos mais comuns, estão as confecções de móveis, tábuas para construções civis, produção de artigos de luxo, instrumentos musicais, palanques, pisos laminados, fabricação de barcos, além de muitos outros produtos. Dentre os usos mais comuns, estão as confecções de móveis, tábuas para construções civis, produção de artigos de luxo, instrumentos musicais, palanques, pisos laminados,entre os usos mais comuns, estão as confecções de móveis, tábuas para construções civis, produção de artigos de luxo, instrumentos musicais, palanques, pisos laminados, fabricação de barcos, além de muitos outros produtos. Dentre os usos mais comuns, de barcos, além de muitos outros produtos. Dentre os usos mais comuns, estão as confecções de móveis, tábuas para construções civis, produção de artigos de luxo, instrumentos musicais, palanques, pisos laminados, fabricação de barcos, além de muitos outros produtos. Dentre os usos mais comuns, estão as confecções de móveis, tábuas para construções civis, produção de artigos de luxo, instrumentos musicais, palanques, pisos laminados, fabricação de barcos, além de muitos outros produtos. Dentre os usos mais comuns, estão as confecções de móveis, tábuas para construções civis, produção de artigos de luxo, instrumentos musicais, palanques, pisos laminados, fabricação de barcos, além de muitos outros produtos. Dentre os usos mais comuns, estão as confecções de móveis, tábuas para construções civis, produção de artigos de luxo, instrumentos musicais, palanques, pisos laminados,entre os usos mais comuns, estão as confecções de móveis, tábuas para construções civis, produção de artigos de luxo, instrumentos musicais, palanques, pisos laminados, fabricação de barcos, além de muitos outros produtos. , além de muitos outros produtos.entre os usos mais comuns, estão as confecções de móveis, tábuas para construções civis, produção de artigos de luxo, instrumentos musicais, palanques, pisos laminados, fabricação de barcos, além de muitos outros produtos. Dentre os usos mais comuns, de barcos, além de muitos outros produtos. Dentre os usos mais comuns, estão as confecções de móveis, tábuas para construções civis, produção de artigos de luxo, instrumentos musicais, palanques, pisos laminados, fabricação de barcos, além de muitos outros produtos. Dentre os usos mais comuns, estão as confecções de móveis, tábuas para construções civis, produção de artigos de luxo, instrumentos musicais, palanques, pisos laminados, fabricação de qbarcos, além de muitos outros produtos. Dentre os usos mais comuns, estão as confecções de móveis, tábuas para construções civis, produção de artigos de luxo, instrumentos musicais, palanques, pisos laminados, fabricação de barcos, além de muitos outros produtos. Dentre os usos mais comuns, estão as confecções de móveis, tábuas para construções civis, produção de artigos de luxo, instrumentos musicais, palanques, pisos laminados,entre os usos mais comuns, estão as confecções de móveis, tábuas para construções civis, produção de artigos de luxo, instrumentos musicais, palanques, pisos laminados, fabricação de barcos, além de muitos outros produtos. , além de muitos outros produtos.</p>
                            <span class="btn-leia">Ver mais<i class="fa fa-angle-down"></i></span><span class="btn-ocultar">Ver menos<i class="fa fa-angle-up"></i></span>
                        </div>
                    </article>
                </div>

                <div class="refinar-busca">
                    <p>Refinar Busca</p>
                    <p>Ordernar por</p>
                    <select class="selecionar-por">
                        <option value="0" disabled selected>Selecionar por</option>
                        <option value="1">Ordem crescente</option>
                        <option value="2">Ordem decrescente</option>
                    </select>
                </div>

                <div class="selecionar-definicao">
                    <p>Listar Produtos Com</p>
                    <div class="selecionar-propriedades">
                        <select id="select-filt1">
                            <option value="0" disabled selected>Largura</option>
                            <option value="1">30cm</option>
                            <option value="2">60cm</option>
                            <option value="2">90cm</option>
                        </select>
                        <select id="select-filt2">
                            <option value="0" disabled selected>Altura</option>
                            <option value="1">30cm</option>
                            <option value="2">60cm</option>
                            <option value="2">90cm</option>
                        </select>
                        <select id="select-filt3">
                            <option value="0" disabled selected>Espessura</option>
                            <option value="1">30cm</option>
                            <option value="2">60cm</option>
                            <option value="2">90cm</option>
                        </select>
                        <select id="select-filt4">
                            <option value="0" disabled selected>Tipo de Material</option>
                            <option value="1">Madeira</option>
                            <option value="2">Plástico</option>
                            <option value="2">Metal</option>
                        </select>
                        <select id="select-filt5">
                            <option value="0" disabled selected>Tonalidade</option>
                            <option value="1">Branco</option>
                            <option value="2">Amarelo</option>
                            <option value="2">Verde</option>
                        </select>

                    </div>
                    <a class="limpar-filtros" id="limpar-filtros">Limpar filtros</a>
                </div>


                <div class="galeria-de-produtos">
                    <aside class="filtro-lateral">
                        <div class="menu-lateral">
                            <ul class="sub-menu-lateral">
                                <li><a class="topicos-menu-lateral">Vendido por</a><i class="fa fa-angle-down"></i></li>
                                <li onclick="mostrarSubmenuLateral(event)">
                                    <a class="topicos-menu-lateral">Largura</a>
                                    <i class="fa fa-angle-down"></i>
                                    <ul class="opcoes-menu-lateral">
                                        <li>
                                            <label>
                                                <input type="checkbox">
                                                <span class="checkmark"></span>
                                                30cm
                                            </label>
                                        </li>
                                        <li>
                                            <label>
                                                <input type="checkbox">
                                                <span class="checkmark"></span>
                                                60cm
                                            </label>
                                        </li>
                                        <li>
                                            <label>
                                                <input type="checkbox">
                                                <span class="checkmark"></span>
                                                90cm
                                            </label>
                                        </li>
                                    </ul>
                                </li>


                                <li><a class="topicos-menu-lateral">Comprimento</a><i class="fa fa-angle-down"></i></li>
                                <li><a class="topicos-menu-lateral">Espessura</a><i class="fa fa-angle-down"></i></li>
                                <li><a class="topicos-menu-lateral">Tipo de Material</a><i class="fa fa-angle-down"></i></li>
                                <li><a class="topicos-menu-lateral">Tonalidade</a><i class="fa fa-angle-down"></i></li>


                            </ul>


                        </div>
                    </aside>
                    <ul class="resultado-produtos">
                        <li class="produto-lista ">
                            <i onclick="curtirProduto(this)" class="fa fa-heart "></i>
                            <img alt="Imagem do Produto" title="Imagem do Produto" src="https://www.solucoesindustriais.com.br/images/produtos/imagens_12417/thumbnails/350/pallet-de-aco_12417_4364701666186040220_cover.jpg">
                            <a class="link-fornecedor" rel="noopener" href="https://www.solucoesindustriais.com.br/empresa/pallets-geral/total-pack-comercio-de-embalag" title="Total Pack Comércio de Embalagens">
                                <img class="fornecedor-img" src="https://www.solucoesindustriais.com.br/images/industrias/imagens_12428/thumbnails/120/total-pack-comercio-de-embalag.jpg" data-src="https://www.solucoesindustriais.com.br/images/industrias/imagens_12428/thumbnails/120/total-pack-comercio-de-embalag.jpg" alt="Total Pack Comércio de Embalagens" data-was-processed="true">
                            </a>
                            <a href="<? $url ?>produto" class="nome-do-produto" title="Nome do Produto">Pallet Liso Madeira Pinus Aparelhado 1200x200cm Settis</a>
                            <!-- <span class="codigo-do-produto">Cód. 123456789</span> -->
                            <div class="avaliacao-produto pb-3">
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star"></span>
                                <span>(23)</span>
                            </div>
                            <button class="btn-orcamento text-center btn-cotar botao-cotar" id="btn-cotar2">SOLICITE UM ORÇAMENTO GRÁTIS</button>

                        </li>

                        <li class="produto-lista ">
                            <i onclick="curtirProduto(this)" class="fa fa-heart "></i>
                            <span class="produto-em-destaque">Destaque</span>
                            <img alt="Imagem do Produto" title="Imagem do Produto" src="https://www.solucoesindustriais.com.br/images/produtos/imagens_10224/thumbnails/350/porca-gaiola-m4_10224_428767_1658435647766_cover.jpg">
                            <a class="link-fornecedor" rel="noopener" href="https://www.solucoesindustriais.com.br/empresa/pallets-geral/total-pack-comercio-de-embalag" title="Total Pack Comércio de Embalagens">
                                <img class="fornecedor-img" src="https://www.solucoesindustriais.com.br/images/industrias/imagens_12428/thumbnails/120/total-pack-comercio-de-embalag.jpg" data-src="https://www.solucoesindustriais.com.br/images/industrias/imagens_12428/thumbnails/120/total-pack-comercio-de-embalag.jpg" alt="Total Pack Comércio de Embalagens" data-was-processed="true">
                            </a>
                            <a href="<? $url ?>produto" class="nome-do-produto" title="Nome do Produto">Pallet Liso Madeira Pinus Aparelhado 1200x200cm Settis</a>
                            <!-- <span class="codigo-do-produto">Cód. 123456789</span> -->
                            <div class="avaliacao-produto pb-3">
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star"></span>
                                <span>(23)</span>
                            </div>
                            <button class="btn-orcamento text-center" id botao-cotar="btn-cotar3">SOLICITE UM ORÇAMENTO GRÁTIS</button>

                        </li>

                        <li class="produto-lista ">
                            <i onclick="curtirProduto(this)" class="fa fa-heart "></i>
                            <img alt="Imagem do Produto" title="Imagem do Produto" src="https://www.solucoesindustriais.com.br/images/produtos/imagens_10224/thumbnails/350/porca-gaiola-m4_10224_428767_1658435647766_cover.jpg">
                            <a class="link-fornecedor" rel="noopener" href="https://www.solucoesindustriais.com.br/empresa/pallets-geral/total-pack-comercio-de-embalag" title="Total Pack Comércio de Embalagens">
                                <img class="fornecedor-img" src="https://www.solucoesindustriais.com.br/images/industrias/imagens_12428/thumbnails/120/total-pack-comercio-de-embalag.jpg" data-src="https://www.solucoesindustriais.com.br/images/industrias/imagens_12428/thumbnails/120/total-pack-comercio-de-embalag.jpg" alt="Total Pack Comércio de Embalagens" data-was-processed="true">
                            </a>
                            <a href="<? $url ?>produto" class="nome-do-produto" title="Nome do Produto">Pallet Liso Madeira Pinus Aparelhado 1200x200cm Settis</a>
                            <!-- <span class="codigo-do-produto">Cód. 123456789</span> -->
                            <div class="avaliacao-produto pb-3">
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star"></span>
                                <span>(23)</span>
                            </div>
                            <button class="btn-orcamento text-center" id botao-cotar="btn-cotar4">SOLICITE UM ORÇAMENTO GRÁTIS</button>

                        </li>

                        <li class="produto-lista ">
                            <i onclick="curtirProduto(this)" class="fa fa-heart "></i>
                            <img alt="Imagem do Produto" title="Imagem do Produto" src="https://www.solucoesindustriais.com.br/images/produtos/imagens_10224/thumbnails/350/porca-gaiola-m4_10224_428767_1658435647766_cover.jpg">
                            <a class="link-fornecedor" rel="noopener" href="https://www.solucoesindustriais.com.br/empresa/pallets-geral/total-pack-comercio-de-embalag" title="Total Pack Comércio de Embalagens">
                                <img class="fornecedor-img" src="https://www.solucoesindustriais.com.br/images/industrias/imagens_12428/thumbnails/120/total-pack-comercio-de-embalag.jpg" data-src="https://www.solucoesindustriais.com.br/images/industrias/imagens_12428/thumbnails/120/total-pack-comercio-de-embalag.jpg" alt="Total Pack Comércio de Embalagens" data-was-processed="true">
                            </a>
                            <a href="<? $url ?>produto" class="nome-do-produto" title="Nome do Produto">Pallet Liso Madeira Pinus Aparelhado 1200x200cm Settis</a>
                            <!-- <span class="codigo-do-produto">Cód. 123456789</span> -->
                            <div class="avaliacao-produto pb-3">
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star"></span>
                                <span>(23)</span>
                            </div>
                            <button class="btn-orcamento text-center" id botao-cotar="btn-cotar5">SOLICITE UM ORÇAMENTO GRÁTIS</button>

                        </li>

                        <li class="produto-lista ">
                            <i onclick="curtirProduto(this)" class="fa fa-heart "></i>
                            <img alt="Imagem do Produto" title="Imagem do Produto" src="https://www.solucoesindustriais.com.br/images/produtos/imagens_10224/thumbnails/350/porca-gaiola-m4_10224_428767_1658435647766_cover.jpg">
                            <a class="link-fornecedor" rel="noopener" href="https://www.solucoesindustriais.com.br/empresa/pallets-geral/total-pack-comercio-de-embalag" title="Total Pack Comércio de Embalagens">
                                <img class="fornecedor-img" src="https://www.solucoesindustriais.com.br/images/industrias/imagens_12428/thumbnails/120/total-pack-comercio-de-embalag.jpg" data-src="https://www.solucoesindustriais.com.br/images/industrias/imagens_12428/thumbnails/120/total-pack-comercio-de-embalag.jpg" alt="Total Pack Comércio de Embalagens" data-was-processed="true">
                            </a>
                            <a href="<? $url ?>produto" class="nome-do-produto" title="Nome do Produto">Pallet Liso Madeira Pinus Aparelhado 1200x200cm Settis</a>
                            <!-- <span class="codigo-do-produto">Cód. 123456789</span> -->
                            <div class="avaliacao-produto pb-3">
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star"></span>
                                <span>(23)</span>
                            </div>
                            <button class="btn-orcamento text-center" id botao-cotar="btn-cotar6">SOLICITE UM ORÇAMENTO GRÁTIS</button>
                        </li>

                        <li class="produto-lista ">
                            <i onclick="curtirProduto(this)" class="fa fa-heart "></i>
                            <img alt="Imagem do Produto" title="Imagem do Produto" src="https://www.solucoesindustriais.com.br/images/produtos/imagens_10224/thumbnails/350/porca-gaiola-m4_10224_428767_1658435647766_cover.jpg">
                            <a class="link-fornecedor" rel="noopener" href="https://www.solucoesindustriais.com.br/empresa/pallets-geral/total-pack-comercio-de-embalag" title="Total Pack Comércio de Embalagens">
                                <img class="fornecedor-img" src="https://www.solucoesindustriais.com.br/images/industrias/imagens_12428/thumbnails/120/total-pack-comercio-de-embalag.jpg" data-src="https://www.solucoesindustriais.com.br/images/industrias/imagens_12428/thumbnails/120/total-pack-comercio-de-embalag.jpg" alt="Total Pack Comércio de Embalagens" data-was-processed="true">
                            </a>
                            <a href="<? $url ?>produto" class="nome-do-produto" title="Nome do Produto">Pallet Liso Madeira Pinus Aparelhado 1200x200cm Settis</a>
                            <!-- <span class="codigo-do-produto">Cód. 123456789</span> -->
                            <div class="avaliacao-produto pb-3">
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star"></span>
                                <span>(23)</span>
                            </div>
                            <button class="btn-orcamento text-center" id botao-cotar="btn-cotar7">SOLICITE UM ORÇAMENTO GRÁTIS</button>

                        </li>

                        <li class="produto-lista ">
                            <i onclick="curtirProduto(this)" class="fa fa-heart "></i>
                            <img alt="Imagem do Produto" title="Imagem do Produto" src="https://www.solucoesindustriais.com.br/images/produtos/imagens_10224/thumbnails/350/porca-gaiola-m4_10224_428767_1658435647766_cover.jpg">
                            <a class="link-fornecedor" rel="noopener" href="https://www.solucoesindustriais.com.br/empresa/pallets-geral/total-pack-comercio-de-embalag" title="Total Pack Comércio de Embalagens">
                                <img class="fornecedor-img" src="https://www.solucoesindustriais.com.br/images/industrias/imagens_12428/thumbnails/120/total-pack-comercio-de-embalag.jpg" data-src="https://www.solucoesindustriais.com.br/images/industrias/imagens_12428/thumbnails/120/total-pack-comercio-de-embalag.jpg" alt="Total Pack Comércio de Embalagens" data-was-processed="true">
                            </a>
                            <a href="<? $url ?>produto" class="nome-do-produto" title="Nome do Produto">Pallet Liso Madeira Pinus Aparelhado 1200x200cm Settis</a>
                            <!-- <span class="codigo-do-produto">Cód. 123456789</span> -->
                            <div class="avaliacao-produto pb-3">
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star"></span>
                                <span>(23)</span>
                            </div>
                            <button class="btn-orcamento text-center" id botao-cotar="btn-cotar8">SOLICITE UM ORÇAMENTO GRÁTIS</button>

                        </li>

                        <li class="produto-lista ">
                            <i onclick="curtirProduto(this)" class="fa fa-heart "></i>
                            <img alt="Imagem do Produto" title="Imagem do Produto" src="https://www.solucoesindustriais.com.br/images/produtos/imagens_10224/thumbnails/350/porca-gaiola-m4_10224_428767_1658435647766_cover.jpg">
                            <a class="link-fornecedor" rel="noopener" href="https://www.solucoesindustriais.com.br/empresa/pallets-geral/total-pack-comercio-de-embalag" title="Total Pack Comércio de Embalagens">
                                <img class="fornecedor-img" src="https://www.solucoesindustriais.com.br/images/industrias/imagens_12428/thumbnails/120/total-pack-comercio-de-embalag.jpg" data-src="https://www.solucoesindustriais.com.br/images/industrias/imagens_12428/thumbnails/120/total-pack-comercio-de-embalag.jpg" alt="Total Pack Comércio de Embalagens" data-was-processed="true">
                            </a>
                            <a href="<? $url ?>produto" class="nome-do-produto" title="Nome do Produto">Pallet Liso Madeira Pinus Aparelhado 1200x200cm Settis</a>
                            <!-- <span class="codigo-do-produto">Cód. 123456789</span> -->
                            <div class="avaliacao-produto pb-3">
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star"></span>
                                <span>(23)</span>
                            </div>
                            <button class="btn-orcamento text-center" id botao-cotar="btn-cotar9">SOLICITE UM ORÇAMENTO GRÁTIS</button>

                        </li>

                        <li class="produto-lista ">
                            <i onclick="curtirProduto(this)" class="fa fa-heart "></i>
                            <img alt="Imagem do Produto" title="Imagem do Produto" src="https://www.solucoesindustriais.com.br/images/produtos/imagens_10224/thumbnails/350/porca-gaiola-m4_10224_428767_1658435647766_cover.jpg">
                            <a class="link-fornecedor" rel="noopener" href="https://www.solucoesindustriais.com.br/empresa/pallets-geral/total-pack-comercio-de-embalag" title="Total Pack Comércio de Embalagens">
                                <img class="fornecedor-img" src="https://www.solucoesindustriais.com.br/images/industrias/imagens_12428/thumbnails/120/total-pack-comercio-de-embalag.jpg" data-src="https://www.solucoesindustriais.com.br/images/industrias/imagens_12428/thumbnails/120/total-pack-comercio-de-embalag.jpg" alt="Total Pack Comércio de Embalagens" data-was-processed="true">
                            </a>
                            <a href="<? $url ?>produto" class="nome-do-produto" title="Nome do Produto">Pallet Liso Madeira Pinus Aparelhado 1200x200cm Settis</a>
                            <!-- <span class="codigo-do-produto">Cód. 123456789</span> -->
                            <div class="avaliacao-produto pb-3">
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star"></span>
                                <span>(23)</span>
                            </div>
                            <button class="btn-orcamento text-center" id botao-cotar="btn-cotar10">SOLICITE UM ORÇAMENTO GRÁTIS</button>

                        </li>
                        <li class="produto-lista ">
                            <i onclick="curtirProduto(this)" class="fa fa-heart "></i>
                            <img alt="Imagem do Produto" title="Imagem do Produto" src="https://www.solucoesindustriais.com.br/images/produtos/imagens_10224/thumbnails/350/porca-gaiola-m4_10224_428767_1658435647766_cover.jpg">
                            <a class="link-fornecedor" rel="noopener" href="https://www.solucoesindustriais.com.br/empresa/pallets-geral/total-pack-comercio-de-embalag" title="Total Pack Comércio de Embalagens">
                                <img class="fornecedor-img" src="https://www.solucoesindustriais.com.br/images/industrias/imagens_12428/thumbnails/120/total-pack-comercio-de-embalag.jpg" data-src="https://www.solucoesindustriais.com.br/images/industrias/imagens_12428/thumbnails/120/total-pack-comercio-de-embalag.jpg" alt="Total Pack Comércio de Embalagens" data-was-processed="true">
                            </a>
                            <a href="<? $url ?>produto" class="nome-do-produto" title="Nome do Produto">Pallet Liso Madeira Pinus Aparelhado 1200x200cm Settis</a>
                            <!-- <span class="codigo-do-produto">Cód. 123456789</span> -->
                            <div class="avaliacao-produto pb-3">
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star"></span>
                                <span>(23)</span>
                            </div>
                            <button class="btn-orcamento text-center" id botao-cotar="btn-cotar11">SOLICITE UM ORÇAMENTO GRÁTIS</button>

                        </li>
                        <li class="produto-lista ">
                            <i onclick="curtirProduto(this)" class="fa fa-heart "></i>
                            <img alt="Imagem do Produto" title="Imagem do Produto" src="https://www.solucoesindustriais.com.br/images/produtos/imagens_10224/thumbnails/350/porca-gaiola-m4_10224_428767_1658435647766_cover.jpg">
                            <a class="link-fornecedor" rel="noopener" href="https://www.solucoesindustriais.com.br/empresa/pallets-geral/total-pack-comercio-de-embalag" title="Total Pack Comércio de Embalagens">
                                <img class="fornecedor-img" src="https://www.solucoesindustriais.com.br/images/industrias/imagens_12428/thumbnails/120/total-pack-comercio-de-embalag.jpg" data-src="https://www.solucoesindustriais.com.br/images/industrias/imagens_12428/thumbnails/120/total-pack-comercio-de-embalag.jpg" alt="Total Pack Comércio de Embalagens" data-was-processed="true">
                            </a>
                            <a href="<? $url ?>produto" class="nome-do-produto" title="Nome do Produto">Pallet Liso Madeira Pinus Aparelhado 1200x200cm Settis</a>
                            <!-- <span class="codigo-do-produto">Cód. 123456789</span> -->
                            <div class="avaliacao-produto pb-3">
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star"></span>
                                <span>(23)</span>
                            </div>
                            <button class="btn-orcamento text-center" id botao-cotar="btn-cotar12">SOLICITE UM ORÇAMENTO GRÁTIS</button>

                        </li>
                        <li class="produto-lista ">
                            <i onclick="curtirProduto(this)" class="fa fa-heart "></i>
                            <img alt="Imagem do Produto" title="Imagem do Produto" src="https://www.solucoesindustriais.com.br/images/produtos/imagens_10224/thumbnails/350/porca-gaiola-m4_10224_428767_1658435647766_cover.jpg">
                            <a class="link-fornecedor" rel="noopener" href="https://www.solucoesindustriais.com.br/empresa/pallets-geral/total-pack-comercio-de-embalag" title="Total Pack Comércio de Embalagens">
                                <img class="fornecedor-img" src="https://www.solucoesindustriais.com.br/images/industrias/imagens_12428/thumbnails/120/total-pack-comercio-de-embalag.jpg" data-src="https://www.solucoesindustriais.com.br/images/industrias/imagens_12428/thumbnails/120/total-pack-comercio-de-embalag.jpg" alt="Total Pack Comércio de Embalagens" data-was-processed="true">
                            </a>
                            <a href="<? $url ?>produto" class="nome-do-produto" title="Nome do Produto">Pallet Liso Madeira Pinus Aparelhado 1200x200cm Settis</a>
                            <!-- <span class="codigo-do-produto">Cód. 123456789</span> -->
                            <div class="avaliacao-produto pb-3">
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star"></span>
                                <span>(23)</span>
                            </div>
                            <button class="btn-orcamento text-center" id botao-cotar="btn-cotar13">SOLICITE UM ORÇAMENTO GRÁTIS</button>

                        </li>
                    </ul>
                    <div class="mais-categorias-field" style="margin: 0 auto;">
                        <button class="mais-categorias-btn more-list-fornecedor" id="mais-produtos" onclick="moreProdutosDesk()">Exibir mais produtos</button>
                    </div>
                </div>

                <div class="container mb-4 ">
                    <div class="card2 info-content-sub">
                        <h2>Texto secundário</h2>
                        <p>Dentre os usos mais comuns, estão as confecções de móveis, tábuas para construções civis, produção de artigos de luxo, instrumentos musicais, palanques, pisos laminados, fabricação de barcos, além de muitos outros produtos. Dentre os usos mais comuns, estão as confecções de móveis, tábuas para construções civis, produção de artigos de luxo, instrumentos musicais, palanques, pisos laminados, fabricação de barcos, além de muitos outros produtos. Dentre os usos mais comuns, estão as confecções de móveis, tábuas para construções civis, produção de artigos de luxo, instrumentos musicais, palanques, pisos laminados, fabricação de barcos, além de muitos outros produtos. Dentre os usos mais comuns, estão as confecções de móveis, tábuas para construções civis, produção de artigos de luxo, instrumentos musicais, palanques, pisos laminados, fabricação de barcos, além de muitos outros produtos.</p>

                        <h2> Variação da palavra </h2>
                        <p>Dentre os usos mais comuns, estão as confecções de móveis, tábuas para construções civis, produção de artigos de luxo, instrumentos musicais, palanques, pisos laminados,entre os usos mais comuns, estão as confecções de móveis, tábuas para construções civis, produção de artigos de luxo, instrumentos musicais, palanques, pisos laminados, fabricação de barcos, além de muitos outros produtos. Dentre os usos mais comuns, de barcos, além de muitos outros produtos. Dentre os usos mais comuns, estão as confecções de móveis, tábuas para construções civis, produção de artigos de luxo, instrumentos musicais, palanques, pisos laminados, fabricação de barcos, além de muitos outros produtos. Dentre os usos mais comuns, estão as confecções de móveis, tábuas para construções civis, produção de artigos de luxo, instrumentos musicais.</p>

                        <h2> Variação da palavra2 </h2>
                        <p>palanques, pisos laminados, fabricação de barcos, além de muitos outros produtos. Dentre os usos mais comuns, estão as confecções de móveis, tábuas para construções civis, produção de artigos de luxo, instrumentos musicais, palanques, pisos laminados, fabricação de barcos, além de muitos outros produtos. Dentre os usos mais comuns, estão as confecções de móveis, tábuas para construções civis, produção de artigos de luxo, instrumentos musicais, palanques, pisos laminados,entre os usos mais comuns, estão as confecções de móveis, tábuas para construções civis, produção de artigos de luxo, instrumentos musicais, palanques, pisos laminados, fabricação de barcos, além de muitos outros produtos. , além de muitos outros produtos.</p>
                    </div>
                </div>

                <?
                include('inc/produto-slider.php')
                ?>
            </div>
        </div>
    </main>


    <!-- scripts gerais -->
    <script async src="<? $url ?>assets/js/pagina-de-pesquisa2.js"></script>

    <!-- filtros de pesquisa -->
    <script async src="<? $url ?>assets/js/filtros.js"></script>

    <!-- galeria de imagens -->
    <script async src="<?= $url ?>assets/js/owl.carousel.js"></script>



</body>