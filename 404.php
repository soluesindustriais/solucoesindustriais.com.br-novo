<?
$h1         = 'Página não encontrada ;(';
$desc       = 'Navegue pelo menu do nosso site e encontre o que está procurando, escolha abaixo a página que deseja visualizar.';

include('inc/head.php');?>
<!-- styles -->
<link rel="stylesheet" href="css/cmp-styles.css" />

<!-- media -->
<link rel="stylesheet" href="css/cmp-media.css" />
</head>
<body>
     <? //include('inc/MenuDe.php'); ?>
    <?// include('inc/navbar-like.php'); ?> 
</head>

<body>
    <section class="section header">
        <!-- <div class="container"> -->
        <?php
        include 'inc/menu-interno.php';
        ?>
    </section>
                    <?= $caminho; ?>
    <main class=" center-all">
        <div class="content my-5">
            <section class="my-5 page-404">
                <article class="full page404 d-flex justify-content-center align-items-center flex-wrap">
                    <h1 class="mb-2"><?= $h1 ?></h1>
                    <div class="container text-center d-flex justify-content-center align-items-center flex-wrap">
                    <p class="mb-3 w-100">A página que você tentou acessar está indisponível ou não existe.</p>
                    <div class="rowButton404 my-5">
                        <a title="Voltar a página inicial" class="btn m-2 btn-primary  mr-3 " href="javascript:history.go(-1);">Retornar a página anterior</a>

                        <a title="Voltar a página inicial" class="btn m-2 btn-secondary " href="<?=$url?>" style="background: #3EBD5D!important">Ir para página inicial</a>
                    </div>
                    </div>
                <div class=" w-100 searchBox">
                        <form class="d-flex justify-content-center mw-50 w-50 mx-auto" action="busca.php" method="get">
                            <input id="searchBox" name="produto" type="text" class="form-control mw-100" style="height:2.6em" placeholder="Digite aqui o produto que você está procurando">
                            <button type="submit" class="btn btn-primary mw-25" style="font-size: 1.1rem;">Buscar</button>
                        </form>
                    </div>
                </article>
            </section>
        </div>
    </main>
    </div><!-- .wrapper -->
    <? include('inc/footer.php'); ?>
</body>
</html>