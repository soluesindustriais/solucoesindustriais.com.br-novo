<?php

include 'inc/head.php';
?>
<style>
    .login-screen {
        width: 240px !important;
        margin: 0 auto;
    }

    .login-screen .logo-form img {
        margin: 0 auto !important;
        display: block !important;
        margin-top: 38px !important;
        width: 100px;
    }

    .login-screen form input[type="checkbox"] {
        width: 17px !important;
        display: inline-flex !important;
        margin: 0 0 !important;
        opacity: 0.7 !important;
    }

    .manterlogado {
        display: flex !important;
        align-items: center !important;
    }

    .login-screen a.login-external {
        width: 400px !important;
        display: flex !important;
        align-items: center !important;
        justify-content: center !important;
        height: 50px !important;
        border: 1px solid #2f5283 !important;
        margin: 7px !important;
        border-radius: 6px !important;
        background: #2f5283 !important;
        color: #fff !important;
    }

    .login-screen p {
        margin: 18px 0px !important;
        font-size: 16px !important;
    }

    .login-screen form {
        display: block;
        margin: 0 auto;
    }

    .login-screen form label {
        position: relative !important;
        top: 13px !important;
        letter-spacing: 0.10em !important;
        text-transform: uppercase !important;
        font-size: 14px !important;
        font-weight: bold !important;
        left: 5px;
    }

    .login-screen form span {
        margin-left: 0px !important;
        font-size: 13px !important;
        font-weight: bold !important;
        display: block;
        align-items: center;
        justify-content: center;
        flex-wrap: wrap;
    }

    .login-screen form a {
        font-size: 12px !important;
        font-weight: bold !important;
    }

    input[type="submit"] {
        margin-top: 20px;
        background: #00A5F0 !important;
        border: 1px solid #00A5F0 !important;
    }

    .login-screen input:not([type="submit"]) {
        background: #e5e5e5 !important;
        border: none !important;
        color: #000 !important;
        padding: 0px 16px !important;
        font-weight: 100 !important;
        width: 100%;
        height: 40px;
        border-radius: 9px;
    }

    .login-screen hr {
        color: #939393 !important;
        height: 2px !important;
        opacity: 1 !important;
        width: 350px !important;
        margin-top: 20px !important;
    }

    .login-screen form a:hover {
        text-decoration: underline !important;
    }

    .login-screen select#regiao {
        width: 100%;
        display: block !important;
        margin: 20px 0px !important;
        height: 42px !important;
        background: #e5e5e5 !important;
        color: #000 !important;
        font-weight: bold !important;
        border: 1px solid #d0d7e1 !important;
        border-radius: 12px;
    }

    .bg-white {
        background: #fff !important;
        padding: 11px !important;
        height: 100vh;
    }

    h1.title_login {
        text-transform: uppercase;
        text-align: center;
        font-weight: bold;
        font-size: 2em;
    }

    h2.title_login {
        font-size: 1em;
        font-weight: bold;
    }

    input[type="submit"] {
        width: 100%;
        color: #fff;
        border-radius: 7px;
        height: 39px;
    }

    .login-screen form .content-remember-forgot label {
        position: unset !important;
        margin-left: 10px;
    }

    input#lembrarLogin {
        height: 20px;
    }

    .content-remember-forgot {
        display: flex;
        justify-content: space-between;
        align-items: flex-end;
    }

    .content-remember {
        display: flex;
        margin-top: 10px;
        align-items: center;
    }

    .create-account p a {
        font-size: 18px !important;
    }

    .login-social {
        display: flex;
        flex-direction: column;
        align-items: center;
    }

    .social-img {
        margin-top: 15px;
    }

    .social-img i {
        padding: 10px 20px;
        font-size: 2em !important;
    }

    .login-screen a i {
        color: #000;
    }

    @media only screen and (max-width: 765px) {

        .login-screen input:not([type="submit"]) {
            width: 240px !important;
            height: 40px;
        }

        .login-screen select#regiao {
            width: 240px !important;
        }

    }

    @media only screen and (max-width: 425px) {

        .login-screen input {
            width: 240px !important;
            height: 40px;
        }
    }
</style>
</head>


<div class="bg-white">

    <body>
        <div class="container screen-container">
            <div class="login-screen">
                <div class="logo-form">
                    <a href="<?= $url ?>" title="Soluções Industriais"><img src="<?= $url ?>img/logo.png" alt="Logo Soluções Industriais" title="Logo Soluções Industriais"></a>
                </div>
                <br>
                <h1 class="title_log">Login</h1>
                <form action="#">

                    <label for="email">E-mail</label>
                    <input type="email" id="email" placeholder="@mail.com">
                    <label for="senha">Senha</label>
                    <input type="password" id="senha" placeholder="senha">
                    <div class="content-remember-forgot">
                        <div class="content-remember">
                            <input type="checkbox" name="lembrarLogin" id="lembrarLogin">
                            <label for="lembrarLogin">Lembrar Login</label>
                        </div>
                        <a href="#">Esqueceu a senha?</a>
                    </div>
                    <input type="submit" name="Entrar" value="Finalizar cadastro">

                    <div class="create-account">
                        <p>Não tem uma conta? <a href="<?= $url ?>finalizar-cadastro">Inscrever-se</a></p>
                    </div>

                    <div class="login-social">
                        <h2 class="title_login">Logar Com</h2>
                        <div class="social-img">
                            <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>

                            <a href="#"><i class="fa fa-google" aria-hidden="true"></i></a>
                            <a href="#"><i class="fa fa-apple" aria-hidden="true"></i></a>

                        </div>
                    </div>
                </form>
            </div>
        </div>


    </body>

    </html>