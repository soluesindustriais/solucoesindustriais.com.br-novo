<section class="section footer">
        <div class="container-xxl">
            <div class="row">
                <div class="col-4">
                    <a href="#" title="Soluções Industriais" class="brand">
                        <img src="img/solucoes-industriais-brand.png" alt="Soluções Industriais"/>
                    </a>
                    <p>
                        Soluções Industriais © 2014-2022 - DSW Soluções Digitais LTDA - Todos os direitos reservados
                    </p>
                    <ul class="socialIcons">
                        <li>
                            <a href="#" title="Soluções Industriais no Twitter">
                                <img src="img/ico-twitter.png" alt="Soluções Industriais no Twitter"/>
                            </a>
                        </li>
                        <li>
                            <a href="#" title="Soluções Industriais RSS">
                                <img src="img/ico-rss.png" alt="Soluções Industriais RSS"/>
                            </a>
                        </li>
                        <li>
                            <a href="#" title="Soluções Industriais no Linkedin">
                                <img src="img/ico-linkedin.png" alt="Soluções Industriais no Linkedin"/>
                            </a>
                        </li>
                        <li>
                            <a href="#" title="Soluções Industriais no Instagram">
                                <img src="img/ico-Instagram.png" alt="Soluções Industriais no Instagram"/>
                            </a>
                        </li>
                        <li>
                            <a href="#" title="Soluções Industriais no Facebook">
                                <img src="img/ico-facebook.png" alt="Soluções Industriais no Facebook"/>
                            </a>
                        </li>
                        <li>
                            <a href="#" title="Soluções Industriais no Youtube">
                                <img src="img/ico-youtube.png" alt="Soluções Industriais no Youtube"/>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-8">
                    <ul>
                        <li>
                            <a href="<?=$url?>sobre-nos" title="Sobre nós">
                                Sobre nós
                            </a>
                        </li>
                        <li>
                            <a href="<?=$url?>categoria" title="Categorias">
                                Categorias
                            </a>
                        </li>
                        <li>
                            <a href="<?=$url?>departamento" title="Departamentos">
                                Departamentos
                            </a>
                        </li>
                        <li>
                            <a href="<?=$url?>anuncio" title="Anunciantes">
                                Anunciantes
                            </a>
                        </li>
                        <li>
                            <a href="https://blog.solucoesindustriais.com.br/" target="_blank" title="Blog">
                                Blog
                            </a>
                        </li>
                        <li>
                            <a href="<?=$url?>pdf/termos-de-uso.pdf" title="Política de privacidade">
                                Política de privacidade
                            </a>
                        </li>
                        <li>
                            <a href="#" title="Termos de Uso">
                                Termos de Uso
                            </a>
                        </li>
                    </ul>
                    <ul>
                        <li>
                            <a href="#" title="Produtos e Serviços">
                                Produtos e Serviços
                            </a>
                        </li>
                        <li>
                            <a href="#" title="Marketing Industrial">
                                Marketing Industrial
                            </a>
                        </li>
                        <li>
                            <a href="#" title="Aumente suas vendas">
                                Aumente suas vendas
                            </a>
                        </li>
                        <li>
                            <a href="#" title="Inbound Marketing">
                                Inbound Marketing
                            </a>
                        </li>
                        <li>
                            <a href="#" title="Cotações Online">
                                Cotações Online
                            </a>
                        </li>
                    </ul>
                    <ul>
                        <li>
                            <a href="#" title="Categorias">
                                Categorias
                            </a>
                        </li>
                        <li>
                            <a href="#" title="Anunciantes">
                                Anunciantes
                            </a>
                        </li>
                        <li>
                            <a href="#" title="Sub Departamento 1">
                                Sub Departamento 1
                            </a>
                        </li>
                        <li>
                            <a href="#" title="Sub Departamento 2">
                                Sub Departamento 2
                            </a>
                        </li>
                        <li>
                            <a href="#" title="Sub Departamento 3">
                                Sub Departamento 3
                            </a>
                        </li>
                    </ul>
                    <ul>
                        <li>
                            <a href="#" class="btn btn-success" title="Cadastre-se">
                                Cadastre-se
                            </a>
                        </li>
                        <li>
                            <a href="#" title="Meu painel">
                                Meu painel
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row text-center">
                <div class="col">
                    <ul>
                        <li>
                            <a href="#" title="Soluções Industriais">
                                <img src="img/solucoes-industriais-brand-small.png" alt="Soluções Industriais"/>
                            </a>
                        </li>
                        <li>
                            <p>
                                É uma empresa
                            </p>
                        </li>
                        <li>
                            <a href="#" title="Grupo Ideal Trends">
                                <img src="img/small-brand-grupoit.png" alt="Grupo Ideal Trends"/>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <script src="<?=$url?>/js/reset-api.js"></script>
    <script src="https://solucoesindustriais.com.br/js/dist/sdk-cotacao-solucs/package.js"></script>
    <script src="<?=$url?>js/functions.js"></script>