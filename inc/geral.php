<?php
$dir = $_SERVER['SCRIPT_NAME'];
$dir = pathinfo($dir);
$host = $_SERVER['HTTP_HOST'];
$http = $_SERVER['REQUEST_SCHEME'];
if ($dir["dirname"] == "/") {
  $url = $http . "://" . $host . "/";
} else {
  $url = $http . "://" . $host . $dir["dirname"] . "/";
}
$nomeSite      = 'Soluções Industriais';
$slogan        = 'A Maior Plataforma B2b Industrial Para Compradores Do Brasil';
$fornecedor = "Fornecedor";
$emailContato    = '';
$rua        = '';
$bairro        = '';
$cidade        = '';
$UF          = '';
$cep        = '';
$latitude      = '';
$longitude      = '';
$idCliente      = '';
$idAnalytics    = '';
$explode      = explode("/", $_SERVER['PHP_SELF']);
$urlPagina       = end($explode);
$urlPagina       = str_replace('.php', '', $urlPagina);
$urlPagina       == "index" ? $urlPagina = "" : "";
$urlAnalytics = str_replace("http://www.", '', $url);
$urlAnalytics = str_replace("/", '', $urlAnalytics);
$urlhtaccess = $url;
$schemaReplace = strpos($urlhtaccess, 'http://www.') === false ? 'http://' : 'http://www.';
$urlhtaccess = str_replace($schemaReplace, '', $urlhtaccess);
$urlhtaccess = rtrim($urlhtaccess, '/');
define('RAIZ', $url);
define('HTACCESS', $urlhtaccess);
include('inc/gerador-htaccess.php');


$caminho = '
<div class="breadcrumb" id="breadcrumb  ">
    <div class="wrapper">
        <div class="bread__row">
<nav aria-label="breadcrumb">
  <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
      <a href="' . $url . '" itemprop="item" title="Home">
        <span itemprop="name"> Home ❯  </span>
      </a>
      <meta itemprop="position" content="1" >
    </li>
    <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
      <a href="' . $urlPagina . '">
	  <span itemprop="name">' . $h1 . '</span>
	  </a>
      <meta itemprop="position" content="2" />
    </li>
  </ol>
</nav>
</div>
</div>
</div>
  ';
$caminhoNoticia = '
<div class="breadcrumb" id="breadcrumb  ">
    <div class="wrapper">
        <div class="bread__row">
<nav aria-label="breadcrumb">
  <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
      <a href="' . $url . '" itemprop="item" title="Home">
        <span itemprop="name"> Home ❱  </span>
      </a>
      <meta itemprop="position" content="1" >
    </li>

    <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
      <a href="' . 'anunciante-noticias' . '">
    <span itemprop="name">' . 'Notícias - ' . $fornecedor  . '❱' . '</span>
    </a>
      <meta itemprop="position" content="2" />
    </li>

    <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
      <a href="' . $urlPagina . '">
    <span itemprop="name">' . $h1 . '</span>
    </a>
      <meta itemprop="position" content="2" />
    </li>
  </ol>
</nav>
</div>
</div>
</div>
  ';

  $caminho2 = '
  <div class="breadcrumb" id="breadcrumb  ">
      <div class="wrapper">
          <div class="bread__row">
  <nav aria-label="breadcrumb">
    <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
      <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
        <a href="' . $url . '" itemprop="item" title="Home">
          <span itemprop="name"> Home ❱  </span>
        </a>
        <meta itemprop="position" content="1" >
      </li>
      <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
        <a href="' . $urlPagina . '">
      <span itemprop="name">' . $h1 . ' ❯</span>
      </a>
        <meta itemprop="position" content="2" />
      </li>
      <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
      <a href="' . $urlPagina . '">
    <span itemprop="name">' . $h1 . '</span>
    </a>
      <meta itemprop="position" content="3" />
    </li>
    </ol>
  </nav>
  </div>
  </div>
  </div>
    ';
    $caminho3 = '
    <div class="breadcrumb" id="breadcrumb  ">
        <div class="wrapper">
            <div class="bread__row">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
        <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
          <a href="' . $url . '" itemprop="item" title="Home">
            <span itemprop="name"> Home ❱  </span>
          </a>
          <meta itemprop="position" content="1" >
        </li>
        <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
          <a href="' . $urlPagina . '">
        <span itemprop="name">' . $h1 . ' ❯</span>
        </a>
          <meta itemprop="position" content="2" />
        </li>
        <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
        <a href="' . $urlPagina . '">
      <span itemprop="name">' . $h1 . ' ❯</span>
      </a>
        <meta itemprop="position" content="3" />
      </li>
      <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
      <a href="' . $urlPagina . '">
    <span itemprop="name">' . $h1 . '</span>
    </a>
      <meta itemprop="position" content="4" />
    </li>
      </ol>
    </nav>
    </div>
    </div>
    </div>
      ';
      $caminho4 = '
    <div class="breadcrumb" id="breadcrumb  ">
        <div class="wrapper">
            <div class="bread__row">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
        <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
          <a href="' . $url . '" itemprop="item" title="Home">
            <span itemprop="name"> Home ❱  </span>
          </a>
          <meta itemprop="position" content="1" >
        </li>
        <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
          <a href="' . $urlPagina . '">
        <span itemprop="name">' . $h1 . ' ❯</span>
        </a>
          <meta itemprop="position" content="2" />
        </li>
        <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
        <a href="' . $urlPagina . '">
      <span itemprop="name">' . $h1 . ' ❯</span>
      </a>
        <meta itemprop="position" content="3" />
      </li>
      <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
      <a href="' . $urlPagina . '">
    <span itemprop="name">' . $h1 . ' ❯</span>
    </a>
      <meta itemprop="position" content="4" />
    </li>
    <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
      <a href="' . $urlPagina . '">
    <span itemprop="name">' . $h1 . '</span>
    </a>
      <meta itemprop="position" content="5" />
    </li>
      </ol>
    </nav>
    </div>
    </div>
    </div>
      ';

$isMobile =  preg_match("/(android|webos|avantgo|iphone|ipad|ipod|blackberry|iemobile|bolt|boost|cricket|docomo|fone|hiptop|mini|opera mini|kitkat|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
