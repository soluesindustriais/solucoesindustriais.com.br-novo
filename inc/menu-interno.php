<?
$logado = false
?>

<!-- <style>

</style> -->

<div class="modal-background">
    <div class="container screen-container">
        <div id="modal-exit-x"><i class="fa fa-times" aria-hidden="true"></i></div>
        <div class="login-screen">
            <div class="logo-form">
                <a href="<?= $url ?>" title="Soluções Industriais"><img src="<?= $url ?>img/logo.png" alt="Logo Soluções Industriais" title="Logo Soluções Industriais"></a>
            </div>

            <h1 class="title_log">Login</h1>
            <form action="#">

                <label for="email">E-mail</label>
                <input type="email" id="email" placeholder="@mail.com">
                <label for="senha">Senha</label>
                <input type="password" id="senha" placeholder="senha">
                <div class="content-remember-forgot">
                    <div class="content-remember">
                        <input type="checkbox" name="lembrarLogin" id="lembrarLogin">
                        <label for="lembrarLogin">Lembrar Login</label>
                    </div>
                    <a href="#">Esqueceu a senha?</a>
                </div>
                <input type="submit" name="Entrar" value="Finalizar cadastro">

                <div class="create-account">
                    <p>Não tem uma conta? <a href="<?= $url ?>finalizar-cadastro">Inscrever-se</a></p>
                </div>

                <div class="login-social">
                    <h2 class="title_login">Entre com</h2>
                    <div class="social-img">
                        <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>

                        <a href="#"><i class="fa fa-google" aria-hidden="true"></i></a>
                        <a href="#"><i class="fa fa-apple" aria-hidden="true"></i></a>

                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal-background2">
    <div class="container screen-container">
        <div id="modal-exit-x-2"><i class="fa fa-times" aria-hidden="true"></i></div>
        <div class="login-screen">
            <div class="logo-form">
                <a href="<?= $url ?>" title="Soluções Industriais"><img src="<?= $url ?>img/logo.png" alt="Logo Soluções Industriais" title="Logo Soluções Industriais"></a>
            </div>
            <br>
            <h1 class="title_login">Faça o seu cadastro</h1>
            <form action="#">

                <label for="email">Nome</label>
                <input type="email" id="email" placeholder="Digite seu nome">

                <label for="email">E-mail</label>
                <input type="email" id="email" placeholder="@mail.com">

                <label for="senha">Senha</label>
                <input type="password" id="senha" placeholder="senha">

                <label for="senha">Confirme sua senha</label>
                <input type="password" id="senha" placeholder="Confirme sua senha">


                <label for="phone">Telefone</label>
                <input type="text" id="phone" placeholder="(12) 93456-7890">

                <label for="regiao">Informe a região em que você está!</label>
                <select name="" id="regiao">
                    <option value="">Selecione</option>
                    <option value="AC">Acre</option>
                    <option value="AL">Alagoas</option>
                    <option value="AP">Amapá</option>
                    <option value="AM">Amazonas</option>
                    <option value="BA">Bahia</option>
                    <option value="CE">Ceará</option>
                    <option value="ES">Espírito Santo</option>
                    <option value="GO">Goiás</option>
                    <option value="MA">Maranhão</option>
                    <option value="MT">Mato Grosso</option>
                    <option value="MS">Mato Grosso do Sul</option>
                    <option value="MG">Minas Gerais</option>
                    <option value="PA">Pará</option>
                    <option value="PB">Paraíba</option>
                    <option value="PR">Paraná</option>
                    <option value="PE">Pernambuco</option>
                    <option value="PI">Piauí</option>
                    <option value="RJ">Rio de Janeiro</option>
                    <option value="RN">Rio Grande do Norte</option>
                    <option value="RS">Rio Grande do Sul</option>
                    <option value="RO">Rondônia</option>
                    <option value="RR">Roraima</option>
                    <option value="SC">Santa Catarina</option>
                    <option value="SP">São Paulo</option>
                    <option value="SE">Sergipe</option>
                    <option value="TO">Tocantins</option>
                </select>
                <input type="submit" name="Entrar" value="Finalizar cadastro">
            </form>
        </div>
    </div>
</div>
<div class="container-xxl">
    <div class="row">
        <div class="col">
            <!-- menu mobile-->
            <a href="#" title="Menu" class="btnMenu otherPages">
                <i class="bi bi-list"></i>
            </a>
            <!-- brand -->
            <a href="<?= $url ?>" title="Soluções Industriais" class="brand">
                <img src="img/solucoes-industriais-brand.png" alt="Soluções Industriais" />
            </a>

        </div>
        <div class="col-6">
            <!-- form -->
            <form action="#" class="search" autocomplete="off">
                <fieldset>
                    <select name="type">
                        <option value="produtos">
                            Produtos
                        </option>
                        <option value="list 01">
                            List 01
                        </option>
                        <option value="list 02">
                            List 02
                        </option>
                    </select>
                    <input type="text" class="form-control" name="search" placeholder="O que você está procurando?" autocomplete="off" />
                    <button type="submit" title="Pesquisar">
                        <img src="img/ico-search.png" alt="search" />
                    </button>
                </fieldset>
            </form>
        </div>
        <div class="col">
            <? if ($logado == true) {
            ?>
                <ul>
                    <li class="notify">
                        <a href="#" title="Notificações">
                            <img src="img/ico-notify.png" alt="notify" />
                            <span>
                                2
                            </span>
                        </a>
                    </li>
                    <li class="infos">
                        <img src="img/joao-vicente.png" alt="João Vicente" />
                        <strong>
                            João Vicente
                        </strong>
                        <img src="img/ico-arrow-down.png" alt="icon" />
                        <ul class="menuInfos">
                            <li>
                                <a href="<?= $url ?>dados-pessoais" title="Meus dados pessoais">
                                    <img src="img/ico-data.png" alt="icon" />
                                    <strong>
                                        Meus dados pessoais
                                    </strong>
                                </a>
                            </li>
                            <? if ($isMobile == true) {
                            ?>
                                <li>
                                    <a href="<?= $url ?>orcamento" title="Orçamentos">
                                        Orçamentos
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= $url ?>favoritos" title="Favoritos">
                                        Favoritos
                                    </a>
                                </li>
                            <?
                            } ?>

                            <li>
                                <a href="#" title="Sair da conta">
                                    <img src="img/ico-exit.png" alt="icon" />
                                    <strong>
                                        Sair da conta
                                    </strong>
                                </a>
                            </li>

                        </ul>
                    </li>
                </ul>
            <?
            }
            ?>

        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="location">
                <a href="#" title="Localização" id="changeLocation">
                    <img src="img/ico-location.png" alt="location" />
                    <strong>São paulo</strong>
                </a>
            </div>
            <div class="speech-bubble-container" id="speechBubbleContainer">
                <div class="speech-bubble"></div>
                <div class="speech-bubble-text">
                    <p>Para acessar produtos e ofertas da sua região, identificamos que você está em: <span>São Paulo</span> </p>
                    <button id="openModalRegion" class="btn">Trocar</button>
                    <button id="closeModalRegion" class="btn">Continuar</button>
                </div>
            </div>
        </div>

        <? include('inc/regioes.php') ?>

        <div class="col">
            <div class="menu">
                <ul>
                    <li>
                        <a href="<?= $url ?>" title="Home">
                            Home
                        </a>
                    </li>
                    <li>
                        <div class="menu-item" style="--i: 0.85s">
                            <a href="<?=$url?>categoria.php">Categorias<i class="fa fa-arrow-circle-down" aria-hidden="true"></i></a>
                            <div class="menu-dropdown">
                                <div class="submenu-item dropdrop">
                                    <a href="#">Link 1<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                                    <div class="submenu-dropdown">
                                        <div class="sub-submenu-item">
                                            <a href="#">More<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                                            <div class="sub-submenu-dropdown">
                                                <div class="deep-submenu-item">
                                                    <a href="#">Link 1<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                                                    <div class="deep-submenu-dropdown">
                                                        <div class="deeper-submenu-item">
                                                            <a href="#">Deeper Link</a>
                                                        </div>

                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="submenu-item">
                                    <a href="#">Link 1<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                                    <div class="submenu-dropdown">
                                        <div class="sub-submenu-item">
                                            <a href="#">More<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                                            <div class="sub-submenu-dropdown">
                                                <div class="deep-submenu-item">
                                                    <a href="#">Link 1<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                                                    <div class="deep-submenu-dropdown">
                                                        <div class="deeper-submenu-item">
                                                            <a href="#">Deeper Link</a>
                                                        </div>

                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </li>

                    <li>
                        <div class="menu-item" style="--i: 0.85s">
                            <a href="<?=$url?>departamento">Departamento<i class="fa fa-arrow-circle-down" aria-hidden="true"></i></a>
                            <div class="menu-dropdown">
                                <div class="submenu-item dropdrop">
                                    <a href="#">Link 1<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                                    <div class="submenu-dropdown">
                                        <div class="sub-submenu-item">
                                            <a href="#">More<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                                            <div class="sub-submenu-dropdown">
                                                <div class="deep-submenu-item">
                                                    <a href="#">Link 1<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                                                    <div class="deep-submenu-dropdown">
                                                        <div class="deeper-submenu-item">
                                                            <a href="#">Deeper Link</a>
                                                        </div>

                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="submenu-item">
                                    <a href="#">Link 1<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                                    <div class="submenu-dropdown">
                                        <div class="sub-submenu-item">
                                            <a href="#">More<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                                            <div class="sub-submenu-dropdown">
                                                <div class="deep-submenu-item">
                                                    <a href="#">Link 1<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                                                    <div class="deep-submenu-dropdown">
                                                        <div class="deeper-submenu-item">
                                                            <a href="#">Deeper Link</a>
                                                        </div>

                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </li>
                    <li>
                        <a href="<?= $url ?>sobre-nos" title="Sobre Nós">
                            Sobre Nós
                        </a>
                    </li>
                    <li>
                        <a href="<?= $url ?>anunciantes" title="Anunciantes">
                            Anunciantes
                        </a>
                    </li>
                    <li>
                        <a href="https://blog.solucoesindustriais.com.br/" target="_blank" title="Blog">
                            Blog
                        </a>
                    </li>
                </ul>
                <? if ($isMobile == false && $logado == true) {
                ?>
                    <ul>
                        <li>
                            <a href="<?= $url ?>orcamento" title="Orçamentos">
                                Orçamentos
                            </a>
                        </li>
                        <li>
                            <a href="<?= $url ?>favoritos" title="Favoritos">
                                Favoritos
                            </a>
                        </li>
                    </ul>
                <?
                } else {
                ?>
                    <ul>
                        <li>
                            <a id="loginModal" title="Favoritos">
                                Meu painel
                            </a>
                        </li>
                        <li>
                            <a id="cadastroModal" title="Orçamentos">
                                Cadastre-se
                            </a>
                        </li>
                    </ul>
                <?
                } ?>
                </ul>

            </div>
        </div>
    </div>
</div>
<script>
    document.getElementById('loginModal').addEventListener('click', function() {
        document.querySelector('.modal-background').style.display = 'flex';
    });
    document.getElementById('modal-exit-x').addEventListener('click', function(event) {
        document.querySelector('.modal-background').style.display = 'none';
    });
    document.querySelector('.modal-background').addEventListener('click', function(event) {
        if (event.target === this) {
            this.style.display = 'none';
        }
    });

    document.getElementById('cadastroModal').addEventListener('click', function() {
        document.querySelector('.modal-background2').style.display = 'flex';
    });
    document.getElementById('modal-exit-x-2').addEventListener('click', function(event) {
        document.querySelector('.modal-background2').style.display = 'none';
    });
    document.querySelector('.modal-background2').addEventListener('click', function(event) {
        if (event.target === this) {
            this.style.display = 'none';
        }
    });
</script>



<script>
    document.addEventListener('DOMContentLoaded', function() {
        var locationLink = document.getElementById('changeLocation');
        var speechBubbleContainer = document.getElementById('speechBubbleContainer');
        var openModalRegion = document.getElementById('openModalRegion');
        var closeModalRegion = document.getElementById('closeModalRegion');
        var regionPopup = document.getElementById('regionPopup');
        var overlay = document.getElementById('overlay');

        locationLink.addEventListener('click', function(e) {
            e.preventDefault();
            speechBubbleContainer.style.display = 'block'; // mostra o balãozinho
        });

        closeModalRegion.addEventListener('click', function() {
            speechBubbleContainer.style.display = 'none';
        });

        openModalRegion.addEventListener('click', function() {
            overlay.style.display = 'block'; // ativa o overlay tbm
            regionPopup.style.display = 'block'; // mostra o pop-up de seleção de região
        });

        // Adicione um evento ao overlay para fechar o pop-up de seleção de região se ele existir
        if (overlay) {
            overlay.addEventListener('click', function(event) {
                if (event.target === overlay) {
                    overlay.style.display = 'none';
                    regionPopup.style.display = 'none';
                }
            });
        }
    });
</script>

<script>
    document.addEventListener('DOMContentLoaded', function() {
        var icons = document.querySelectorAll('.menu-item a i, .submenu-item a i, .sub-submenu-item a i, .deep-submenu-item a i');

        icons.forEach(function(icon) {
            icon.addEventListener('click', function(e) {
                e.stopPropagation();
                e.preventDefault();

                // seleciona o sub menu e abre
                var submenu = this.parentElement.nextElementSibling;

                if (submenu) {
                    if (submenu.classList.contains('menu-active')) {
                        submenu.classList.remove('menu-active');
                        submenu.style.display = 'none'; //fecha de vez lista de categorias
                    } else {
                        submenu.classList.add('menu-active');
                        submenu.style.display = 'block';
                    }
                }
            });
        });
    });

    // fecha sub menu clicando fora
    document.body.addEventListener('click', function(e) {
        var openMenus = document.querySelectorAll('.menu-active');
        openMenus.forEach(function(menu) {
            if (!menu.contains(e.target)) {
                menu.classList.remove('menu-active');
            }
        });
    });
</script>