<!-- Para deixar dinamico crie um foreach com a estrura do swiper-slide alterando o link do href para a página e o src da img e coloque um limite de no máximo 10 imagens! aconselho a colocar um shuffle ou criar um algoritimo de prioridade -->
<h2>
    GALERIA DE IMAGENS
</h2>


<script src="https://cdn.jsdelivr.net/npm/swiper@11/swiper-element-bundle.min.js"></script>
<swiper-container>
    <swiper-slide>
        <a href="#" title="Product Gallery 03"  class="d-flex" style="justify-content: space-around;">
            <img src="img/product-gal-hood-03.png" alt="pallet" />
            <img src="img/product-gal-hood-03.png" alt="pallet" />
            <img src="img/product-gal-hood-03.png" alt="pallet" />
        </a>
    </swiper-slide>
    <swiper-slide>
    <a href="#" title="Product Gallery 03"  class="d-flex" style="justify-content: space-around;">
            <img src="img/product-gal-hood-03.png" alt="pallet" />
            <img src="img/product-gal-hood-03.png" alt="pallet" />
            <img src="img/product-gal-hood-03.png" alt="pallet" />
        </a>
    </swiper-slide>
    <swiper-slide>
    <a href="#" title="Product Gallery 03"  class="d-flex" style="justify-content: space-around;">
            <img src="img/product-gal-hood-03.png" alt="pallet" />
            <img src="img/product-gal-hood-03.png" alt="pallet" />
            <img src="img/product-gal-hood-03.png" alt="pallet" />
        </a>
    </swiper-slide>
    <swiper-slide>
    <a href="#" title="Product Gallery 03"  class="d-flex" style="justify-content: space-around;">
            <img src="img/product-gal-hood-03.png" alt="pallet" />
            <img src="img/product-gal-hood-03.png" alt="pallet" />
            <img src="img/product-gal-hood-03.png" alt="pallet" />
        </a>
    </swiper-slide>
    <div class="swiper-pagination"></div>
</swiper-container>
<script>
const swiper = new Swiper('.swiper2', {
    // Optional parameters
    direction: 'vertical',
    loop: true,
    slidesPerView: 3,
    // If we need pagination
    pagination: {
        el: '.swiper-pagination',
    },

    // Navigation arrows
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },

    // And if we need scrollbar
    scrollbar: {
        el: '.swiper-scrollbar',
    },
});
</script>