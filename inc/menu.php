
<div class="row">
                <div class="col boxTop">
                    <!-- menu mobile-->
                    <a href="#" title="Menu" class="btnMenu">
                        <i class="bi bi-list"></i>
                    </a>
                    <!-- brand -->
                    <a href="<?=$url?>" title="Soluções Industriais" class="brand">
                        <img src="img/solucoes-industriais-brand.png" alt="Soluções Industriais"/>
                    </a>
                    <!-- menu -->
                    <ul class="menu">
                        <li>
                            <a href="<?=$url?>sobre-nos" title="Sobre nós">
                                Sobre nós
                            </a>
                        </li>
                        <li>
                            <a href="<?=$url?>categoria" title="Categorias">
                                Categorias
                            </a>
                        </li>
                        <li>
                            <a href="<?=$url?>departamentos" title="Departamentos">
                                Departamentos
                            </a>
                        </li>
                        <li>
                            <a href="<?=$url?>anunciantes" title="Anunciantes">
                                Anunciantes
                            </a>
                        </li>
                        <li>
                            <a href="https://blog.solucoesindustriais.com.br/" target="_blank" title="Blog">
                                Blog
                            </a>
                        </li>
                    </ul>
                    <!-- action -->
                    <ul class="action">
                        <li>
                            <a href="#" title="Meu painel">
                                Meu painel
                            </a>
                        </li>
                        <li>
                            <a href="#" class="btn btn-success" title="Cadastre-se">
                                Cadastre-se
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            