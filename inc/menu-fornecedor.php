
<div class="menu-content p-0 hide-on-mobile">
				<a href="<?=$url?>mini-site-home" title="Dados">
					<div class="border-class menu-content">
						<p>DADOS</p>
					</div>
				</a>
				<a href="<?=$url?>anunciante-produtos" title="Produtos">
					<div class="border-class menu-content">
						<p>PRODUTOS</p>
					</div>
				</a>
				<a href="<?=$url?>mini-site-home" title="Dados">
					<div class="border-class menu-content">
						<p>CATÁLOGOS E ARQUIVOS</p>
					</div>
				</a>
				<a href="<?=$url?>anunciante-noticias" title="Noticias">
					<div class="border-class menu-content">
						<p>NOTICIAS</p>
					</div>
				</a>
				<a href="<?=$url?>anunciante-videos" title="Videos">
					<div class="border-class menu-content">
						<p>VIDEOS</p>
					</div>
				</a>
				<a href="<?=$url?>anunciante-agenda" title="Agenda">
					<div class="border-class menu-content">
						<p>AGENDA</p>
					</div>
				</a>
				<a href="<?=$url?>anunciante-localizacao" title="Localização">
					<div class="border-class mb-5 menu-content">
						<p>LOCALIZAÇÃO</p>
					</div>
				</a>
			</div>
<select id="linkSelect" class="only-mobile" onchange="redirectToSelected()">

  <option value="">Navegue pelo mini site de <?=$fornecedor?></option>
  <!-- Variavel de "fornecedor" no arquivo 'inc/menu-fornecedor.php' -->
  <option value="<?=$url?>mini-site-home">Dados</option>
  <option value="<?=$url?>anunciante-produtos">Produtos</option>
  <option value="<?=$url?>mini-site-home">Catálogos e Arquivos</option>
  <option value="<?=$url?>anunciante-noticias">Noticias</option>
  <option value="<?=$url?>anunciante-videos">Videos</option>
  <option value="<?=$url?>anunciante-agenda">Agenda</option>
  <option value="<?=$url?>anunciante-localizacao">Localização</option>
  
</select>


	<script>
  function redirectToSelected() {
    var selectedLink = document.getElementById("linkSelect").value;
    if (selectedLink !== '') {
      window.location.href = selectedLink;
    }
  }
</script>
