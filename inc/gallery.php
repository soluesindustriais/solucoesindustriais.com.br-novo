<!-- Para deixar dinamico crie um foreach com a estrura do swiper-slide alterando o link do href para a página e o src da img e coloque um limite de no máximo 10 imagens! aconselho a colocar um shuffle ou criar um algoritimo de prioridade -->
<h2>
GALERIA DE IMAGENS
</h2>

<style>
    .lightbox {
        width: 250px;
        height: 200px;
        display: flex !important;
        flex-direction: column !important;
        justify-content: space-between !important;
    }
    .lightbox > img {
        width: 100%;
        height: 250px !important;
    }
    


</style>
<div class="swiper brandsGallery2">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <a href="#" title="products" class="lightbox">
                                    <img src="img/produtos/prod-1.jpg" alt="Gerdau" />
                                    <p>Product - 1</p>
                                </a>
                            </div>
                            
                            <div class="swiper-slide">
                                <a href="#" title="products" class="lightbox">
                                    <img src="img/produtos/prod-2.jpg" alt="Gerdau" />
                                    <p>Product - 2</p>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="#" title="products" class="lightbox">
                                    <img src="img/produtos/prod-3.jpg" alt="Gerdau" />
                                    <p>Product - 3</p>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="#" title="products" class="lightbox">
                                    <img src="img/produtos/prod-4.jpg" alt="Gerdau" />
                                    <p>Product - 4</p>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="#" title="products" class="lightbox">
                                    <img src="img/produtos/prod-5.jpg" alt="Gerdau" />
                                    <p>Product - 5</p>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="#" title="products" class="lightbox">
                                    <img src="img/produtos/prod-6.jpg" alt="Gerdau" />
                                    <p>Product - 6</p>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="#" title="products" class="lightbox">
                                    <img src="img/produtos/prod-7.jpg" alt="Gerdau" />
                                    <p>Product - 7</p>
                                </a>
                            </div>
                        </div>
                        <div class="swiper-button-next"></div>
                        <div class="swiper-button-prev"></div>
                    </div>