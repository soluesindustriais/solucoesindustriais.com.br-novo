<!-- <style>

</style> -->

<div class="modal-background3">
    <? include('inc/forms.php'); ?>
</div>

<div class="row">
    <div class="col boxTop">
        <!-- menu mobile-->
        <a href="#" title="Menu" class="btnMenu">
            <i class="bi bi-list"></i>
        </a>
        <!-- brand -->
        <a href="<?= $url ?>" title="Soluções Industriais" class="brand">
            <img src="img/solucoes-industriais-brand.png" alt="Soluções Industriais" />
        </a>
        <!-- menu -->
        <ul class="menu">
            <li>
                <a href="<?= $url ?>sobre-nos" title="Sobre nós">
                    Sobre nós
                </a>
            </li>
            <li>
                <div class="menu-item-home" style="--i: 0.85s">
                    <a href="<?= $url ?>categoria">Categorias<i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                    <div class="menu-dropdown-home">
                        <div class="submenu-item-home dropdrop">
                            <a href="#">Link 1<i class="fa fa-arrow-circle-down" aria-hidden="true"></i></a>
                            <div class="submenu-dropdown-home">
                                <div class="sub-submenu-item-home">
                                    <a href="#">More<i class="fa fa-arrow-circle-down" aria-hidden="true"></i></a>
                                    <div class="sub-submenu-dropdown-home">
                                        <div class="deep-submenu-item-home">
                                            <a href="#">Link 1<i class="fa fa-arrow-circle-down" aria-hidden="true"></i></a>
                                            <div class="deep-submenu-dropdown-home">
                                                <div class="deeper-submenu-item-home">
                                                    <a href="#">Deeper Link</a>
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="submenu-item-home">
                            <a href="#">Link 1<i class="fa fa-arrow-circle-down" aria-hidden="true"></i></a>
                            <div class="submenu-dropdown-home">
                                <div class="sub-submenu-item-home">
                                    <a href="#">More<i class="fa fa-arrow-circle-down" aria-hidden="true"></i></a>
                                    <div class="sub-submenu-dropdown-home">
                                        <div class="deep-submenu-item-home">
                                            <a href="#">Link 1<i class="fa fa-arrow-circle-down" aria-hidden="true"></i></a>
                                            <div class="deep-submenu-dropdown-home">
                                                <div class="deeper-submenu-item-home">
                                                    <a href="#">Deeper Link</a>
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </li>
            <li>
                <div class="menu-item-home" style="--i: 0.85s">
                    <a href="<?= $url ?>departamento">Departamento<i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                    <div class="menu-dropdown-home">
                        <div class="submenu-item-home dropdrop">
                            <a href="#">Link 1<i class="fa fa-arrow-circle-down" aria-hidden="true"></i></a>
                            <div class="submenu-dropdown-home">
                                <div class="sub-submenu-item-home">
                                    <a href="#">More<i class="fa fa-arrow-circle-down" aria-hidden="true"></i></a>
                                    <div class="sub-submenu-dropdown-home">
                                        <div class="deep-submenu-item-home">
                                            <a href="#">Link 1<i class="fa fa-arrow-circle-down" aria-hidden="true"></i></a>
                                            <div class="deep-submenu-dropdown-home">
                                                <div class="deeper-submenu-item-home">
                                                    <a href="#">Deeper Link</a>
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="submenu-item-home">
                            <a href="#">Link 1<i class="fa fa-arrow-circle-down" aria-hidden="true"></i></a>
                            <div class="submenu-dropdown-home">
                                <div class="sub-submenu-item-home">
                                    <a href="#">More<i class="fa fa-arrow-circle-down" aria-hidden="true"></i></a>
                                    <div class="sub-submenu-dropdown-home">
                                        <div class="deep-submenu-item-home">
                                            <a href="#">Link 1<i class="fa fa-arrow-circle-down" aria-hidden="true"></i></a>
                                            <div class="deep-submenu-dropdown-home">
                                                <div class="deeper-submenu-item-home">
                                                    <a href="#">Deeper Link</a>
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </li>
            <li>
                <a href="<?= $url ?>anunciantes" title="Anunciantes">
                    Anunciantes
                </a>
            </li>
            <li>
                <a href="https://blog.solucoesindustriais.com.br/" target="_blank" title="Blog">
                    Blog
                </a>
            </li>

            <!-- action -->
            <ul class="action mobOnly">
                <li class="painel-action-li">
                    <a title="Meu painel" id="loginModal" class="painel-action-a">
                        Meu painel
                    </a>
                </li>
                <li>
                    <a class="btn btn-success" id="cadastroModal" title="Cadastre-se">
                        Cadastre-se
                    </a>
                </li>
            </ul>
        </ul>
        <!-- action -->
        <ul class="action deskOnly">
            <li class="painel-action-li">
                <a id="loginModal-1" title="Meu painel" class="painel-action-a">
                    Meu painel
                </a>
            </li>
            <li>
                <a id="cadastroModal-1" class="btn btn-success" title="Cadastre-se">
                    Cadastre-se
                </a>
            </li>
        </ul>
    </div>
</div>


<script>
    document.addEventListener('DOMContentLoaded', function() {
        // Selecione todos os ícones dentro dos links do menu
        var icons = document.querySelectorAll('.menu-item-home a i, .submenu-item-home a i, .sub-submenu-item-home a i, .deep-submenu-item-home a i');

        icons.forEach(function(icon) {
            icon.addEventListener('click', function(e) {
                e.stopPropagation(); // Impede que o evento do clique se propague
                e.preventDefault(); // Previne o comportamento padrão do link

                // Seleciona o submenu relacionado ao ícone clicado
                var submenu = this.parentElement.nextElementSibling;

                if (submenu) {
                    // Verifica se o submenu já está visível
                    if (submenu.classList.contains('menu-active')) {
                        // Se já estiver visível, o fechamos
                        submenu.classList.remove('menu-active');
                        submenu.style.display = 'none'; // Opcional, para fechamento imediato
                    } else {
                        // Se não estiver visível, o abrimos
                        submenu.classList.add('menu-active');
                        submenu.style.display = 'block'; // Opcional, para abertura imediata
                    }
                }
            });
        });
    });


    // Adicionalmente, você pode querer garantir que um clique fora dos menus feche os submenus abertos
    document.body.addEventListener('click', function(e) {
        var openMenus = document.querySelectorAll('.menu-active');
        openMenus.forEach(function(menu) {
            if (!menu.contains(e.target)) {
                menu.classList.remove('menu-active');
            }
        });
    });
</script>

<script>
    document.getElementById('loginModal-1').addEventListener('click', function() {
        document.querySelector('.modal-background3').style.display = 'flex';
    });
    document.getElementById('modal-exit-x').addEventListener('click', function(event) {
        document.querySelector('.modal-background3').style.display = 'none';
    });
    document.querySelector('.modal-background3').addEventListener('click', function(event) {
        if (event.target === this) {
            this.style.display = 'none';
        }
    });

    document.getElementById('cadastroModal-1').addEventListener('click', function() {
        document.querySelector('.modal-background4').style.display = 'flex';
    });
    document.getElementById('modal-exit-x-2').addEventListener('click', function(event) {
        document.querySelector('.modal-background4').style.display = 'none';
    });
    document.querySelector('.modal-background4').addEventListener('click', function(event) {
        if (event.target === this) {
            this.style.display = 'none';
        }
    });
</script>