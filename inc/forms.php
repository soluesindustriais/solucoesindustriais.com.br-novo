<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style-forms.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js"></script>
    <script defer src="js/forms.js"></script>
    <title>Document</title>
</head>


<div class="both__forms">
<div id="modal-exit-x"><i class="fa fa-times" aria-hidden="true"></i></div>
    <!-- Sign Up -->
    <div class="container__form container--signup">
        <form action="#" class="form" id="form1">
            <h2 class="fixed__title">Faça parte do Soluções Industriais</h2>
            <div class="sign-up-social">
                <h2 class="title_sign-up">Cadastrar-se com</h2>
                <div class="social-img">
                    <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>

                    <a href="#"><i class="fa fa-google" aria-hidden="true"></i></a>
                    <a href="#"><i class="fa fa-apple" aria-hidden="true"></i></a>

                </div>
            </div>

            <!-- NÃO APAGAR, SERÁ USADO DEPOIS -->

            <div class="signUpType">
                <input type="radio" name="userType" id="useyType1" value="Comprador">Comprador
                <input type="radio" name="userType" id="useyType2" value="Fornecedor">Fornecedor
                <input type="radio" name="userType" id="useyType3" value="Ambos">Ambos<br>
            </div>

            <!-- NÃO APAGAR, SERÁ USADO DEPOIS -->

            <input type="text" placeholder="Nome" class="input" required />
            <input type="tel" placeholder="Telefone" class="input" maxlength="15" required />
            <input type="email" placeholder="E-mail" class="input" required />
            <input id="hidden" type="hidden" class="input" maxlength="18" />
            <p class="signIn__mob">Já possui uma conta? <a href="#" id="signInMob">Clique aqui</a></p>
            <button class="btn__form" type="submit">Cadastrar-se</button>
        </form>


        <!--! ________________________ segundo passo do cadastro _______________________________ !-->

        <div class="form-step" id="form-step-2" style="display:none;">
            <form action="#" class="form">
                <h2 class="form__title">Preencha os dados da sua Empresa</h2>
                <div class="selects">
                    <input type="text" placeholder="Nome Fantasia" class="input" />
                    <label>Segmento Primário</label>
                    <select name="segmento primário" id="segmento">
                        <option value="" selected>Selecione</option>
                        <option value="Value 1">Value 1</option>
                        <option value="Value 2">Value 2</option>
                    </select>
                    <label id="secondSeg">Segmento Secundário</label>
                    <select name="segmento secundário" id="segType">
                        <option value="" selected>Selecione</option>
                        <option value="Value 1">Value 1</option>
                        <option value="Value 2">Value 2</option>
                    </select>
                    <label id="thridSeg">Segmento Terceiário</label>
                    <select name="Segmento Terceiário" id="terType">
                        <option value="" selected>Selecione</option>
                        <option value="Value 1">Value 1</option>
                        <option value="Value 2">Value 2</option>
                    </select>

                    <label id="typeLabel">Tipo de Empresa</label>
                    <select name="Tipo de Empresa" id="TypeCompany">
                        <option value="" selected>Selecione</option>
                        <option value="Value 1">Value 1</option>
                        <option value="Value 2">Value 2</option>
                    </select>
                </div>
                <div class="buttonsForm">
                    <button type="button" class="btn__form btn-back">Voltar</button>
                    <button type="button" class="btn__form btn-next">Continuar</button>
                </div>
            </form>
        </div>

        <!--! _________________________ terceiro passo do cadastro _____________________________ !-->

        <div class="form-step" id="form-step-3" style="display:none;">
            <form action="#" class="form">
                <h2 class="form__title">Preencha seus dados de Contato</h2>
                <!-- <div class="form-content"> -->
                <input type="url" placeholder="Site da Empresa" class="input" />
                <input type="email" placeholder="E-mail coorporativo" class="input" />
                <select id="state-select" multiple name="state[]" placeholder="Estados que deseja atender" class="input demo-default" required>
                    <option value="">Escolha um estado...</option>
                    <option value="Acre">Acre</option>
                    <option value="Alagoas">Alagoas</option>
                    <option value="Amapá">Amapá</option>
                    <option value="Amazonas">Amazonas</option>
                    <option value="Bahia">Bahia</option>
                    <option value="Ceará">Ceará</option>
                    <option value="Espírito Santo">Espírito Santo</option>
                    <option value="Goiás">Goiás</option>
                    <option value="Maranhão">Maranhão</option>
                    <option value="Mato Grosso">Mato Grosso</option>
                    <option value="Mato Grosso do Sul">Mato Grosso do Sul</option>
                    <option value="Minas Gerais">Minas Gerais</option>
                    <option value="Pará">Pará</option>
                    <option value="Paraíba">Paraíba</option>
                    <option value="Paraná">Paraná</option>
                    <option value="Pernambuco">Pernambuco</option>
                    <option value="Piauí">Piauí</option>
                    <option value="Rio de Janeiro">Rio de Janeiro</option>
                    <option value="Rio Grande do Norte">Rio Grande do Norte</option>
                    <option value="Rio Grande do Sul">Rio Grande do Sul</option>
                    <option value="Rondônia">Rondônia</option>
                    <option value="Roraima">Roraima</option>
                    <option value="Santa Catarina">Santa Catarina</option>
                    <option value="São Paulo">São Paulo</option>
                    <option value="Sergipe">Sergipe</option>
                    <option value="Tocantins">Tocantins</option>
                </select>
                <!-- </div> -->
                <div class="buttonsForm">
                    <button type="button" class="btn__form btn-back">Voltar</button>
                    <button type="submit" class="btn__form btn-next">Criar Cadastro</button>
                </div>
            </form>
        </div>



    </div>



    <!-- Sign In -->
    <div class="container__form container--signin">
        <form action="#" class="form" id="form2">
            <h2 class="form__title__login" id="formTitleLogin">Login</h2>
            <div class="login-social" id="socialLogin">
                <h2 class="title_login">Entrar com</h2>
                <div class="social-img">
                    <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                    <a href="#"><i class="fa fa-google" aria-hidden="true"></i></a>
                    <a href="#"><i class="fa fa-apple" aria-hidden="true"></i></a>
                </div>
            </div>
            <input type="email" placeholder="Email" class="input" id="emailInput" />
            <input type="password" placeholder="Senha" class="input" id="passwordInput" />
            <a href="#" class="link" title="Esqueceu a Senha?" id="recuperarSenha">Esqueceu a Senha?</a>
            <button class="btn__form" id="loginButton">Entrar</button>
            <button class="btn__form" id="backButton" style="display: none;">Voltar</button>
            <p class="signUp__mob">Ainda não possui uma conta? <a href="#" id="signUpMob">Clique aqui</a> para se cadastrar</p>
        </form>

    </div>

    <!-- Overlay -->
    <div class="container__overlay">
        <div class="overlay">
            <div class="overlay__panel overlay--left">
                <button class="btn__form" id="signIn">Login</button>
            </div>
            <div class="overlay__panel overlay--right">
                <button class="btn__form" id="signUp">Cadastrar-se</button>
            </div>
        </div>
    </div>
</div>