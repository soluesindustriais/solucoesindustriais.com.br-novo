<style>
    /*  modal */
    .modal-regions.close {
        display: none;
    }

    .modal-regions {
        z-index: 9999;
        position: fixed;
        background: #00000052;
        width: 100vw;
        height: 100vh;
        top: 0;
        left: 0;
    }

    .modal-regions .content {
        display: flex;
        background: #fff;
        padding: 52px 30px 20px 30px;
        border-radius: 10px;
        border: solid 1px #00000085;
        box-shadow: 0px 1px 20px #000000b8;
        position: relative;
        min-height: 200px;
        min-width: 200px;
        transform-origin: top;
        position: absolute;
        top: 35%;
        left: 50%;
        flex-direction: column;
        transform: translate(-50%, -50%);
    }

    .modal-regions .content .btn-close {
        cursor: pointer;
        background: #fff;
        border: none;
        position: absolute;
        right: 10px;
        top: 10px;
        width: 50px;
        height: 50px;
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .modal-regions .content h2.title {
        font-size: 24px;
        line-height: 31px;
        font-weight: 800;
        text-align: center;
        color: #504f96;
    }

    .modal-regions .content p {
        font-size: 16px;
        line-height: 24px;
        font-weight: 400;
        text-align: center;
    }

    .modal-regions .content #estado:focus-visible {
        outline: none
    }

    .modal-regions .content #estado {
        margin: 10px auto;
        display: block;
        text-align: center;
        padding: 10px;
        border-radius: 8px;
        min-width: 250px;
        border: solid #2a538630;
        color: rgb(35 85 129);
        background: #00000005;
        font-size: 18px;
        font-weight: 600;
    }

    .modal-regions .content option {
        text-align: left;
    }


    .modal-regions .content .btn-set-region:hover {
        background: rgb(28 61 124);
    }

    .modal-regions .content .btn-set-region {
        height: auto;
        font-weight: 600;
        text-decoration: none;
        cursor: pointer;
        border-color: transparent;
        border-style: solid;
        border-width: 1px;
        border-radius: 8px;
        transition: 0.3s;
        color: rgb(255 255 255);
        background: rgb(43 92 187 / 83%);
        width: 100%;
        font-size: 16px;
        line-height: 24px;
        padding: 5px;
        border: solid #2a538630;
        margin: 36px 0 auto auto;
        display: block;
    }

    @media (max-width: 800px) {
        .modal-regions .content {
            right: 10vh !important;
        }

        .modal-regions .content #estado {
            min-width: 200px !important;
        }

        .modal-regions .content {
            background: #fff;
            padding: 52px 30px 20px 30px;
            border-radius: 10px;
            border: solid 1px #00000085;
            box-shadow: 0px 1px 20px #000000b8;
            position: relative;
            min-height: 200px;
            min-width: 90%;
            transform-origin: top;
            position: absolute;
            top: 2vh;
            left: 15px;
        }

        #cotacao-modal .cotacao-modal-content {
            position: relative;
            background: #eee;
            width: 40vw;
            border: 1px solid #aaa;
            box-sizing: border-box;
            padding: 20px;
            margin: 20px auto;
            border-radius: 10px;
            width: 90%;
        }
    }
</style>

<div class="modal-regions close" id="modal-regions">
    <div class="content">
        <button title="Close" class="btn-close">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" width="16" height="16" fill="currentColor" role="presentation">
                <path d="M8.632 10.01L0 1.376 1.359.017 9.98 8.641 18.619 0l1.316 1.316-8.637 8.638L20 18.658 18.658 20l-8.693-8.696-8.648 8.65L.004 18.64l8.628-8.63" fill-rule="nonzero">

                </path>
            </svg>
        </button>
        <h2 class="title">Onde você está?</h2>
        <p>Para uma melhor experiência dentro do site, nos informe a região do local de entrega.</p>

        <select id="estado" name="estado" required="">
            <option value="">Selecione a Região</option>
            <option value="AC">Acre</option>
            <option value="AL">Alagoas</option>
            <option value="AP">Amapá</option>
            <option value="AM">Amazonas</option>
            <option value="BA">Bahia</option>
            <option value="CE">Ceará</option>
            <option value="DF">Distrito Federal</option>
            <option value="ES">Espírito Santo</option>
            <option value="GO">Goiás</option>
            <option value="MA">Maranhão</option>
            <option value="MT">Mato Grosso</option>
            <option value="MS">Mato Grosso do Sul</option>
            <option value="MG">Minas Gerais</option>
            <option value="PA">Pará</option>
            <option value="PB">Paraíba</option>
            <option value="PR">Paraná</option>
            <option value="PE">Pernambuco</option>
            <option value="PI">Piauí</option>
            <option value="RJ">Rio de Janeiro</option>
            <option value="RN">Rio Grande do Norte</option>
            <option value="RS">Rio Grande do Sul</option>
            <option value="RO">Rondônia</option>
            <option value="RR">Roraima</option>
            <option value="SC">Santa Catarina</option>
            <option value="SP">São Paulo</option>
            <option value="SE">Sergipe</option>
            <option value="TO">Tocantins</option>
            <option value="EX">Estrangeiro</option>
            <option value="Todos">Brasil</option>
        </select>
        <button title="Close" class="btn-set-region" id="btnSaveRegion">
            Salvar
        </button>

    </div>
</div>


<script>
    // script para trocar a região
    document.addEventListener('DOMContentLoaded', function() {
        var openModalRegion = document.getElementById('openModalRegion');
        var closeModal = document.querySelector('.modal-regions .btn-close');
        var setRegion = document.querySelector('.modal-regions .btn-set-region');
        var modalRegions = document.getElementById('modal-regions');

        openModalRegion.addEventListener('click', function() {
            modalRegions.classList.remove('close');
        });

        // botão fechar modal
        closeModal.addEventListener('click', function() {
            modalRegions.classList.add('close');
        });

        // botão salvar
        setRegion.addEventListener('click', function() {
            var selectedRegion = document.getElementById('estado').value;
            console.log(selectedRegion);

            modalRegions.classList.add('close');
        });
    });


    // alterar a região
    document.addEventListener('DOMContentLoaded', function() {
        document.getElementById('estado').addEventListener('change', function() {
            var selectedRegionText = this.options[this.selectedIndex].text;
            updateLocation(selectedRegionText);
        });
    });

    document.addEventListener('DOMContentLoaded', function() {
        var btnSaveRegion = document.getElementById('btnSaveRegion');

        btnSaveRegion.addEventListener('click', function() {
            var selectElement = document.getElementById('estado');
            var selectedRegionText = selectElement.options[selectElement.selectedIndex].text;
            var selectedRegionValue = selectElement.value;
            var combinedText = selectedRegionText + ', ' + selectedRegionValue;
            
            localStorage.setItem('userRegion', combinedText);

            window.location.reload();
        });

        // verifica se existe uma região salva no localStorage pra atualizar
        var savedRegion = localStorage.getItem('userRegion');
        if (savedRegion) {
            updateLocation(savedRegion);
        }
    });


    function updateLocation(state) {
        var locationStrong = document.querySelector('#changeLocation strong');
        if (locationStrong) {
            locationStrong.textContent = state;
        }
        document.querySelector('.speech-bubble-container span').textContent = state;
    }
</script>