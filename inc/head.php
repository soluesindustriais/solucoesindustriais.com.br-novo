<!DOCTYPE html>
<html lang="pt-BR">
<?php
include 'inc/geral.php';
?>

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="<?= $desc ?>">

    <link rel="icon" type="image/ico" href="img/favicon.ico" />


    <!-- BOOTSTRAP @5.3.2 -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous" />

    <!-- SWIPER JS @11.0.0 -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.css" />

    <!-- fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;700&display=swap" rel="stylesheet" />

    <!-- icons -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.2/font/bootstrap-icons.min.css" />

    <!-- styles -->
    <link rel="stylesheet" href="css/styles.css" />
    <link rel="stylesheet" href="css/gabriel.css" />

    <!-- media -->
    <link rel="stylesheet" href="css/media.css" />

    <!-- Style menus -->
    <link rel="stylesheet" href="css/menuHome.css" />
    <link rel="stylesheet" href="css/menu.css" />

    <script src="https://code.jquery.com/jquery-3.7.1.min.js" integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=League+Spartan:wght@200;400;600;700&family=Open+Sans:wght@300;400;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title><?= $h1 ?> - Soluções Industriais</title>




    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "Organization",
            "name": "Soluções Industriais",
            "url": "https://www.solucoesindustriais.com.br/",
            "logo": "https://www.solucoesindustriais.com.br/img/solucoes-industriais-brand.png",
            "sameAs": [
                "https://www.facebook.com/plataformasolucoesindustriais",
                "https://twitter.com/solucoesindustr",
                "https://www.instagram.com/solucoesindustriaisoficial/",
                "https://br.linkedin.com/company/solucoesindustriais",
                "https://br.pinterest.com/solucoesindustriaismarketplace/"
            ]
        }
    </script>