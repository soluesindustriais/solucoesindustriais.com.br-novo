<?php
$h1 = 'Detalhes orçamento';
$desc = 'Falta desc';
include 'inc/head.php';
?>
<!-- styles -->
<link rel="stylesheet" href="css/cmp-styles.css" />

<!-- media -->
<link rel="stylesheet" href="css/cmp-media.css" />
</head>

<body>

    <!-- header -->
    <section class="section header">
        <!-- <div class="container"> -->
        <?php
        include 'inc/menu-interno.php';
        ?>
    </section>
    <!-- content list top order -->
    <section class="section contentListTopOrder">
        <div class="container-xxl">
            <?= $caminho ?>
            <div class="row">
                <div class="col">
                    <h3>
                        Detalhes do Orçamento #343453
                    </h3>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <img src="img/img-product-big.png" alt="product big" />
                </div>
                <div class="col">
                    <p>
                        <strong>
                            Caixa de papelão
                        </strong>
                        <span>
                            <img src="img/ico-product-paper.png" alt="Caixa de papelão" />
                            Produto/Serviço
                        </span>
                        <em>
                            Atendimento exclusivo para São Paulo Se alguém busca por caixa de papelão, descobrirá na líder do segmento Officepack Embalage
                        </em>
                    </p>
                </div>
                <div class="col">
                    <a href="<?= $url ?>anuncio" class="btn btn-success btn-fill btn-blue" title="Visualizar anúncio">
                        <img src="img/ico-eye.png" alt="Visualizar anúncio" />
                        Visualizar anúncio
                    </a>
                </div>
            </div>
        </div>
    </section>
    <section class="section contentListDetails">
        <div class="container-xxl">
            <div class="row">
                <div class="col">
                    <p>
                        <strong>
                            Data do orçamento
                        </strong>
                        22/08/2023 - 03:39
                    </p>
                    <p>
                        <strong>
                            Produto/Serviço
                        </strong>
                        Caixa de papelão
                    </p>
                    <p>
                        <strong>
                            Potenciais anunciantes
                            <img src="img/ico-help.png" alt="Quantos fornecedores têm interesse em seu orçamento" title="Quantos fornecedores têm interesse em seu orçamento" />
                        </strong>
                        2 Fornecedores
                    </p>
                    <p>
                        <strong>
                            Mensagem
                        </strong>
                        Teste de qualidade de orçamento.
                    </p>
                </div>
                <div class="col">
                    <p>
                        <strong>
                            Status
                        </strong>
                        <em class="statusNone">
                            Finalizado
                        </em>
                    </p>
                    <p>
                        <strong>
                            Perfil do comprador
                        </strong>
                        Pessoa Física
                    </p>
                </div>
            </div>
        </div>
    </section>

    <!-- content category -->
    <section class="section contentList boxOrder">
        <div class="container-xxl">
            <div class="row">
                <div class="col">
                    <h3>
                        Anunciantes Interessados em seu orçamento
                    </h3>
                    <div class="row linePending">
                        <div class="col">
                            <p>
                                <strong>
                                    01/01/2023
                                </strong>
                                Data do orçamento
                            </p>
                        </div>
                        <div class="col">
                            <img src="img/product-tuberfil.png" alt="Tuberfil Indústria e Comércio de Tubos" />
                            <p>
                                <strong>
                                    Tuberfil Indústria e Comércio de Tubos
                                </strong>
                                <a href="#" class="btn btn-success btn-fill btn-blue" title="Iniciar Avaliação">
                                    Iniciar Avaliação
                                </a>
                            </p>
                        </div>
                        <div class="col">
                            <p>
                                <strong>
                                    Status
                                </strong>
                                <em class="statusFinished">
                                    Orçamento Ganho
                                </em>
                            </p>
                        </div>
                        <div class="col">
                            <a href="<?= $url ?>chat" class="btn btn-success btn-fill" title="Acessar o Chat">
                                Acessar o Chat
                            </a>
                            <a href="#" class="btn btn-success btn-fill btn-blue" title="Orçar novamente">
                                Orçar novamente
                            </a>
                        </div>
                    </div>
                    <div class="row linePending">
                        <div class="col">
                            <p>
                                <strong>
                                    01/01/2023
                                </strong>
                                Data do orçamento
                            </p>
                        </div>
                        <div class="col">
                            <img src="img/product-dayco.png" alt="Dayco Power Transmission Ltda" />
                            <p>
                                <strong>
                                    Dayco Power Transmission Ltda
                                </strong>
                                <a href="#" class="btn btn-success btn-fill btn-blue" title="Iniciar Avaliação">
                                    Iniciar Avaliação
                                </a>
                            </p>
                        </div>
                        <div class="col">
                            <p>
                                <strong>
                                    Status
                                </strong>
                                <em class="statusNone">
                                    Finalizado
                                </em>
                            </p>
                        </div>
                        <div class="col">
                            <a href="#" class="btn btn-success btn-fill" title="Acessar o Chat">
                                Acessar o Chat
                            </a>
                            <a href="#" class="btn btn-success btn-fill btn-blue" title="Orçar novamente">
                                Orçar novamente
                            </a>
                        </div>
                    </div>
                    <div class="row linePending">
                        <div class="col">
                            <p>
                                <strong>
                                    01/01/2023
                                </strong>
                                Data do orçamento
                            </p>
                        </div>
                        <div class="col">
                            <img src="img/product-tuberfil.png" alt="Tuberfil Indústria e Comércio de Tubos" />
                            <p>
                                <strong>
                                    Tuberfil Indústria e Comércio de Tubos
                                </strong>
                                <a href="#" class="btn btn-success btn-fill btn-blue" title="Iniciar Avaliação">
                                    Iniciar Avaliação
                                </a>
                            </p>
                        </div>
                        <div class="col">
                            <p>
                                <strong>
                                    Status
                                </strong>
                                <em class="statusFinished">
                                    Orçamento Ganho
                                </em>
                            </p>
                        </div>
                        <div class="col">
                            <a href="#" class="btn btn-success btn-fill" title="Acessar o Chat">
                                Acessar o Chat
                            </a>
                            <a href="#" class="btn btn-success btn-fill btn-blue" title="Orçar novamente">
                                Orçar novamente
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- footer -->
    <?php include 'inc/footer.php'; ?>

    <!-- BOOTSTRAP @5.3.2 -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>

    <!-- SWIPER JS @11.0.0 -->
    <script src="https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.js"></script>

    <!-- functions -->
    <script src="js/functions.js"></script>

</body>

</html>